precision mediump float;
varying vec2 v_TexCoord;
uniform sampler2D sTexture;

void main() {
    float b, g, r;
    vec4 rgbData =texture2D(sTexture, v_TexCoord);
    r = rgbData.r;
    g = rgbData.g;
    b = rgbData.b;
    gl_FragColor = vec4(b, g, r, 1.0);
}