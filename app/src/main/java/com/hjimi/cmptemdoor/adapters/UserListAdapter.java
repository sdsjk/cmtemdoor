package com.hjimi.cmptemdoor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.base.BaseHeaderAdapter;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.FileUtils;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class UserListAdapter extends BaseHeaderAdapter<UserListAdapter.ViewHolder> {
    private SoftReference<BaseActivity> mContext;
    private List<UserInfo> list;

    public UserListAdapter(BaseActivity context) {
        mContext = new SoftReference<>(context);
        list = new ArrayList<>();
    }

    public void setList(List<UserInfo> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(List<UserInfo> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    protected int getAdvanceViewType(int position) {
        return position;
    }

    @Override
    protected int getAdvanceItemId(int position) {
        return 0;
    }

    @Override
    protected int getAdvanceCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    protected void onBindAdvanceViewHolder(final RecyclerView.ViewHolder holder, final int i) {
        ((ViewHolder) holder).tvName.setText(FileUtils.getContent(list.get(i).getUserName(), 8));
        ((ViewHolder) holder).tvJobNo.setText(FileUtils.getContent(list.get(i).getWorkerId(), 8));
        ((ViewHolder) holder).tvBranch.setText(list.get(i).getBranch());
        FileUtils.loadLocalPicture(((ViewHolder) holder).ivPhoto, ImiNrApplication.getApplication().getFilesDir().getAbsolutePath()+File.separator+list.get(i).getLocalName());
    }


    @Override
    protected ViewHolder onCreateAdvanceViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext.get()).inflate(R.layout.item_user_list, parent, false);
        return new ViewHolder(view);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvJobNo;
        private TextView tvBranch;
        private SimpleDraweeView ivPhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvJobNo = (TextView) itemView.findViewById(R.id.tv_job_no);
            tvBranch = (TextView) itemView.findViewById(R.id.tv_branch);
            ivPhoto = (SimpleDraweeView) itemView.findViewById(R.id.iv_photo);
        }
    }

}
