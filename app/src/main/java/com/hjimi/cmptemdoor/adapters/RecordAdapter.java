package com.hjimi.cmptemdoor.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.base.BaseHeaderAdapter;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.FileUtils;

import java.io.File;
import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class RecordAdapter extends BaseHeaderAdapter<RecordAdapter.ViewHolder> {
    private SoftReference<BaseActivity> mContext;
    private List<RecordInfo> list;
    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public RecordAdapter(BaseActivity context) {
        mContext = new SoftReference<>(context);
        list = new ArrayList<>();
    }

    public void setList(List<RecordInfo> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addList(List<RecordInfo> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    protected int getAdvanceViewType(int position) {
        return position;
    }

    @Override
    protected int getAdvanceItemId(int position) {
        return 0;
    }

    @Override
    protected int getAdvanceCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override
    protected void onBindAdvanceViewHolder(final RecyclerView.ViewHolder holder, final int i) {

        ((ViewHolder) holder).tvName.setText(FileUtils.getContent(list.get(i).getName(), 10));
        ((ViewHolder) holder).tvJobNo.setText(FileUtils.getContent(list.get(i).getJobNo(), 10));

        ((ViewHolder) holder).tvBranch.setText(list.get(i).getBranch());
        ((ViewHolder) holder).tvTime.setText(format.format(list.get(i).getTime()));
//        if (AppConstants.MODE_SET_MEAL.equals(list.get(i).getPayMode())) {
//            ((ViewHolder) holder).tvMode.setText(Utils.getStringByValues(R.string.pay_mode_2));
//        } else {
//            ((ViewHolder) holder).tvMode.setText(Utils.getStringByValues(R.string.amount));
//        }
//        ((ViewHolder) holder).tvAmount.setText(list.get(i).getAmount() + "元");
        FileUtils.loadLocalPicture(((ViewHolder) holder).ivPhoto, ImiNrApplication.getApplication().getFilesDir().getAbsolutePath() + File.separator + list.get(i).getPhoto());
    }


    @Override
    protected ViewHolder onCreateAdvanceViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext.get()).inflate(R.layout.item_record, parent, false);
        return new ViewHolder(view);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvJobNo;
        private TextView tvBranch;
        private TextView tvTime;
        private TextView tvAmount;
        private TextView tvMode;
        private SimpleDraweeView ivPhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvJobNo = (TextView) itemView.findViewById(R.id.tv_job_no);
            tvBranch = (TextView) itemView.findViewById(R.id.tv_branch);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvAmount = (TextView) itemView.findViewById(R.id.tv_amount);
            tvMode = (TextView) itemView.findViewById(R.id.tv_mode);
            ivPhoto = (SimpleDraweeView) itemView.findViewById(R.id.iv_photo);
        }
    }

}
