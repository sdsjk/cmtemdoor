package com.hjimi.cmptemdoor.bean;

import com.hjimi.cmptemdoor.net.BaseResult;

public class DateInfo extends BaseResult {
    private String serverCert;
    private String hmac;

    public String getServerCert() {
        return serverCert;
    }

    public void setServerCert(String serverCert) {
        this.serverCert = serverCert;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }
}
