package com.hjimi.cmptemdoor.bean;

public class FaceUpdataInfo {
    private boolean isOnline;
    private String csvUrl;
    private long faceSetVerStart;//设备当前的人脸库版本
    private long faceSetVerEnd;//csv文件对应的人脸库版本
    private long faceSetId;
    private boolean isBindNewFaceSet; // true表示绑定新的人脸库，false表示基于旧的人脸库升级，默认值为false

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getCsvUrl() {
        return csvUrl;
    }

    public void setCsvUrl(String csvUrl) {
        this.csvUrl = csvUrl;
    }

    public long getFaceSetVerStart() {
        return faceSetVerStart;
    }

    public void setFaceSetVerStart(long faceSetVerStart) {
        this.faceSetVerStart = faceSetVerStart;
    }

    public long getFaceSetVerEnd() {
        return faceSetVerEnd;
    }

    public void setFaceSetVerEnd(long faceSetVerEnd) {
        this.faceSetVerEnd = faceSetVerEnd;
    }

    public long getFaceSetId() {
        return faceSetId;
    }

    public void setFaceSetId(long faceSetId) {
        this.faceSetId = faceSetId;
    }

    public boolean isBindNewFaceSet() {
        return isBindNewFaceSet;
    }

    public void setBindNewFaceSet(boolean bindNewFaceSet) {
        isBindNewFaceSet = bindNewFaceSet;
    }
}
