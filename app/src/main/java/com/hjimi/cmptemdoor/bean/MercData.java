package com.hjimi.cmptemdoor.bean;

import com.hjimi.cmptemdoor.net.BaseResult;

public class MercData extends BaseResult {
    private String merchantId;
    private String privateKey;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
