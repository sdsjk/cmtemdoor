package com.hjimi.cmptemdoor.bean;

import com.hjimi.cmptemdoor.net.BaseResult;

public class MercInfo extends BaseResult {
    private MercData data;
    private String hmac;

    public MercData getData() {
        return data;
    }

    public void setData(MercData data) {
        this.data = data;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }
}
