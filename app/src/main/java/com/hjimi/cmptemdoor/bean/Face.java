package com.hjimi.cmptemdoor.bean;

import java.util.Arrays;

/**
 * Created by liyuan on 2018/1/11.
 */

public class Face {

    /**
     * 是否为活体
     */
    float liveness;


    private int[] faceRect;

    String result;

    private float[] feaceFeature;

    String shoot;
    public Face(int[] faceRect, float liveness) {
        this.liveness = liveness;
        this.faceRect = faceRect;
    }

    public float getLiveness() {
        return liveness;
    }

    public void setLiveness(float liveness) {
        this.liveness = liveness;
    }

    public int[] getFaceRect() {
        return faceRect;
    }

    public void setFaceRect(int[] faceRect) {
        this.faceRect = faceRect;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public float[] getFeaceFeature() {
        return feaceFeature;
    }

    public void setFeaceFeature(float[] feaceFeature) {
        this.feaceFeature = feaceFeature;
    }

    public String getShoot() {
        return shoot;
    }

    public void setShoot(String shoot) {
        this.shoot = shoot;
    }

    @Override
    public String toString() {
        return "Face{" +
                "liveness=" + liveness +
                ", faceRect=" + Arrays.toString(faceRect) +
                '}';
    }
}
