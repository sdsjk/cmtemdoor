package com.hjimi.cmptemdoor.bean;

import com.hjimi.cmptemdoor.net.BaseResult;

public class PayInfo extends BaseResult {
    private PayData data;
    private String serverCert;
    private String hmac;

    public PayData getData() {
        return data;
    }

    public void setData(PayData data) {
        this.data = data;
    }

    public String getServerCert() {
        return serverCert;
    }

    public void setServerCert(String serverCert) {
        this.serverCert = serverCert;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }
}
