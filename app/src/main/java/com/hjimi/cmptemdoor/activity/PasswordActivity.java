package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;
import android.view.WindowManager;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.view.PasswordView;

public class PasswordActivity extends BaseActivity {
    private static final String TAG = "PasswordActivity";

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_password);
        PasswordView passwordView = PasswordView.newInstance(this);
//        new MainPresenter(this, mainView);
    }

    @Override
    protected void loadData() {
        super.loadData();
        mImmersionBar.keyboardEnable(true, WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager
                .LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE).init();
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
