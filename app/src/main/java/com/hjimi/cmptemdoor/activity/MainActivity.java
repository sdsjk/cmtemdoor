package com.hjimi.cmptemdoor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.cmiot.huadong.andshi.mqtt.MqttEvent;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.bean.FaceUpdataInfo;
import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.entity.BusInfo;
import com.hjimi.cmptemdoor.interfaces.QueryFaceRecordCallBack;
import com.hjimi.cmptemdoor.presenter.MainPresenter;
import com.hjimi.cmptemdoor.utils.AppConstantInfoUtils;
import com.hjimi.cmptemdoor.utils.PackageUtils;
import com.hjimi.cmptemdoor.utils.QueryFaceRecordUtils;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.SpeechUtils;
import com.hjimi.cmptemdoor.utils.StringUtils;
import com.hjimi.cmptemdoor.utils.TimerUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.dbutils.FaceRecordInfoDBUtil;
import com.hjimi.cmptemdoor.utils.download.DownLoadUtils;
import com.hjimi.cmptemdoor.utils.mqtt.FdfsUtils;
import com.hjimi.cmptemdoor.utils.mqtt.MqttMessageQuene;
import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.view.MainView;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    private MainView mainView;
    private boolean isMqttConnect = false;
    private boolean isFirst = true;//第一次进入该页面，mqtt连接成功后需要请求人脸库
    private boolean isInUpdateFace = false;//是否在已同步完后，又有手动下发更新
    private MainPresenter presenter;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        Log.e(TAG, "MainActivity initViews: ");
        setContentView(R.layout.activity_main);
        AppConstantInfoUtils.setAppConstantInfoParms();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        MqttUtils.regist();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mainView = MainView.newInstance(this);
        presenter = new MainPresenter(this, mainView);
        if (mainView != null) {
            mainView.setActiveBindState(AppConstantInfo.getInstance().isActive(), AppConstantInfo.getInstance().isBind());
        }
    }

    @Override
    public Object getTag() {
        return TAG;
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "MainActivity onStart: ");
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        Log.e(TAG, "MainActivity onStop: ");
        super.onStop();
        SpeechUtils.stopTTS();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (Utils.isDoubleClick()) {
                ImiNrApplication.getApplication().exitApp();
            }
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "MainActivity onResume: ");
        if (mainView != null) {
            mainView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "MainActivity onPause: ");
        if (mainView != null) {
            mainView.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "MainActivity onPause: ");
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        MqttUtils.stop();
        if (presenter != null) {
            presenter.onDestroy();
        }
        if (mainView != null) {
            mainView.onDestroy();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BusInfo event) {
        switch (event.getKey()) {
//            case AppConstants.PAY_MODE:
//                if (mainView != null) {
//                    mainView.changePayMode(event.getMsg());
//                }
//                break;
            case AppConstants.MQTT_REGIST_SUCCESS:
                //mqtt注册登录成功
                MqttUtils.start();
                break;
            case AppConstants.DOWNLOAD_CSV_FAIL:
                //下载csv文件失败
                faceUpdataInfo = null;
            case AppConstants.TO_MAKE_FEATURE:
                //csv内部文件下载完成，开始生成特征值
                if (mainView != null) {
                    if (mainView.isMakeDataFinish()) {
                        Log.e(TAG, "onMessageEvent: setRetryUpdateFace");
                        mainView.setRetryUpdateFace();
                    } else {
                        Log.e(TAG, "onMessageEvent: setFaceDataOk");
                        mainView.setFaceDataOk();
                    }
                }
                break;
            case AppConstants.SYNCHRONOUS_PROGRESS:
                //更新进度
                if (mainView != null) {
                    mainView.synchronousProgress(event.getMsg());
                }
                break;
            case AppConstants.NET_WORK_CONNECTED:
                //网络连接
                boolean isNetConnect = (boolean) event.getObj();
                if (isNetConnect) {
//                    presenter.diningMerc();
                    MqttUtils.start();
                } else {
                    TimerUtils.getInstance().cancelTimer();
                }
                break;
            case AppConstants.UP_FACE_EXCEPTION:
                //上报刷脸信息异常
                if (presenter != null) {
                    presenter.setUping(false);
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMqttAcceptedOrRejectedEvent(MqttEvent<Integer> event) {
        Logger.e("handleMqttAcceptedOrRejectedEvent:" + event.getCode());
        switch (event.getCode()) {
            case 0:
                //MQTT SAAS已连接
                if (!isMqttConnect) {
                    isMqttConnect = true;
                    mqttHadConnect();
                }

                break;
            case 1:
                //MQTT SAAS断连接、异常
                if (isMqttConnect) {
                    isMqttConnect = false;
                }
                break;
            case 2:
                //上报消息失败
                Integer errorId = event.getData();
                MqttMessageQuene.getInstance().updateQueneFail(errorId);
                MqttMessageQuene.getInstance().retryPublish(errorId);
                break;
            case 3:
                //上报消息成功
                Integer messageId = event.getData();
                MqttMessageQuene.getInstance().removeQuene(messageId);
                break;
        }
    }

    private FaceUpdataInfo faceUpdataInfo;

    public FaceUpdataInfo getFaceUpdataInfo() {
        return faceUpdataInfo;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMqttEvent(MqttEvent<JsonObject> event) {
        Log.e(TAG, "handleMqttEvent: "+event.getData() );
        switch (event.getCode()) {
            case 104:
                Log.e(TAG, "handleMqttEvent: --------------------104" );
                //下载csv文件
                //{"head":{"cmd":104,"version":"1"},"body":{"faceSetVerStart":0,"faceSetVerEnd":1,"csvUrl":"http://172.16.1.40/sf100/M00/00/9A/rBABKF3uDu-AZOc0AABU8mWAlOY348.csv","faceSetId":56,"isOnline":false,"isBindNewFaceSet":true}}
                if (StringUtils.isEmpty(AppConstantInfo.getInstance().getFdfsToken())) {
                    //fdfstoken为空
                    MqttUtils.faceSetUpgradeFdfsSecretKeyMiss();
                    return;
                }

                faceUpdataInfo = new GsonBuilder().create().fromJson(event.getData().toString(), FaceUpdataInfo.class);

                if (!faceUpdataInfo.isBindNewFaceSet() && SpUtil.getLong(PreferenceConstants.FACE_SET_VER, 0) != faceUpdataInfo.getFaceSetVerStart()) {
                    //当前人脸库版本不等于下发人脸库 开始版本  上报10001  版本不对
                    MqttUtils.faceSetUpgradeVersionError((int) AppConstantInfo.getInstance().getFaceSetVer());
                    return;
                }
                //判断是否已同步了，如果在app已同步完后，又下发，则不处理，直接上报结果；否则以后下发会提示正在同步，无法下发
                if (mainView != null) {
                    if (mainView.isMakeDataFinish()) {
                        //中途进行人脸库主动下发了
                        //如果此时有检测、支付过程中，等操作完后，在进行同步
                        if (!AppConstantInfo.getInstance().isActive()) {
                            //等激活后，在进行同步
                            isInUpdateFace = true;
                        } else {
                            if (mainView.ismIsIdentify()) {
                                //等结束后，在进行同步
                                isInUpdateFace = true;
                            } else {
                                Intent intent = new Intent(this, MainActivity.class);
                                intent.putExtra(AppConstants.AGAIN_TO_MAINACTIVITY_TYPE, AppConstants.AGAIN_MAINACTIVITY_TYPE_CSV);
                                startActivity(intent);
                            }
                        }

                        return;
                    } else {
                        DownLoadUtils.getInstance().downloadCSV(FdfsUtils.getUrl(faceUpdataInfo.getCsvUrl(), AppConstantInfo.getInstance().getFdfsToken()));
                    }
                }

                break;
            case 113:
                //{"head":{"cmd":113,"version":"1"},"body":{"fdfsToken":{"val":"FastDFS1234567890"},"devPara":{"isVoiceOn":true,"similarityThreshold":80},"isActive":true}}

                boolean isActive = true;//如果不传值，无值，默认为true
                try {
                    isActive = event.getData().get(AppConstants.IS_ACTIVE).getAsBoolean();
                } catch (Exception e) {
                } finally {
                    SpUtil.putBoolean(PreferenceConstants.IS_ACTIVE, isActive);
                    AppConstantInfo.getInstance().setActive(isActive);
                }

                try {
                    //不一定每次都要有值
                    //获取并保存fafstoken
                    String fdfsToken = event.getData().getAsJsonObject(AppConstants.FDFS_TOKEN).get(AppConstants.VAL).toString();
                    fdfsToken = fdfsToken.replace("\"", "");
                    SpUtil.putString(PreferenceConstants.FDFS_TOKEN, fdfsToken);
                    AppConstantInfo.getInstance().setFdfsToken(fdfsToken);
                } catch (Exception e) {
                }

                boolean isVoiceOn = true;
                boolean isbind = true;
                try {
                    JsonObject devPara = event.getData().getAsJsonObject(AppConstants.DEV_PARA);
                    try {
                        // 不一定有值，有值修改，无值，默认为true
                        isVoiceOn = devPara.get(AppConstants.IS_VOICE_ON).getAsBoolean();
                    } catch (Exception e) {
                    }

                    try {
                        //不一定有值，有值修改，无值不变
                        int similarityThreshold = devPara.get(AppConstants.SIMILARITY_THRESHOLD).getAsInt();
                        AppConstantInfo.getInstance().setCompareThreshold(similarityThreshold);
                        SpUtil.putFloat(PreferenceConstants.FACE_COMPARE_THRESHOLD, similarityThreshold);
                    } catch (Exception e) {
                    }

                    try {
                        // 不一定有值，有值修改，无值，默认为true
                        isbind = devPara.get(AppConstants.IS_BIND).getAsBoolean();
                    } catch (Exception e) {
                    }


                } catch (Exception e) {
                    Log.e(TAG, "下发参数不全 解析错误");
                } finally {
                    AppConstantInfo.getInstance().setVoiceOpen(isVoiceOn);
                    SpUtil.putBoolean(PreferenceConstants.VOICE_OPEN, isVoiceOn);
                    SpUtil.putBoolean(PreferenceConstants.IS_BIND, isbind);
                    AppConstantInfo.getInstance().setBind(isbind);

                    ToastUtils.showToast(this, Utils.getStringByValues(R.string.config_had_update));
                }

                //下发配置之后，设备上报配置信息
                MqttUtils.devConfigUpload(false);

                if (mainView != null) {
                    mainView.setActiveBindState(AppConstantInfo.getInstance().isActive(), AppConstantInfo.getInstance().isBind());
                }
                break;
            case 112:
                //{"head":{"cmd":112,"version":"1"},"body":{"res":200,"faceCommitId":0}}       "faceCommitId": //刷脸记录id（设备侧）
                //人脸状态上报成功回执
                long faceCommitId = event.getData().get(AppConstants.FACE_COMMIT_ID).getAsLong();
                if (faceCommitId != -1) {
                    if (FaceRecordInfoDBUtil.queryFaceRecordInfoById(faceCommitId) != null) {
                        FaceRecordInfoDBUtil.deleteFaceRecordInfoById(faceCommitId);
                        Log.e("faceRecordUpload: ", "收到回直 删除 ：" + faceCommitId);

                        QueryFaceRecordUtils.getInstance().queryFaceRecord(new QueryFaceRecordCallBack() {
                            @Override
                            public void queryFaceRecord(FaceRecordInfo info) {
                                if (info != null) {
                                    MqttUtils.faceRecordUpload(info);
                                    Log.e("faceRecordUpload: ", "收到回直 上报下一条" + info.getFaceCommitId());
                                } else {
                                    presenter.setUping(false);
                                    Log.e("faceRecordUpload: ", "收到回直 无下一条");
                                }
                            }
                        });
                    }
                }


                break;
            case 118:
                //{"head":{"cmd":118,"version":"1"},"body":{"fail":{"res":20001}}}      20001:人脸库版本已经是最新
                //人脸库主动升级请求平台异常响应通知
                int res = event.getData().getAsJsonObject(AppConstants.FAIL).get(AppConstants.RES).getAsInt();
                if (res == 20001) {
                    ToastUtils.showToast(ImiNrApplication.getApplication(), Utils.getStringByValues(R.string.face_data_latest_version));
                } else {
                    ToastUtils.showToast(ImiNrApplication.getApplication(), Utils.getStringByValues(R.string.face_data_check_version_error));
                }
                if (mainView != null) {
                    mainView.setFaceDataOk();
                }
                break;
            case 106:
                //平台通知设备更新app软件
                //{  "head": {      "version": 1,      "cmd": 106  }，  "body":{      "appUrl": "www.xxx.com/groupxxx",//app下载地址      "appVersionNew": 0.2,//app升级后的版本号      "appVersionCur": 0.1,//当前运行的app版本号      "updateTime": 2019-10-31 23:00:00//更新时间  }}
                //TODO:确定appVersionNew 是版本号 还是版本名称  需要判断下是否要进行升级
                int appVersionNew = event.getData().get("").getAsInt();
                int versionCode = PackageUtils.getVersionCode(this);
                if (appVersionNew > versionCode) {
                    if (!mainView.ismIsIdentify()) {
                        String appUrl = event.getData().get(AppConstants.APP_URL).getAsString();
                        SpUtil.putInt(PreferenceConstants.APP_VERSION, appVersionNew);
                        DownLoadUtils.getInstance().downloadAPK(appUrl);
                    }
                } else {
                    //直接上报升级成功
                    MqttUtils.upadataAppUpload();
                }
                break;
        }
    }

    /**
     * 网络重新连接后需要上报某些记录
     */
    private void mqttHadConnect() {
        if (isFirst) {
            //TODO:主动获取fadstoken
            if (StringUtils.isEmpty(AppConstantInfo.getInstance().getFdfsToken())) {
                //fdfstoken为空的话，等待下发 cmd=113
                Log.e(TAG, "dfstoken为空");
            } else {
                Log.e(TAG, "requestFaceDataUpdate" );
                MqttUtils.requestFaceDataUpdate();
            }

            //判断是否升级成功 并上报
            int anInt = SpUtil.getInt(PreferenceConstants.APP_VERSION, -1);
            if (anInt != -1) {
                if (anInt == PackageUtils.getVersionCode(this)) {
                    MqttUtils.upadataAppUpload();
                } else {
                    //上报升级失败
                }
                SpUtil.putInt(PreferenceConstants.APP_VERSION, -1);
            }
            isFirst = false;
        }
        // 如有未上传刷脸记录 全部上传
        presenter.faceRecordsUpload();
        //网络连接后，上报配置信息
        TimerUtils.getInstance().startTimer();
        //重新上报发布失败的
        MqttMessageQuene.getInstance().retryAll();
    }

    public boolean isInUpdateFace() {
        return isInUpdateFace;
    }

    public void setInUpdateFace(boolean inUpdateFace) {
        isInUpdateFace = inUpdateFace;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            String type = intent.getStringExtra(AppConstants.AGAIN_TO_MAINACTIVITY_TYPE);
            if (StringUtils.isEmpty(type)) {
                return;
            }
            switch (type) {
                case AppConstants.AGAIN_MAINACTIVITY_TYPE_CSV:
                    if (mainView != null) {
                        //同步人脸库
                        mainView.handleInUpdate();
                    }
                    break;
                case AppConstants.AGAIN_MAINACTIVITY_TYPE_ACTIVE:
                    //激活
                    break;
                case AppConstants.EXCEPTION_TO_RESTART:
                    //异常捕获后重启
                    AppConstantInfoUtils.setAppConstantInfoParms();
                    break;
            }


        }
    }
}
