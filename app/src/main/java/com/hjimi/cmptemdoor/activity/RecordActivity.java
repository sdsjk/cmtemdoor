package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.presenter.RecordPresenter;
import com.hjimi.cmptemdoor.view.RecordView;

public class RecordActivity extends BaseActivity {
    private static final String TAG = "RecordActivity";
    private RecordView recordView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_record);
        recordView = RecordView.newInstance(this);
        new RecordPresenter(this, recordView);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
