package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.view.MenuView;

public class MenuActivity extends BaseActivity {
    private static final String TAG = "MenuActivity";
    private MenuView menuView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_menu);
        menuView = MenuView.newInstance(this);
//        new MainPresenter(this, mainView);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
