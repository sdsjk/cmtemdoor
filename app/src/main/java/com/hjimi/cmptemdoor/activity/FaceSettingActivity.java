package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.view.FaceSettingView;

public class FaceSettingActivity extends BaseActivity {
    private static final String TAG = "FaceSettingActivity";
    private FaceSettingView faceSettingView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_face_setting);
        faceSettingView = FaceSettingView.newInstance(this);
//        new MainPresenter(this, mainView);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
