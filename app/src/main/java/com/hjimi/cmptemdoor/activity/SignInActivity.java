package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.view.SignInView;

public class SignInActivity extends BaseActivity {
    private static final String TAG = "SignInActivity";
    private SignInView signInView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_sign_in);
        signInView = SignInView.newInstance(this);
//        new MainPresenter(this, mainView);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void loadData() {
        super.loadData();
        mImmersionBar.keyboardEnable(true, WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager
                .LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE).init();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (Utils.isDoubleClick()) {
                ImiNrApplication.getApplication().exitApp();
            }
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }


}
