package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.presenter.UserListPresenter;
import com.hjimi.cmptemdoor.view.UserListView;

public class UserListActivity extends BaseActivity {
    private static final String TAG = "UserListActivity";
    private UserListView userListView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_user_list);
        userListView = UserListView.newInstance(this);
        new UserListPresenter(this, userListView);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
