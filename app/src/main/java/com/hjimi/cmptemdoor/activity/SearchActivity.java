package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.presenter.SearchPresenter;
import com.hjimi.cmptemdoor.view.SearchView;

public class SearchActivity extends BaseActivity {
    private static final String TAG = "SearchActivity";
    private SearchView searchView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_search);
        searchView = SearchView.newInstance(this);
        new SearchPresenter(this, searchView);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
