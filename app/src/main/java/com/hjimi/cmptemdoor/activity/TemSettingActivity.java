package com.hjimi.cmptemdoor.activity;

import android.os.Bundle;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.view.TemSettingView;

public class TemSettingActivity extends BaseActivity {
    private static final String TAG = "TemSettingActivity";
    private TemSettingView temSettingView;

    @Override
    protected void initViews(Bundle savedInstanceState) {
        setContentView(R.layout.activity_tem_setting);
        temSettingView = TemSettingView.newInstance(this);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mImmersionBar.transparentNavigationBar().init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
