package com.hjimi.cmptemdoor.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.interfaces.JudgeNetCallBack;
import com.hjimi.cmptemdoor.presenter.SplashPresenter;
import com.hjimi.cmptemdoor.utils.JudgeNetUtils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.view.SplashView;

import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;


public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";

    private SplashView splashView;
    private SplashPresenter splashPresenter;
    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };


    @Override
    protected void initViews(Bundle savedInstanceState) {
        setSplashActivity();
        setContentView(R.layout.activity_splash);
        requestPermissions();
    }

    private void requestPermissions() {
        PermissionGen.with(this)
                .addRequestCode(AppConstants.PERMISSION_REQUEST_CODE)
                .permissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        for (int i = 0; i < grantResults.length; i++) {
            boolean isTip = ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permissions[i]);
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                if (!isTip) {//用户彻底禁止弹出权限请求
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, AppConstants.PERMISSION_APP_SETTING_REQUEST);
                    break;
                } else {//用户没有彻底禁止弹出权限请求
                    // requestPermissions();
                    ImiNrApplication.getApplication().exitApp();
                }
                return;
            }
        }
    }

    @PermissionSuccess(requestCode = AppConstants.PERMISSION_REQUEST_CODE)
    public void successRequestPermission() {
        if (splashView == null || splashPresenter == null) {
            splashView = new SplashView(this);
            splashPresenter = new SplashPresenter(this, splashView);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult: " + requestCode + "--" + resultCode);
        if (requestCode == AppConstants.PERMISSION_APP_SETTING_REQUEST) {
            //用户跳到Setting界面也有可能没有去开启权限
            for (String PERMISSION : PERMISSIONS) {
                if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(this, PERMISSION)) {
                    ImiNrApplication.getApplication().exitApp();
                    return;
                }
            }
            successRequestPermission();
        }
        if (requestCode == AppConstants.WIFI_REQUEST_CODE) {
            JudgeNetUtils.getInstance().judgeNetConnect(new JudgeNetCallBack() {
                @Override
                public void judgeNet(boolean isNetConnect) {
                    if (isNetConnect) {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                        SplashActivity.this.finish();
                    } else {
                        ToastUtils.showToast(SplashActivity.this, "请链接有效网络");
                        JudgeNetUtils.getInstance().toWifiSetting(SplashActivity.this);
                    }
                }
            });
        }
    }

    /**
     * 返回要申请的敏感权限
     *
     * @return
     */
    public static String[] getPERMISSIONS() {
        return PERMISSIONS;
    }

    @Override
    public Object getTag() {
        return TAG;
    }

}
