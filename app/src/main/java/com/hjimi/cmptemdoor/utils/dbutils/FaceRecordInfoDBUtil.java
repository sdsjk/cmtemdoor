package com.hjimi.cmptemdoor.utils.dbutils;

import android.util.Log;

import com.hjimi.cmptemdoor.databean.DaoMaster;
import com.hjimi.cmptemdoor.databean.DaoSession;
import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.databean.FaceRecordInfoDao;

import java.util.List;

public class FaceRecordInfoDBUtil {

    public static long insertFaceRecordInfo(FaceRecordInfo faceRecordInfo) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        FaceRecordInfoDao dao = daoSession.getFaceRecordInfoDao();
        long insert = dao.insert(faceRecordInfo);
        Log.e("insertFaceRecordInfo: ","---"+insert );
        FaceRecordInfo info = queryFaceRecordInfoById(insert);
        info.setFaceCommitId(insert);
        dao.update(info);
        return insert;
    }

    public static void insertFaceRecordInfos(List<FaceRecordInfo> recordInfos) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        FaceRecordInfoDao dao = daoSession.getFaceRecordInfoDao();
        dao.insertInTx(recordInfos);

    }

    /**
     * @param id
     * @param recordInfo
     */
    public static void updateFaceRecordInfo(long id, FaceRecordInfo recordInfo) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        FaceRecordInfoDao dao = daoSession.getFaceRecordInfoDao();

        if (queryFaceRecordInfoById(id) != null) {
            dao.update(recordInfo);
        }
    }

    public static FaceRecordInfo queryFaceRecordInfoById(long id) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        FaceRecordInfoDao dao = daoSession.getFaceRecordInfoDao();
        return dao.queryBuilder()
                .where(FaceRecordInfoDao.Properties.Id.eq(id)).unique();

    }


    public static FaceRecordInfo queryFirstFaceRecordInfo() {
        List<FaceRecordInfo> faceRecordInfos = queryAllFaceRecordInfo();
        return (faceRecordInfos == null || faceRecordInfos.size() == 0) ? null : faceRecordInfos.get(0);
    }


    public static List<FaceRecordInfo> queryAllFaceRecordInfo() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        FaceRecordInfoDao dao = daoSession.getFaceRecordInfoDao();
        return dao.queryBuilder().orderAsc(FaceRecordInfoDao.Properties.Id).offset(0).limit(1).list();
    }

    public static void deleteFaceRecordInfoById(long id) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        FaceRecordInfoDao dao = daoSession.getFaceRecordInfoDao();
        dao.deleteByKey(id);

    }


}
