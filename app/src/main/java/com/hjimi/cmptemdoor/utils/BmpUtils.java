package com.hjimi.cmptemdoor.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.ThumbnailUtils;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * * @author  fenghl
 * created at 2018/10/15 13:57
 */


public class BmpUtils {
    private static final String TAG = BmpUtils.class.getSimpleName();

    /**
     * 把一个View的对象转换成bitmap
     */
    static Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);

        //能画缓存就返回false
        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);
        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            Log.e("BtPrinter", "failed getViewBitmap(" + v + ")", new RuntimeException());
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);
        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);
        return bitmap;
    }

    /**
     * 将彩色图转换为黑白图
     *
     * @return 返回转换好的位图
     */
    public static Bitmap convertToBlackWhite(Bitmap bmp) {
        int width = bmp.getWidth(); // 获取位图的宽
        int height = bmp.getHeight(); // 获取位图的高
        int[] pixels = new int[width * height]; // 通过位图的大小创建像素点数组

        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int alpha = 0xFF << 24;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int grey = pixels[width * i + j];

                int red = ((grey & 0x00FF0000) >> 16);
                int green = ((grey & 0x0000FF00) >> 8);
                int blue = (grey & 0x000000FF);

                grey = (int) (red * 0.3 + green * 0.59 + blue * 0.11);
                grey = alpha | (grey << 16) | (grey << 8) | grey;
                pixels[width * i + j] = grey;
            }
        }
        Bitmap newBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        newBmp.setPixels(pixels, 0, width, 0, 0, width, height);

        Bitmap resizeBmp = ThumbnailUtils.extractThumbnail(newBmp, 380, 460);
        return resizeBmp;
    }


    protected static void writeWord(FileOutputStream stream, int value) throws IOException {
        byte[] b = new byte[2];
        b[0] = (byte) (value & 0xff);
        b[1] = (byte) (value >> 8 & 0xff);
        stream.write(b);
    }

    protected static void writeDword(FileOutputStream stream, long value) throws IOException {
        byte[] b = new byte[4];
        b[0] = (byte) (value & 0xff);
        b[1] = (byte) (value >> 8 & 0xff);
        b[2] = (byte) (value >> 16 & 0xff);
        b[3] = (byte) (value >> 24 & 0xff);
        stream.write(b);
    }

    protected static void writeLong(FileOutputStream stream, long value) throws IOException {
        byte[] b = new byte[4];
        b[0] = (byte) (value & 0xff);
        b[1] = (byte) (value >> 8 & 0xff);
        b[2] = (byte) (value >> 16 & 0xff);
        b[3] = (byte) (value >> 24 & 0xff);
        stream.write(b);
    }

    /**
     * @方法描述 将RGB字节数组转换成Bitmap，
     */
    static public Bitmap rgb2Bitmap(byte[] data, int width, int height) {
        int[] colors = convertByteToColor(data);    //取RGB值转换为int数组
        if (colors == null) {
            return null;
        }

        Bitmap bmp = Bitmap.createBitmap(colors, 0, width, width, height,
                Bitmap.Config.ARGB_8888);
        return bmp;
    }

    /**
     * @方法描述 将RGB字节数组转换成Bitmap，
     */
    synchronized static public Bitmap rgb2Bitmap(ByteBuffer buff, int width, int height) {

        int len = buff.limit() - buff.position();
        // Logger.d("rgb长度: "+len);
        byte[] bytes = null;
        if (buff.isReadOnly()) {
            Log.e("BmpUtils", " bytebuffer2Bitmap failed");
            return null;
        } else {
            //data.get(bytes);
            bytes = new byte[len];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = buff.get();

            }
            // byteBuffer.get(bytes,0,1);
        }
        buff.flip();

        // buff.reset();
        int[] colors = convertByteToColor(bytes);    //取RGB值转换为int数组
        if (colors == null) {
            return null;
        }

        Bitmap bmp = Bitmap.createBitmap(colors, 0, width, width, height,
                Bitmap.Config.ARGB_8888);
        return bmp;
    }

    /**
     * 將ByteBuffer转成Bitmap
     *
     * @param data
     * @param width
     * @param height
     * @return
     */
    synchronized static public Bitmap bytebuffer2Bitmap(ByteBuffer data, int width, int height) {

        int len = data.limit() - data.position();
        // Logger.d("rgb长度: "+len);
        byte[] bytes = null;
        if (data.isReadOnly()) {
            Log.e("BmpUtils", " bytebuffer2Bitmap failed");
            return null;
        } else {
            //data.get(bytes);
            bytes = new byte[len];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = data.get();

            }
            // byteBuffer.get(bytes,0,1);
        }
        data.flip();

/*        Bitmap stitchBmp = Bitmap.createBitmap(width, height, type);
        stitchBmp.copyPixelsFromBuffer(data);
        imageView.setImageBitmap(stitchBmp);*/

        YuvImage yuvimage = new YuvImage(bytes, ImageFormat.FLEX_RGB_888, width, height, null); //20、20分别是图的宽度与高度
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);//80--JPG图片的质量[0-100],100最高
        byte[] jdata = baos.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(jdata, 0, jdata.length);
        return bitmap;
    }


    // 将一个byte数转成int
    // 实现这个函数的目的是为了将byte数当成无符号的变量去转化成int
    public static int convertByteToInt(byte data) {

        int heightBit = (int) ((data >> 4) & 0x0F);
        int lowBit = (int) (0x0F & data);
        return heightBit * 16 + lowBit;
    }


    // 将纯RGB数据数组转化成int像素数组
    public static int[] convertByteToColor(byte[] data) {
        int size = data.length;
        if (size == 0) {
            return null;
        }

        int arg = 0;
        if (size % 3 != 0) {
            arg = 1;
        }

        // 一般RGB字节数组的长度应该是3的倍数，
        // 不排除有特殊情况，多余的RGB数据用黑色0XFF000000填充
        int[] color = new int[size / 3 + arg];
        int red, green, blue;
        int colorLen = color.length;
        if (arg == 0) {
            for (int i = 0; i < colorLen; ++i) {
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);

                // 获取RGB分量值通过按位或生成int的像素值
                color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
            }
        } else {
            for (int i = 0; i < colorLen - 1; ++i) {
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);
                color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
            }

            color[colorLen - 1] = 0xFF000000;
        }

        return color;
    }

    public static Bitmap uploadNativeImage(String pathformcam) {
        if (pathformcam != null) {
            Bitmap nativeImage = BmpUtils.getBitmapFromFileLimitSize(pathformcam,
                    640);
            return nativeImage;
        } else {
            return null;
        }
    }

    /**
     * 从文件中解图 解大图内存不足时尝试10次, samplesize增大
     *
     * @param filePath
     * @param max      宽或高的最大值, <= 0 , 能解多大解多大, > 0, 最大max, 内存不足解更小
     * @return
     */
    public static Bitmap getBitmapFromFileLimitSize(String filePath, int max) {
        if (TextUtils.isEmpty(filePath) || !new File(filePath).exists()) {
            return null;
        }
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;

        if (max > 0) {
            options.inJustDecodeBounds = true;
            // 获取这个图片的宽和高
            bm = BitmapFactory.decodeFile(filePath, options);
            options.inJustDecodeBounds = false;
//            float blW = (float) options.outWidth / max;
//            float blH = (float) options.outHeight / max;
//
//            if (blW > 1 || blH > 1) {
//                if (blW > blH) {
//                    options.inSampleSize = (int) (blW + 0.9f);
//                } else {
//                    options.inSampleSize = (int) (blH + 0.9f);
//                }
//            }
            /*
            将需要压缩的尺寸为 850  600
             */
            options.inSampleSize = calculateInSampleSize(options, 700, 600);
        }
        int i = 0;
        while (i <= 10) {
            i++;
            try {
                bm = BitmapFactory.decodeFile(filePath, options);
                //LogUtils.d("getBitmapFromFileLimitSize压缩后的图片的宽高", "宽==" + options.outWidth + "\t高==" + options.outHeight);
                System.out.println("压缩后的图片的宽高" + "宽==" + options.outWidth + "\t高==" + options.outHeight);
                break;
            } catch (OutOfMemoryError e) {
                options.inSampleSize++;
                e.printStackTrace();
            }
        }
        return compressImage(bm);
    }

    /**
     * 压缩图片
     *
     * @param options   BitmapFactory.Options
     * @param reqWidth  要求的宽度
     * @param reqHeight 要求的高度
     * @return 返回 bitmap
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        //LogUtils.d("取得图片的长宽--->  ","宽--->"+width+"  高--->   "+height);
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = (int) Math.ceil((float) height / (float) reqHeight);
            final int widthRatio = (int) Math.ceil((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        //质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        int options = 90;

        //循环判断如果压缩后图片是否大于80kb,大于继续压缩
        while (baos.toByteArray().length / 1024 > 100) {

            //重置baos即清空baos
            baos.reset();
            //这里压缩options%，把压缩后的数据存放到baos中
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);

            //每次都减少10
            options -= 10;
        }

        //把压缩后的数据baos存放到ByteArrayInputStream中
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());

        //把ByteArrayInputStream数据生成图片
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);
        return bitmap;
    }

    /**
     * bitmap转为base64
     *
     * @param bitmap
     * @return
     */
    public static String bitmapToBase64(Bitmap bitmap) {

        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.NO_WRAP);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
                if (bitmap != null) {
                    bitmap.recycle();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
