package com.hjimi.cmptemdoor.utils;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;

import java.util.Locale;

public class SpeechUtils {
    private static Context mContext;
    private static TextToSpeech tts;
    private static boolean isInit;
    private static String mText;

    public static void play(Context context, String text) {
        mContext = context;
        mText = text;
        if (!AppConstantInfo.getInstance().isVoiceOpen()) {
            ToastUtils.showToast(context, text);
            return;
        }
        if (tts == null || !isInit) {
            tts = new TextToSpeech(context, new listener());
        } else {
            tts.speak(text, TextToSpeech.QUEUE_ADD, null);
        }
    }

    private static class listener implements TextToSpeech.OnInitListener {

        @Override
        public void onInit(int status) {
            if (status == TextToSpeech.SUCCESS) {
                //设置播放语言
                int result = tts.setLanguage(Locale.CHINESE);
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    ToastUtils.showToast(mContext, "不支持");
                } else if (result == TextToSpeech.LANG_AVAILABLE) {
                    isInit = true;
                    //初始化成功之后才可以播放文字
                    //否则会提示“speak failed: not bound to tts engine
                    //TextToSpeech.QUEUE_ADD会将加入队列的待播报文字按顺序播放
                    //TextToSpeech.QUEUE_FLUSH会替换原有文字
                    tts.speak(mText, TextToSpeech.QUEUE_ADD, null);
                }

            } else {
                Log.e("TAG", "初始化失败");
            }

        }
    }

    public static void stopTTS() {
        if (tts != null) {
            tts.shutdown();
            tts.stop();
            tts = null;
        }
    }
}
