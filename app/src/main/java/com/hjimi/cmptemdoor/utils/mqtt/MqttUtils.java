package com.hjimi.cmptemdoor.utils.mqtt;

import android.text.TextUtils;
import android.util.Log;

import com.cmiot.huadong.andshi.mqtt.MqttManager;
import com.cmiot.huadong.andshi.mqtt.MqttRegistManager;
import com.cmiot.huadong.andshi.mqtt.MqttRequest;
import com.cmiot.huadong.andshi.mqtt.protocol.ActionEnum;
import com.cmiot.huadong.andshi.mqtt.protocol.Function;
import com.cmiot.huadong.andshi.mqtt.protocol.StringMessage;
import com.google.gson.Gson;
import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.bean.FaceUpdataInfo;
import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.entity.BusInfo;
import com.hjimi.cmptemdoor.utils.PackageUtils;
import com.hjimi.cmptemdoor.utils.PushModelToLocalUtils;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.StringUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.dbutils.UserInfoDBUtil;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.orhanobut.logger.Logger;

import org.fusesource.mqtt.client.Callback;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MqttUtils {
    private static final String MQTT_REGIST_URL = "ssl://183.230.40.32:26004";
    private static String MQTT_SERVER_URL = "ssl://";
    public static String REGIST_NAME;
    public static String REGIST_PASSWORD;
    public static String CLIENT_ID;
    private static String PASSWORD;
    private static File cerFile;
    private long faceSetVer;

    public static void regist() {
        if (StringUtils.isEmpty(REGIST_NAME)) {
            REGIST_NAME = "319733";
        }
        if (StringUtils.isEmpty(REGIST_PASSWORD)) {
            REGIST_PASSWORD = "ikXlANMWz6tb";
        }
        if (StringUtils.isEmpty(CLIENT_ID)) {
            CLIENT_ID = getDeviceSN();
        }
        MqttRegistManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).init(MQTT_REGIST_URL, CLIENT_ID, REGIST_NAME, REGIST_PASSWORD).setCallback(new MqttRegistManager.RegistCallback() {
            @Override
            public void registSuccess(String s, String s1, String s2, String s3) {
                Logger.e("mqtt注册登录成功:");
                MQTT_SERVER_URL = MQTT_SERVER_URL + s;

                PASSWORD = s2;
                cerFile = PushModelToLocalUtils.copyPemToLocal(s3);
                MqttRegistManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).stop();
                BusInfo info = new BusInfo();
                info.setKey(AppConstants.MQTT_REGIST_SUCCESS);
                EventBus.getDefault().post(info);

            }

            @Override
            public void registFail(String s) {
                Logger.e("mqtt注册登录失败:" + s);

            }
        }).regist();
    }

    //连接至行业平台
    public static void start() {
        MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).start(MQTT_SERVER_URL, CLIENT_ID, REGIST_NAME, PASSWORD, cerFile);
    }

    //断开行业平台连接
    public static void stop() {
        MqttManager.getInstance(ImiNrApplication.getApplication()).stop();
    }

    public static String getDeviceSN() {

        String serialNumber = android.os.Build.SERIAL;

        Logger.e("getDeviceSN: " + serialNumber);
        return "JGAJ2200105000200002742";
    }


    //上报刷脸记录
    public static void faceRecordUpload(FaceRecordInfo info) {
        try {
            StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext())
                    .faceRecordUpload(info.getFaceCommitId() == -1 ? info.getId() : info.getFaceCommitId(),
                            info.getFaceSetId(),
                            info.getFaceSetVer(),
                            info.getFaceId(),
                            info.getName(),
                            "",
                            info.getFaceImg(),
                            info.getSimilarity(),
                            info.getTimestamp(),
                            info.getIsIdentified(),
                            info.getTem());
//        MqttMessageQuene.getInstance().putQuene(stringMessage);
        } catch (Exception e) {
            Logger.e("上报刷脸异常:" + e.getMessage());
            BusInfo busInfo = new BusInfo();
            busInfo.setKey(AppConstants.UP_FACE_EXCEPTION);
            EventBus.getDefault().post(busInfo);
        }
    }


    public static Integer devConfigUpload(boolean isSyncNow) {
        if (!MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        }
        Map devConfig = new HashMap();
        Map fdfsConfig = new HashMap();
//            Map aiConfig = new HashMap();
        Map faceSetConfig = new HashMap();
        fdfsConfig.put("isBind", !TextUtils.isEmpty(AppConstantInfo.getInstance().getFdfsToken()));
        fdfsConfig.put("key", AppConstantInfo.getInstance().getFdfsToken());
//            aiConfig.put("isBind", !TextUtils.isEmpty(""));
//            aiConfig.put("key", "");
//            aiConfig.put("isValid", false);
        faceSetConfig.put("bindId", AppConstantInfo.getInstance().getFaceSetId());
        faceSetConfig.put("ver", AppConstantInfo.getInstance().getFaceSetVer());
        faceSetConfig.put("isOnline", false);
        faceSetConfig.put("isSyncSuc", System.currentTimeMillis() - AppConstantInfo.getInstance().getFaceSetUpdataTime() < 24 * 60 * 60 * 100);
        faceSetConfig.put("isSyncNow", isSyncNow);

        Map devParaConfig = new HashMap();
        devParaConfig.put("similarityThreshold", AppConstantInfo.getInstance().getCompareThreshold());
        devParaConfig.put("livnessThreshold", AppConstantInfo.getInstance().getLivnessThreshold());
        devParaConfig.put("isOpenLiveness", AppConstantInfo.getInstance().isNeedLiveness());
        devParaConfig.put("isOpenVoice", AppConstantInfo.getInstance().isVoiceOpen());


        devConfig.put("appVersionCur", PackageUtils.getVersionCode(ImiNrApplication.getApplication()));
        devConfig.put("isActive", AppConstantInfo.getInstance().isActive());
        devConfig.put("faceSet", faceSetConfig);
//            devConfig.put("aiSdk", aiConfig);
        devConfig.put("isFdfsTokenGot", fdfsConfig);
        devConfig.put("devPara", devParaConfig);
        MqttRequest request = new MqttRequest(100, devConfig);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, new Gson().toJson(request));
        MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).publish(stringMessage.toBytes(), new Callback() {
            @Override
            public void onSuccess(Object value) {

            }

            @Override
            public void onFailure(Throwable value) {

            }
        });
        Log.d("andshi_mqtt", "设备配置上报\n" + (String) stringMessage.getValue());
        return stringMessage.getMessageId();
    }


    public static Integer upadataAppUpload() {
        if (!MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        }
        Map body = new HashMap();
        body.put("res", 200);
        body.put("reason", "");
        body.put("appVersion", PackageUtils.getVersionCode(ImiNrApplication.getApplication()));
        MqttRequest request = new MqttRequest(107, body);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, new Gson().toJson(request));
        MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).publish(stringMessage.toBytes(), new Callback() {
            @Override
            public void onSuccess(Object value) {

            }

            @Override
            public void onFailure(Throwable value) {

            }
        });
        Log.d("andshi_mqtt", "app升级结果上报\n" + (String) stringMessage.getValue());
        return stringMessage.getMessageId();
    }

    /**
     * 生成特征值后，上报结果
     */
    public static void handleFaceResult(List<UserInfo> errorInfos, FaceUpdataInfo faceUpdataInfo) {
        if (errorInfos == null || errorInfos.isEmpty()) {
            //全部成功
            StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetUpgradeSuccess((int) faceUpdataInfo.getFaceSetId(), (int) faceUpdataInfo.getFaceSetVerStart(), (int) faceUpdataInfo.getFaceSetVerEnd(), faceUpdataInfo.isOnline());
            MqttMessageQuene.getInstance().putQuene(stringMessage);

            SpUtil.putLong(PreferenceConstants.FACE_SET_ID, faceUpdataInfo.getFaceSetId());
            SpUtil.putLong(PreferenceConstants.FACE_SET_VER, faceUpdataInfo.getFaceSetVerEnd());
        } else {
            //有失败的
            ToastUtils.showToast(ImiNrApplication.getApplication().getApplicationContext(), Utils.getStringByValues(R.string.sync_fail));
            List<String> list = new ArrayList<>();
            String s;
            //0：文件无法下载，1：图片无法获取特征值
            for (UserInfo info : errorInfos) {
                s = info.getUserId() + "-" + (info.getDownState() == 2 ? 0 : 1);
                list.add(s);
            }
            StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetUpgradeCsvExecuteError(list);
            MqttMessageQuene.getInstance().putQuene(stringMessage);
            //上报成功后，将失败数据从数据库删除
            for (UserInfo info : errorInfos) {
                UserInfoDBUtil.deleteUserInfo(info.getUserId());
            }

        }

    }

    // 人脸库主动升级请求
    public static void requestFaceDataUpdate() {
        long faceSetId = AppConstantInfo.getInstance().getFaceSetId();
        long faceSetVer = AppConstantInfo.getInstance().getFaceSetVer();
        if (faceSetId != -1) {
            Log.e("mqttutils", "requestFaceDataUpdate: ");
            StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetCheckUpgrades((int) faceSetId, (int) faceSetVer);
            MqttMessageQuene.getInstance().putQuene(stringMessage);
        }
    }

    //人脸库升级结果(Fdfs 秘钥缺失)
    public static void faceSetUpgradeFdfsSecretKeyMiss() {
        StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetUpgradeFdfsSecretKeyMiss();
        MqttMessageQuene.getInstance().putQuene(stringMessage);
    }

    //人脸库升级结果(CSV文件无法下载)
    public static void faceSetUpgradeCsvDownloadError() {
        StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetUpgradeCsvDownloadError();
        MqttMessageQuene.getInstance().putQuene(stringMessage);
    }

    //人脸库版本错误
    public static void faceSetUpgradeVersionError(int version) {
        StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetUpgradeVersionError(version);
        MqttMessageQuene.getInstance().putQuene(stringMessage);
    }

    //CSV文件内容有误
    public static void faceSetUpgradeCsvContentError() {
        StringMessage stringMessage = MqttPublishApi.getInstance(ImiNrApplication.getApplication().getApplicationContext()).faceSetUpgradeCsvContentError();
        MqttMessageQuene.getInstance().putQuene(stringMessage);
    }

}
