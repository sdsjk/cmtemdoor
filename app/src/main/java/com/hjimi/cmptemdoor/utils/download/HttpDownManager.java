package com.hjimi.cmptemdoor.utils.download;

import android.content.Context;
import android.util.Log;

import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.download.DownLoadListener.DownloadInterceptor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * http下载处理类
 */
public class HttpDownManager {


    private HttpDownManager() {
    }

    private static class HttpDownManagerHolder {
        private static final HttpDownManager INSTANCE = new HttpDownManager();
    }

    /**
     * 获取单例
     */
    public static HttpDownManager getInstance() {
        return HttpDownManagerHolder.INSTANCE;
    }

    /**
     * 开始下载
     */
    public void startDown(final UserInfo info, final int index, Subscriber<Integer> subscriber) {
        /*获取service，多次请求公用一个sercie*/
        HttpDownService httpService;

        DownloadInterceptor interceptor = new DownloadInterceptor(null);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //手动创建一个OkHttpClient并设置超时时间
        builder.connectTimeout(15, TimeUnit.SECONDS);
        builder.addInterceptor(interceptor);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request()
                        .newBuilder()
                        .addHeader("Connection", "close").build());
            }
        });
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(builder.build())
                .baseUrl(getBaseUrl(info.getUrl()))
                .build();
        httpService = retrofit.create(HttpDownService.class);

        /*得到rx对象-上一次下載的位置開始下載*/
        httpService.download(getUrl(info.getUrl()))
                /*指定线程*/
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                /*读取下载写入文件*/
                .map(new Func1<ResponseBody, Integer>() {
                    @Override
                    public Integer call(ResponseBody responseBody) {
                        writeCaches(responseBody, info.getLocalName());
                        return index;
                    }
                })
                /*回调线程*/
                .observeOn(AndroidSchedulers.mainThread())
                /*数据回调*/
                .subscribe(subscriber);
    }

    //下载csv文件
    public void startDown(String url, final String localPath, Subscriber<String> subscriber) {

        HttpDownService httpService;

        DownloadInterceptor interceptor = new DownloadInterceptor(null);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //手动创建一个OkHttpClient并设置超时时间
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.addInterceptor(interceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(getBaseUrl(url))
                .build();
        httpService = retrofit.create(HttpDownService.class);


        /*得到rx对象-上一次下載的位置開始下載*/
        httpService.download(getUrl(url))
                /*指定线程*/
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                /*读取下载写入文件*/
                .map(new Func1<ResponseBody, String>() {
                    @Override
                    public String call(ResponseBody responseBody) {
                        writeCaches(responseBody, localPath);
                        return localPath;
                    }
                })
                /*回调线程*/
                .observeOn(AndroidSchedulers.mainThread())
                /*数据回调*/
                .subscribe(subscriber);
    }


    /**
     * 写入文件
     *
     * @param file
     * @throws Exception
     */
    public void writeCaches(ResponseBody responseBody, String file) {
        FileOutputStream outStream = null;
        try {
            outStream = ImiNrApplication.getApplication().openFileOutput(file, Context.MODE_PRIVATE);
            outStream.write(responseBody.bytes());
            outStream.close();
        } catch (Exception e) {
            Log.e("-------------", "-----------exception222222222222222:" + e.getClass() + "-------" + e.fillInStackTrace());
        } finally {
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getBaseUrl(String path) {
        int index = path.lastIndexOf("/");
        String baseUrl = path;
        if (index != -1 && index < path.length()) {
            baseUrl = path.substring(0, index + 1);
        }
        return baseUrl;
    }

    private String getUrl(String path) {
        int index = path.lastIndexOf("/");
        String url = path;
        if (index != -1 && index + 1 < path.length()) {
            url = path.substring(index + 1, path.length());
        }
        return url;
    }
}
