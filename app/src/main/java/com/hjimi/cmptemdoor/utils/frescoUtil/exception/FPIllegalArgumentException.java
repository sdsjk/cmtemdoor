package com.hjimi.cmptemdoor.utils.frescoUtil.exception;

public class FPIllegalArgumentException extends IllegalArgumentException {
    public FPIllegalArgumentException(String detailMessage) {
        super(detailMessage);
    }
}
