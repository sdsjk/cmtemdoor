package com.hjimi.cmptemdoor.utils;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;

import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.interfaces.JudgeNetCallBack;

public class JudgeNetUtils {
    private JudgeNetHandler mHandler = new JudgeNetHandler();
    private static final int JUDGE_NET = 100;
    private static final int HAD_NET = 1;
    private static final int NOT_NET = 2;
    private JudgeNetCallBack judgeNetCallBack;
    private Runnable runnable;

    private JudgeNetUtils() {
    }

    private static class JudgeNetHolder {
        private static final JudgeNetUtils INSTANCE = new JudgeNetUtils();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static JudgeNetUtils getInstance() {
        return JudgeNetHolder.INSTANCE;
    }


    private boolean isNetConnect() {
        return NetUtils.isNetWorkConnected(ImiNrApplication.getApplication().getApplicationContext());
    }


    public void judgeNetConnect(JudgeNetCallBack callBack) {
        this.judgeNetCallBack = callBack;
        runnable = new Runnable() {
            @Override
            public void run() {
                boolean isConnectNet = isNetConnect();
                AppConstantInfo.getInstance().setNetConnect(isConnectNet);
                Message message = mHandler.obtainMessage();
                message.what = JUDGE_NET;
                message.arg1 = isConnectNet ? HAD_NET : NOT_NET;
                if (mHandler.hasMessages(JUDGE_NET)) {
                    mHandler.removeMessages(JUDGE_NET);
                }
                mHandler.sendMessage(message);
            }
        };
        DefaultThreadPool.getMaxPool().execute(runnable);
    }

    private static class JudgeNetHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case JUDGE_NET:
                    if (getInstance().runnable != null) {
                        DefaultThreadPool.getMaxPool().cancel(getInstance().runnable);
                    }
                    boolean isNetConnect = msg.arg1 == HAD_NET;
                    if (getInstance().judgeNetCallBack != null) {
                        getInstance().judgeNetCallBack.judgeNet(isNetConnect);
                    }
                    break;
            }
        }
    }

    public  void toWifiSetting(BaseActivity activity){
        Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
        intent.putExtra("only_access_points", true);

        intent.putExtra("extra_prefs_show_button_bar", true);
        intent.putExtra("extra_prefs_set_back_text", "");
        intent.putExtra("wifi_enable_next_on_connect", true);
        intent.putExtra(":settings:show_fragment_as_subsetting", true);
        intent.putExtra("extra_prefs_set_next_text", "完成");
        activity.startActivityForResult(intent,  AppConstants.WIFI_REQUEST_CODE);
    }
}
