package com.hjimi.cmptemdoor.utils;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditUtils {

    /**
     * 显示键盘
     *
     * @param et 输入焦点
     */
    public static void showInput(Context context, final EditText et) {
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * 隐藏键盘
     */
    public static void hideInput(Context context, EditText et) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != et) {
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        }
    }

    //6-8位数字或字母组合
    public static boolean menuPasswordFilter(String content) {
        String regex = "^[a-z0-9A-Z]{6,8}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(content);
        return match.matches();
    }
}
