package com.hjimi.cmptemdoor.utils;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;

import java.util.Timer;
import java.util.TimerTask;

public class TimerUtils {

    private Timer timer;
    private TimerTask task;
    private TimerHandler mHandler = new TimerHandler();

    private TimerUtils() {
    }

    private static class TimerHolder {
        private static final TimerUtils INSTANCE = new TimerUtils();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static TimerUtils getInstance() {
        return TimerHolder.INSTANCE;
    }

    private static class TimerHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case AppConstants.UP_DEVICE_CONFIG:
                    //更新设备信息
                    Log.e("-----------", "-----------更新设备信息");
                    MqttUtils.devConfigUpload(false);
                    break;
            }
        }
    }


    private void initTimer() {
        task = new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = AppConstants.UP_DEVICE_CONFIG;
                if (mHandler.hasMessages(AppConstants.UP_DEVICE_CONFIG)) {
                    mHandler.removeMessages(AppConstants.UP_DEVICE_CONFIG);
                }
                mHandler.sendMessage(message);
            }
        };
    }

    public void startTimer() {
        cancelTimer();
        if (timer == null) {
            timer = new Timer();
        }
        if (task == null) {
            initTimer();
        }
        //这个方法是说，delay/1000秒后执行task,然后进过period/1000秒再次执行task，这个用于循环任务，执行无数次，当然，你可以用timer.cancel();取消计时器的执行。
        timer.schedule(task, 1000, 10 * 60 * 1000);
    }

    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (task != null) {
            task = null;
        }
    }
}
