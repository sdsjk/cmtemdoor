package com.hjimi.cmptemdoor.utils.keyboard;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.geekmaker.paykeyboard.DefaultKeyboardListener;
import com.geekmaker.paykeyboard.IPayRequest;
import com.hjimi.cmptemdoor.interfaces.OnKeyboardListener;

public class KeyboardUtils {

    private static zhsPayKeyboard keyboard;
    private static OnKeyboardListener mListener;

    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case 100:
                        mListener.onCallback(msg.obj.toString());
                        break;
                }
            } catch (Exception e) {

            }
        }
    };


    private static void openKeyboard(final Context context) {

        if (keyboard == null || keyboard.isReleased()) {
            keyboard = zhsPayKeyboard.get(context);
            if (keyboard != null) {
                keyboard.setLayout("1:5,2:4,3:9,4:8,5:7,6:F3,7:F2,8:F1,9:0,12:REFUND,13:Backspace,14:3,15:2,16:6,19:.,20:CANCEL,31:1,21:SCAN_PAY,23:FACE_PAY,28:LIST,29:OPT,30:CLEAR");
                keyboard.setBaudRate(9600);
                keyboard.setListener(new DefaultKeyboardListener() {
                    @Override
                    public void onRelease() {
                        openKeyboard(context);
                    }

                    @Override
                    public void onAvailable() {
                        openKeyboard(context);
                    }

                    @Override
                    public void onException(Exception e) {
                        Log.e("Thread","---------onException:"+Thread.currentThread().getName());
                        Log.i("KeyboardUI", "usb exception!!!!", e);
                        openKeyboard(context);
                    }

                    @Override
                    public void onPay(final IPayRequest request) {
                    }

                    @Override
                    public void onKeyDown(final int keyCode, final String keyName) {
                        Log.d(keyName, "/////" + keyCode);
                        Message mg = new Message();
                        mg.what = 100;
                        mg.obj = keyName;
                        handler.sendMessage(mg);
                    }

                    @Override
                    public void onKeyUp(int keyCode, String keyName) {
                    }
                });
                keyboard.open();
            }
        } else {
            Log.i("KeyboardUI", "keyboard exists!!!");
        }
    }

    public static void openKeyboard(Context context, OnKeyboardListener listener) {
        mListener = listener;
        openKeyboard(context);
    }

    public static void resetKeyboard() {
        if (keyboard != null) {
            keyboard.reset();
        }
    }
    public static void releaseKeyboard() {
        if (keyboard != null) {
            keyboard.release();
            keyboard = null;
        }
    }
}
