package com.hjimi.cmptemdoor.utils.toast;

import android.content.Context;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.utils.Utils;


public class SmartToast {

    private Toast toast;

    /**
     * 显示Toast
     *
     * @param tvString
     */

    public void show(final Context context, String tvString) {
        if (context != null) {
            final View layout = LayoutInflater.from(context).inflate(R.layout.toast, null);
            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(tvString);
            final Context con = context.getApplicationContext();
            if (Looper.myLooper() == Looper.getMainLooper()) {
                toast = new Toast(con);
                toast.setGravity(Gravity.TOP, 0, Utils.dip2px(75));
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            } else {
                Utils.runInMainThread(new Runnable() {
                    @Override
                    public void run() {
                        toast = new Toast(con);
                        toast.setGravity(Gravity.TOP, 0, Utils.dip2px(75));
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setView(layout);
                        toast.show();
                    }
                });
            }
        }
    }

    public void cancel() {
        if (toast != null) {
            toast.cancel();
        }
    }

}
