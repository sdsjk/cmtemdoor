package com.hjimi.cmptemdoor.utils.mqtt;

import android.util.Log;

import com.cmiot.huadong.andshi.mqtt.MqttManager;
import com.cmiot.huadong.andshi.mqtt.protocol.StringMessage;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.orhanobut.logger.Logger;

import org.fusesource.mqtt.client.Callback;

import java.util.HashMap;
import java.util.Map;

public class MqttMessageQuene {

    private Map<Integer, MqttMessageInfo> queneMap = new HashMap<>();

    private static class MqttQueneHolder {
        private static final MqttMessageQuene INSTANCE = new MqttMessageQuene();
    }

    private MqttMessageQuene() {
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static MqttMessageQuene getInstance() {
        return MqttMessageQuene.MqttQueneHolder.INSTANCE;
    }

    public synchronized void putQuene(StringMessage stringMessage) {
        if (stringMessage != null) {
            Logger.e("存储发布消息:" + stringMessage.getMessageId() + "===发布的消息：" + stringMessage.getValue());
            MqttMessageInfo mqttMessageInfo = new MqttMessageInfo();


            if (AppConstantInfo.getInstance().isNetConnect()) {
                mqttMessageInfo.setFail(false);
            } else {
                mqttMessageInfo.setFail(true);
            }
            mqttMessageInfo.setStringMessage(stringMessage);
            queneMap.put(stringMessage.getMessageId(), mqttMessageInfo);
        }
    }

    public synchronized void updateQueneFail(Integer messageId) {
        MqttMessageInfo mqttMessageInfo = queneMap.get(messageId);
        if (mqttMessageInfo != null) {
            mqttMessageInfo.setFail(true);
            queneMap.put(messageId, mqttMessageInfo);
        }
    }


    public synchronized void removeQuene(Integer messageId) {
        queneMap.remove(messageId);
    }

    private synchronized StringMessage getQueneMessage(Integer messageId) {
        return queneMap.get(messageId).getStringMessage();
    }

    public synchronized void retryPublish(Integer messageId) {
        if (!MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).isConnected()) {
            Logger.e("andshi_mqtt retryPublish:设备不在线");
        } else {
            StringMessage stringMessage = getQueneMessage(messageId);
            if (stringMessage != null) {
                MqttManager.getInstance(ImiNrApplication.getApplication().getApplicationContext()).publish(stringMessage.toBytes(), new Callback() {
                    @Override
                    public void onSuccess(Object value) {

                    }

                    @Override
                    public void onFailure(Throwable value) {

                    }
                });
                Log.d("andshi_mqtt", "重新发布内容\n" + (String) stringMessage.getValue());
            }
        }
    }

    //重连后，重新发布所有的
    public void retryAll() {
        for (Map.Entry<Integer, MqttMessageInfo> entry : queneMap.entrySet()) {
            if (entry.getValue().isFail()) {
                retryPublish(entry.getKey());
            }
        }
    }
}
