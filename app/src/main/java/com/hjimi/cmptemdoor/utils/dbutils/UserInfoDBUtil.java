package com.hjimi.cmptemdoor.utils.dbutils;

import com.hjimi.cmptemdoor.databean.DaoMaster;
import com.hjimi.cmptemdoor.databean.DaoSession;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.databean.UserInfoDao;

import java.util.List;

public class UserInfoDBUtil {

    public static void insertUserInfo(UserInfo userInfo) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        dao.insert(userInfo);

    }

    public static void insertUserInfos(List<UserInfo> userInfos) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        for (UserInfo userInfo : userInfos) {
            UserInfo info = queryUserInfoByUserId(userInfo.getUserId());
            if (info != null) {
                dao.delete(info);
            }
            dao.insert(userInfo);
        }
    }

    /**
     * @param id
     * @param userInfo
     */
    public static void updateUserInfo(long id, UserInfo userInfo) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();

        if (queryUserInfoById(id) != null) {
            dao.update(userInfo);
        }
    }

    public static UserInfo queryUserInfoById(long id) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder()
                .where(UserInfoDao.Properties.Id.eq(id)).unique();

    }

    public static UserInfo queryUserInfoByPath(String path) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder()
                .where(UserInfoDao.Properties.LocalPath.eq(path)).unique();

    }

    public static List<UserInfo> queryAllUserInfo() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder().list();
    }

    public static List<UserInfo> queryAllUserInfoBySearch(String search) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder().whereOr(UserInfoDao.Properties.UserName.like("%" + search + "%"),
                UserInfoDao.Properties.Branch.like("%" + search + "%"),
                UserInfoDao.Properties.WorkerId.like("%" + search + "%")).list();
    }

    public static void deleteUserInfo(String userId) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        dao.queryBuilder().where(UserInfoDao.Properties.UserId.eq(userId)).buildDelete().executeDeleteWithoutDetachingEntities();

    }

    public static UserInfo queryUserInfoByUserId(String id) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder()
                .where(UserInfoDao.Properties.UserId.eq(id)).unique();

    }


    public static List<UserInfo> queryAllUserInfoByPage(int page) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder().orderDesc(UserInfoDao.Properties.Id).offset(page * 10).limit(10).list();
    }

    public static List<UserInfo> queryUserInfoToMakeFeature() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder()
                .where(UserInfoDao.Properties.DownState.eq(1), UserInfoDao.Properties.GenerateState.eq(-1)).list();

    }

    public static List<UserInfo> queryUserInfoNeedUp() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        return dao.queryBuilder().whereOr(UserInfoDao.Properties.DownState.eq(2),
                UserInfoDao.Properties.GenerateState.eq(-1)).list();
    }

    public static void deleteAllUserInfos() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        UserInfoDao dao = daoSession.getUserInfoDao();
        dao.deleteAll();

    }
}
