package com.hjimi.cmptemdoor.utils.mqtt;

import com.cmiot.huadong.andshi.mqtt.protocol.StringMessage;

public class MqttMessageInfo {
    private boolean isFail;
    private StringMessage stringMessage;

    public boolean isFail() {
        return isFail;
    }

    public void setFail(boolean fail) {
        isFail = fail;
    }

    public StringMessage getStringMessage() {
        return stringMessage;
    }

    public void setStringMessage(StringMessage stringMessage) {
        this.stringMessage = stringMessage;
    }
}
