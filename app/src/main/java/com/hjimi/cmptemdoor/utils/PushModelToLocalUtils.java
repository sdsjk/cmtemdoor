package com.hjimi.cmptemdoor.utils;

import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PushModelToLocalUtils {

    public static void pushConfig() {
        String[] files = {"det1.bin", "det1.param.bin", "det2.bin", "det2.param.bin", "det3.bin", "det3.param.bin", "imiFaceFeat_V4_1.pkt", "liveness_DCI_D.model"};
        File outFile;
        for (String file : files) {
            try {
                outFile = new File(AppConstants.MODEL_PATH + File.separator + file);
                if (outFile.exists()) {
                    continue;
                }
                InputStream in = ImiNrApplication.getApplication().getApplicationContext().getAssets().open(file);
                OutputStream out = new FileOutputStream(outFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();

            } catch (IOException e) {

            }
        }


    }

    public static void pushLicense() {
        String file = "face-alg-license";
        File outFile;

        try {
            outFile = new File(AppConstants.LICENSE_PATH + file);
            if (outFile.exists()) {
                return;
            }
            InputStream in = ImiNrApplication.getApplication().getApplicationContext().getAssets().open(file);
            OutputStream out = new FileOutputStream(outFile);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (IOException e) {

        }

    }

    public static File copyPemToLocal(String crt) {
        String file = "certificate.pem";

        File outFile = new File(AppConstants.CER_PATH + file);

        if (outFile.exists()) {
            outFile.delete();
        }
        try {
            FileWriter fileWriter = new FileWriter(outFile);
            fileWriter.write(crt);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {

        }

        return outFile;
    }
}
