package com.hjimi.cmptemdoor.utils;

import android.os.Handler;
import android.os.Message;

import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.interfaces.QueryFaceRecordCallBack;
import com.hjimi.cmptemdoor.utils.dbutils.FaceRecordInfoDBUtil;

public class QueryFaceRecordUtils {
    private QueryRecordHandler mHandler = new QueryRecordHandler();
    private static final int QUERY_FACE_RECORD = 111;
    private QueryFaceRecordCallBack queryFaceRecordCallBack;
    private Runnable runnable;

    private QueryFaceRecordUtils() {
    }

    private static class QueryFaceRecordHolder {
        private static final QueryFaceRecordUtils INSTANCE = new QueryFaceRecordUtils();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static QueryFaceRecordUtils getInstance() {
        return QueryFaceRecordHolder.INSTANCE;
    }


    public synchronized void queryFaceRecord(QueryFaceRecordCallBack callBack) {
        this.queryFaceRecordCallBack = callBack;
        runnable = new Runnable() {
            @Override
            public void run() {
                FaceRecordInfo info = FaceRecordInfoDBUtil.queryFirstFaceRecordInfo();

                Message message = mHandler.obtainMessage();
                message.what = QUERY_FACE_RECORD;
                message.obj = info;
                if (mHandler.hasMessages(QUERY_FACE_RECORD)) {
                    mHandler.removeMessages(QUERY_FACE_RECORD);
                }
                mHandler.sendMessage(message);
            }
        };
        DefaultThreadPool.getMaxPool().execute(runnable);
    }

    private static class QueryRecordHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case QUERY_FACE_RECORD:
                    if (getInstance().runnable != null) {
                        DefaultThreadPool.getMaxPool().cancel(getInstance().runnable);
                    }

                    FaceRecordInfo info = (FaceRecordInfo) msg.obj;
                    if (getInstance().queryFaceRecordCallBack != null) {
                        getInstance().queryFaceRecordCallBack.queryFaceRecord(info);
                    }
                    break;
            }
        }
    }
}
