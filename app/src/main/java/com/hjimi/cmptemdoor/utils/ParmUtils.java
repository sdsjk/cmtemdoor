package com.hjimi.cmptemdoor.utils;


import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ParmUtils {

    //返回请求的交易流水号，需要唯一
    public static String getRequestId() {
        char[] chars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        String nums = "";
        for (int i = 0; i < 32; i++) {
            int id = new Random().nextInt(61);
            nums = nums + chars[id];
        }
        return nums;

    }

    //订单号生成规则：
    //终端号16位+年月日时分秒14位+随机4位
    public static String getOrderId() {
        String random = getNum() + getNum() + getNum() + getNum();
        String date = String.valueOf(System.currentTimeMillis());
        return MqttUtils.getDeviceSN() + date + random;
    }

    private static String getNum() {
        Random random = new Random();
        return String.valueOf(random.nextInt(10));
    }

    //订单日期 8位  如：20190723
    public static String getOrderDate() {
        return new SimpleDateFormat("yyyyMMdd").format(new Date());
    }

    //订单时间 6位 如：120101
    public static String getOrderTime() {
        return new SimpleDateFormat("HHmmss").format(new Date());
    }

    public static String getOrderTime2(long time) {
        return new SimpleDateFormat("HHmmss").format(time);
    }

    //签名数据
    public static String getHmac(Map<String, String> mParam, String key) {
        StringBuffer hmac = new StringBuffer();
        List<Map.Entry<String, String>> parms = new ArrayList<>(mParam.entrySet());
        Collections.sort(parms, new Comparator<Map.Entry<String, String>>() {

            public int compare(Map.Entry<String, String> o1,
                               Map.Entry<String, String> o2) {
                // 指定排序器按照降序排列
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        for (int i = 0; i < parms.size(); i++) {
            String parm = parms.get(i).toString();
            if (i == parms.size() - 1) {
                hmac.append(parm);
            } else {
                hmac.append(parm).append("&");
            }
        }
        String sign = null;
        try {
            sign = RSAUtils.sign(hmac.toString(), RSAUtils.getPrivateKey(key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sign;
    }
}
