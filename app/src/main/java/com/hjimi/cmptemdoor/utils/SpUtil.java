package com.hjimi.cmptemdoor.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;


/**
 * Created by myseterycode on 2018/1/30 0030.
 * Description:SharePreferences存储，主要存储手机号码
 * Update:
 */

public class SpUtil {
    public static void putString(String key, String value) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        sp.edit().putString(key, value).apply();
    }

    public static String getString(String key) {
        return getString(key, "");
    }

    public static String getString(String key, String defauleValue) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sp.getString(key, defauleValue);
    }

    public static void putBoolean(String key, boolean flag) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, flag).apply();
    }

    public static Boolean getBoolean(String key) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    public static Boolean getBoolean(String key, boolean defaultValue) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defaultValue);
    }

    public static void putFloat(String key, float value) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        sp.edit().putFloat(key, value).apply();
    }

    public static float getFloat(String key, float defaultValue) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sp.getFloat(key, defaultValue);
    }


    public static void putInt(String key, int value) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).apply();
    }

    public static int getInt(String key, int defaultValue) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sp.getInt(key, defaultValue);
    }


    public static void putLong(String key, long value) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        sp.edit().putLong(key, value).apply();
    }

    public static long getLong(String key, long defaultValue) {
        SharedPreferences sp = ImiNrApplication.getApplication().getSharedPreferences(AppConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
        return sp.getLong(key, defaultValue);
    }
}
