package com.hjimi.cmptemdoor.utils.download;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.entity.BusInfo;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.StringUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.dbutils.UserInfoDBUtil;
import com.hjimi.cmptemdoor.utils.mqtt.FdfsUtils;
import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.orhanobut.logger.Logger;
import com.zed.zedlib.SystemHelper;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class DownLoadUtils {

    private DownLoadUtils() {
    }

    private static class DownLoadHolder {
        private static final DownLoadUtils INSTANCE = new DownLoadUtils();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static DownLoadUtils getInstance() {
        return DownLoadHolder.INSTANCE;
    }


    public void downloadCSV(String csvPath) {
        //TODO:正式版本去掉
//        csvPath = csvPath.replace("172.16.1.40", "112.25.66.178:8011");
        Log.e("downloadCSV: ", csvPath);
        final String csvLocalPath = "faceData.csv";
        HttpDownManager.getInstance().startDown(csvPath, csvLocalPath, new Subscriber<String>() {
            @Override
            public void onCompleted() {
                //下载csv文件完成
                Logger.e("下载csv文件完成");
                parseCSV(csvLocalPath);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("downloadPhoto: ","---"+e.getMessage() );
                Logger.e("下载csv文件失败");
                ToastUtils.showToast(ImiNrApplication.getApplication().getApplicationContext(), Utils.getStringByValues(R.string.sync_fail));
                MqttUtils.faceSetUpgradeCsvDownloadError();

                BusInfo busInfo = new BusInfo();
                busInfo.setKey(AppConstants.DOWNLOAD_CSV_FAIL);
                EventBus.getDefault().post(busInfo);
            }

            @Override
            public void onNext(String s) {

            }
        });
    }

    private void parseCSV(String csvPath) {

        Observable.just(csvPath)
                .map(new Func1<String, UserInfo>() {
                    @Override
                    public UserInfo call(String csvPath) {
                        Log.e("Thread", "-----parseCSV:" + Thread.currentThread().getName());
                        List<UserInfo> list = makeData(csvPath);
                        Log.e("makeData: ", "--" + list.size());
                        if (list == null || list.size() == 0) {
                            BusInfo busInfo = new BusInfo();
                            busInfo.setKey(AppConstants.TO_MAKE_FEATURE);
                            EventBus.getDefault().post(busInfo);
                            return null;
                        }
                        down(list, list.size(), 0);
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UserInfo>() {
                    @Override
                    public void call(UserInfo info) {

                    }
                });


    }

    /**
     * 解析csv文件并修改数据库
     *
     * @param csvPath
     */
    private List<UserInfo> makeData(String csvPath) {
        BufferedReader in = null;
        List<UserInfo> list = new ArrayList<>();
        File file = new File(ImiNrApplication.getApplication().getFilesDir().getAbsolutePath() + File.separator + csvPath);
        if (!file.exists()) {
            return list;
        }
        boolean isCsvFail = false;
        try {
            in = new BufferedReader(new InputStreamReader(ImiNrApplication.getApplication().openFileInput(csvPath)));
            //每一行的数据
            String inputLine;

            //每一行一个循环并存储
            UserInfo userInfo;

            String userId;
            String workerId;
            String phone;
            String orgName;
            JSONObject jsonObject;

            String localName;
            while ((inputLine = in.readLine()) != null) {

                userInfo = new UserInfo();
                String[] values = inputLine.split("\\s+");

                userId = values[0];
                userInfo.setUserId(userId);
                String state = values[1];
                if (AppConstants.STRING_1.equals(state)) {
                    //删除
                    UserInfoDBUtil.deleteUserInfo(userId);
                    continue;
                }

                userInfo.setUserName(values[2]);
                userInfo.setUrl(values[3]);
                if (!StringUtils.isEmpty(values[4])) {
                    jsonObject = new JSONObject(values[4]);
                    workerId = jsonObject.optString(AppConstants.WORKER_ID);
                    phone = jsonObject.optString(AppConstants.PHONE);
                    orgName = jsonObject.optString(AppConstants.ORG_NAME);
                    userInfo.setWorkerId(workerId);
                    userInfo.setPhone(phone);
                    userInfo.setBranch(orgName);
                }
                userInfo.setDownState(-1);
                userInfo.setGenerateState(-1);

                localName = values[3].substring(values[3].lastIndexOf("/") + 1);
                userInfo.setLocalName(localName);
                userInfo.setLocalPath(Environment.getExternalStorageDirectory().getAbsolutePath() + AppConstants.USER_INFO + File.separator + localName);
                list.add(userInfo);
            }
        } catch (Exception e) {
            isCsvFail = true;
            Log.e("makeData:-- ", e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {

                }
            }
        }

        if (!list.isEmpty()) {
            UserInfoDBUtil.insertUserInfos(list);
        } else {
            if (isCsvFail) {
                //csv文件可能下载是错误的
                MqttUtils.faceSetUpgradeCsvContentError();
            }
        }

        return list;
    }

    private Runnable downRunnable;

    private void down(final List<UserInfo> list, final int listCount, final int currentIndex) {
        downRunnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < listCount; i++) {
                    downloadPhoto(list.get(i), i, listCount);
                }

                BusInfo busInfo = new BusInfo();
                busInfo.setKey(AppConstants.TO_MAKE_FEATURE);
                EventBus.getDefault().post(busInfo);
            }
        };
        DefaultThreadPool.getMaxPool().execute(downRunnable);
    }

    private void downloadPhoto(UserInfo userInfo, int i, int listCount) {
        try {
            URL httpUrl = new URL(FdfsUtils.getUrl(userInfo.getUrl(), AppConstantInfo.getInstance().getFdfsToken()));//获取传入进来的url地址  并捕获解析过程产生的异常
            //使用是Http访问  所以用HttpURLConnection  同理如果使用的是https  则用HttpsURLConnection
            HttpURLConnection conn;
            if (("https").equals(httpUrl.getProtocol())) {
                conn = (HttpsURLConnection) httpUrl.openConnection();//通过httpUrl开启一个HttpsURLConnection对象
            } else {
                conn = (HttpURLConnection) httpUrl.openConnection();//通过httpUrl开启一个HttpURLConnection对象
            }
            conn.setReadTimeout(5000);//设置显示超市时间为5秒
            conn.setRequestMethod("GET");//设置访问方式
            conn.setDoInput(true);//设置可以获取输入流
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setChunkedStreamingMode(0);//避免 如果请求超时就会自动再次发送一次请求
            InputStream in = conn.getInputStream();//获取输入流


            ImiNrApplication.getApplication().deleteFile(userInfo.getLocalName());
            FileOutputStream outStream = ImiNrApplication.getApplication().openFileOutput(userInfo.getLocalName(), Context.MODE_APPEND);
            byte[] b = new byte[2 * 1024];
            int len;
            if (outStream != null) {//id卡如果存在  则写入
                while ((len = in.read(b)) != -1) {
                    outStream.write(b, 0, len);
                }
            }
            outStream.close();
            in.close();

            UserInfo info = UserInfoDBUtil.queryUserInfoByUserId(userInfo.getUserId());
            info.setDownFinish(true);
            info.setDownState(1);
            UserInfoDBUtil.updateUserInfo(info.getId(), info);

            BusInfo busInfo = new BusInfo();
            busInfo.setKey(AppConstants.SYNCHRONOUS_PROGRESS);
            busInfo.setMsg(String.valueOf(String.format("%.1f", (float) i / (float) listCount * 10)) + "%");
            EventBus.getDefault().post(busInfo);
            Logger.e("下载成功:" + i + "---" + userInfo.getLocalName());
        } catch (Exception e) {
            e.printStackTrace();
            Logger.e("下载失败:" + userInfo.getLocalName() + userInfo.getUrl());

            UserInfo info1 = UserInfoDBUtil.queryUserInfoByUserId(userInfo.getUserId());
            info1.setDownFinish(false);
            info1.setDownState(2);
            UserInfoDBUtil.updateUserInfo(info1.getId(), info1);
        }

    }


    public void downloadAPK(String apkPath) {
        Log.e("downloadAPK: ", apkPath);
        final String apkLocalPath = "cmtemdoor.apk";
        HttpDownManager.getInstance().startDown(apkPath, apkLocalPath, new Subscriber<String>() {
            @Override
            public void onCompleted() {
                //下载csv文件完成
                Logger.e("下载apk文件完成");
                installApk(apkLocalPath);
            }

            @Override
            public void onError(Throwable e) {
                Logger.e("下载apk文件失败");
                //TODO:是否重新下载
                SpUtil.putInt(PreferenceConstants.APP_VERSION, -1);
            }

            @Override
            public void onNext(String s) {

            }
        });
    }

    private void installApk(String apkPath) {
        //TODO:新设备可能不支持此api
        SystemHelper.ZedSilentInstallApk(ImiNrApplication.getApplication(), ImiNrApplication.getApplication().getFilesDir().getAbsolutePath() + File.separator + apkPath, 1);

    }
}
