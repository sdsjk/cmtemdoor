package com.hjimi.cmptemdoor.utils;


import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;

/**
 * AES 对称算法加密/解密工具类
 *
 * @author xietansheng
 */
public class AESUtils {

    private static final String AES = "AES";

    private static final String DEFAULT_URL_ENCODING = "UTF-8";

    private static final String AES_CBC = "AES/CBC/PKCS5Padding";

    private static final byte[] DEFAULT_KEY = new byte[]{-97, 88, -94, 9, 70, -76, 126, 25, 0, 3, -20, 113, 108, 28, 69, 125};

    private static final byte[] DEFAULT_IV = new byte[]{-93, 84, -94, 9, 71, -44, 126, 25, 0, 3, -20, 113, 108, 28, 69, 125};

    /**
     * 使用AES加密原始字符串.
     *
     * @param input 原始输入字符数组
     */
    public static String aesEncrypt(byte[] input) {
        return Base64.encodeBase64String(aes(input, DEFAULT_KEY, DEFAULT_IV, Cipher.ENCRYPT_MODE));
    }

    /**
     * 使用AES加密或解密无编码的原始字节数组, 返回无编码的字节数组结果.
     *
     * @param input 原始字节数组
     * @param key   符合AES要求的密钥
     * @param iv    初始向量
     * @param mode  Cipher.ENCRYPT_MODE 或 Cipher.DECRYPT_MODE
     */
    private static byte[] aes(byte[] input, byte[] key, byte[] iv, int mode) {
        try {
            SecretKey secretKey = new SecretKeySpec(key, AES);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance(AES_CBC);
            cipher.init(mode, secretKey, ivSpec);
            return cipher.doFinal(input);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 使用AES解密字符串, 返回原始字符串.
     *
     * @param input Hex编码的加密字符串
     */
    public static String aesDecrypt(byte[] input) throws Exception {
        return new String(aes(Base64.decodeBase64(input), DEFAULT_KEY, DEFAULT_IV, Cipher.DECRYPT_MODE), DEFAULT_URL_ENCODING);
    }


    public static void main(String[] args) throws Exception {
        String ff = "308204be020100300d06092a864886f70d0101010500048204a8308204a40201000282010100d79d067b0e158a9a4d751f181cea1df2b394d8dff4b7ef576bcfabb2ed6fbc88a3b8ee3dcb5798d73ea3a6e5b94410fe8cf586932a410d3e91224cfa330a42e667d6fe25cbb40e2153b84ef7c1cc17d6044f31973ecab2e13342c19bf219a44d1acca61cbb0cb49533aec311889492fd638f36b42ad22f5610292cb617fa2d92537ffdf1c863ad959741fbbc2fabb35665824cfb7af094c46f08b530430748d08ca098e046698808045388844d7e6aa3bffd3814f7ce8a990d28d1449807da1dde682709bf5ee9cbb032ea59a413f7cb0596b90250be4d0f3c208db7389ba98a86b884342c0b584d645d37c2ebd3dffc242e98d04ce661bf10315ac31e1d93890203010001028201010098a82bcb7a68540ef5e283d66dba70e3c2b4136d5575d976baf5792aa123a887d1eb807aadfae2531b9d80f04b60deeb6b5ff71d412077c121bef8637939bdae263d390a0794ff2027876a19ced3b2d8ab8a6aa11c2b25c57ea6b82fb294e718546e2afa5291275630167f346dd04bd62de45b25eadf8ff64e50e8c0306fc84cf232b2ca2630e92545bf1622b95d8e67f649136d10767005c92b1f90b2deb5d1d5b5f74879d7c48aef4f2c96a82d13ed50c8557d0d1fba1099721202b32c5ec9467491b3a1f5d1588e7b04eb9216c3a52c4de58c3609d13eb6cd46828975574e06ef0c1549f27e90716bfdf29ccdb257d1bdd12d309981a330dcbe2aaf90054502818100ec3d487933ecff9576c76f26caf2ac1009cdbf98f44c28cbdeec1c3a1f6c166e56e228f579fd4a8334b57aa17a9806cc5e4a9ffe8d15cc7089a52266050540bc2ae6170b02d02ce0fc7477c5a6541246fac7d88ea499529489739fcd0da9ae2da8d4fa1d66fba7e0d58e01b7d04b3d5a9cbf4aacd056cc8216c11d1107d7c93b02818100e9a61119a86f6c3dabc47c1975a5d3174dec5d765dbae3ec4949f5f41ca59a93b64f7ce5832e81d11fe9ba47ce15c101be632125b6654e6b0d8c919324a52ef6bd4a701fd1af87b0e945fd72426cc582e51a8ce93624e211903b6f561f185ad2d0e5458dab4ac0ad870e32b3c3082840b9a3184c0711e565eb711e393f38ea0b0281803748ec31f915e88628e2e93f31d572b0da2a7d412542c1755c78f2ee23365f55791dba2a205f4755c73b0534cf04ceea7ad289f8bf39afbb526b03cece33f97f4fa6c313bf97ca38e8bbb4dfdb59e761170537c7c1fb63990145becaf4fc959777e0479c23e296da060012a4daa910f5a8a317adb39fec82312306535afb83cb02818100e3ba1c88a98690353ba9725b4e33a59edd3037981a9b017357ab62b1df09f9b366c9eaadb84227fd2fe19ad249a7224b14c98191444951393e2777ebb697d9425c";
        String s2 = aesEncrypt(ff.getBytes());
        System.out.println(s2.length());
        System.out.println(aesDecrypt(s2.getBytes()));
    }
}
