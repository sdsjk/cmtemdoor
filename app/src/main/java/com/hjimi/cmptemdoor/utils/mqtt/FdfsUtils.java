package com.hjimi.cmptemdoor.utils.mqtt;

import java.io.UnsupportedEncodingException;

/**
 * 鉴权工具类
 */
public final class FdfsUtils {

    private static String getToken(String remoteFilename, int ts, String secretKey)
            throws UnsupportedEncodingException {
        byte[] bsFilename = remoteFilename.getBytes("UTF-8");
        byte[] bsKey = secretKey.getBytes("UTF-8");
        byte[] bsTimestamp = (new Integer(ts)).toString().getBytes("UTF-8");
        byte[] buff = new byte[bsFilename.length + bsKey.length + bsTimestamp.length];
        System.arraycopy(bsFilename, 0, buff, 0, bsFilename.length);
        System.arraycopy(bsKey, 0, buff, bsFilename.length, bsKey.length);
        System.arraycopy(bsTimestamp, 0, buff, bsFilename.length + bsKey.length, bsTimestamp.length);
        return MD5Utils.getMD5Code(buff);
    }

    public static String getUrl(String url, String secretKey) {
        try {
            String fileId = url.replaceAll("http://[^/]*/", "").replaceAll("https://[^/]*/", "");
            int pos = fileId.indexOf("/");
            int lts = (int) (System.currentTimeMillis() / 1000L);
            String token = getToken(fileId.substring(pos + 1), lts, secretKey);
            return url + "?token=" + token + "&ts=" + lts;
        } catch (Exception e) {
            return null;
        }
    }
}
