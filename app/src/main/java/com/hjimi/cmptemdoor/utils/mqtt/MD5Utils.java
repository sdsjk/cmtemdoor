package com.hjimi.cmptemdoor.utils.mqtt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5工具类
 */
public final class MD5Utils {

    private static final String[] DIGITS = {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"
    };

    /**
     * 返回形式为数字跟字符串
     */
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return DIGITS[iD1] + DIGITS[iD2];
    }

    /**
     * 返回形式只为数字
     */
    private static String byteToNum(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        return String.valueOf(iRet);
    }

    /**
     * 转换字节数组为16进制字串
     */
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    /**
     * 获取MD5编码
     */
    public static String getMD5Code(String source) {
        String resultString = null;
        try {
            resultString = new String(source);
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteToString(md.digest(source.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }

    /**
     * 获取MD5编码
     */
    public static String getMD5Code(byte[] source) {
        String resultString = null;
        try {
            resultString = new String(source);
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteToString(md.digest(source));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }
}
