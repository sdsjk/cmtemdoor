package com.hjimi.cmptemdoor.utils;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.R;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {
    private static long lastClickTime;
    private static int lastClickId;

    /**
     * @param str 如果是中文则等于两个字符
     * @return
     */
    public static int getChineseCount(String str) {
        int count = 0;
        String regEx = "[\\u4e00-\\u9fa5]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        while (m.find()) {
            count++;
        }
        return count;
    }


    public static String getStringByValues(int stringId) {
        return ImiNrApplication.getApplication().getString(stringId);
    }

    public static String[] getStringArrayByValues(int stringId) {
        return ImiNrApplication.getApplication().getResources().getStringArray(stringId);
    }

    public static String generateTime(long time) {
        time = time + 800;
        int totalSeconds = (int) (time / 1000);
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    /**
     * dip转换px
     */
    public static int dip2px(int dip) {
        final float scale = ImiNrApplication.getApplication().getResources().getDisplayMetrics().density;
        return (int) (dip * scale + 0.5f);
    }

    //Uri中包含中文，编码后返回
    public static Uri getUri(String url) {
        int start = 0;
        if (url.startsWith("http:") || url.startsWith("rtsp:")) {
            start = url.indexOf(':') + 3;
        }
        start = url.indexOf('/', start);
        String temp = url.substring(start + 1);
        String[] paths = temp.split("/");
        String prefix = url.substring(0, start + 1);
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        try {
            for (int i = 0; i < paths.length; i++) {
                paths[i] = URLEncoder.encode(paths[i], "utf-8").replaceAll("\\+", "%20");
                paths[i] = paths[i].replaceAll("%3A", ":").replaceAll("%2F", "/");
                sb.append(paths[i]);
                if (i < paths.length - 1) {
                    sb.append('/');
                }
            }
        } catch (UnsupportedEncodingException e) {
        }
        return Uri.parse(sb.toString());
    }


    /**
     * @param url
     * @return Uri中包含中文，编码后返回 过滤缩率图后面的时间戳http://home.wayclouds.com:5080/sata/icon//root/admin/jpg/1.JPG?time=1524640457
     */
    public static Uri getUriImage(String url) {
        int start = 0;
        if (url.startsWith("http:") || url.startsWith("rtsp:")) {
            start = url.indexOf(':') + 3;
        }
        start = url.indexOf('/', start);
        String temp = url.substring(start + 1);
        String[] paths = temp.split("/");
        String prefix = url.substring(0, start + 1);
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        try {
            //缩率图 后面加的时间戳不转义, 最后一个“？time=”区分
            int last = -1;
            String foot = "";
            for (int i = 0; i < paths.length; i++) {
                if (i == paths.length - 1) {
                    String a = paths[i];
                    last = a.lastIndexOf("?time=");
                    if (-1 != last) {
                        foot = a.substring(last);
                        String content = a.substring(0, last);
                        paths[i] = content;
                    }
                }
                paths[i] = URLEncoder.encode(paths[i], "utf-8").replaceAll("\\+", "%20");
                paths[i] = paths[i].replaceAll("%3A", ":").replaceAll("%2F", "/");
                sb.append(paths[i]);
                if (i < paths.length - 1) {
                    sb.append('/');
                }
                if (i == paths.length - 1 && -1 != last) {
                    sb.append(foot);
                }
            }
        } catch (UnsupportedEncodingException e) {
        }
        return Uri.parse(sb.toString());
    }

    /**
     * 判断是不是连续点击
     *
     * @return
     */
    public static boolean isFastDoubleClick(int id) {
        long time = System.currentTimeMillis();
        if (id != lastClickId) {
            lastClickTime = time;
            lastClickId = id;
            return false;
        } else {
            if (time - lastClickTime < 500) {
                return true;
            }
            lastClickId = id;
            lastClickTime = time;
            return false;
        }
    }

    public static boolean isRunInMainThread() {
        return android.os.Process.myTid() == getMainThreadId();
    }

    public static long getMainThreadId() {
        return ImiNrApplication.getApplication().getMainThreadId();
    }

    public static void runInMainThread(Runnable runnable) {
        if (isRunInMainThread()) {
            runnable.run();
        } else {
            post(runnable);
        }
    }

    /**
     * 在主线程执行runnable
     */
    public static boolean post(Runnable runnable) {
        return getHandler().post(runnable);
    }

    /**
     * 获取主线程的handler
     */
    public static Handler getHandler() {
        Looper mainLooper = ImiNrApplication.getApplication().getMainThreadLooper();
        Handler handler = new Handler(mainLooper);
        return handler;
    }

    /**
     * 得到文件的大小
     *
     * @param context
     * @param size    long
     * @return 1, 023 MB
     */
    public static String getFileSize(Context context, long size) {
        String fileSize = Formatter.formatFileSize(context, size);
        String company;
        String substring = fileSize.substring(fileSize.length() - 2, fileSize.length() - 1);
        String newSize;
        DecimalFormat decimalFormat = new DecimalFormat("#,###.0");
        if ("K".equals(substring) || "M".equals(substring) || "G".equals(substring) || "T".equals(substring)) {
            company = fileSize.substring(fileSize.length() - 2);
            String realSize = fileSize.substring(0, fileSize.length() - company.length()).trim();
            newSize = decimalFormat.format(Float.parseFloat(realSize));

        } else {
            company = "KB";
            newSize = decimalFormat.format(size / 1024F);
        }
        if (newSize.indexOf(".") == 0) {
            if (newSize.endsWith("0")) {
                newSize = "0";
            } else {
                newSize = 0 + newSize;
            }
        }
        return newSize + " " + company;
    }

    /**
     * 格式化音乐显示时间
     *
     * @param time
     * @return
     */
    public static String formatTime(long time) {
        time = time + 500;
        String min = time / (1000 * 60) + "";
        String sec = time % (1000 * 60) + "";
        if (min.length() < 2) {
            min = "0" + time / (1000 * 60) + "";
        } else {
            min = time / (1000 * 60) + "";
        }
        if (sec.length() == 4) {
            sec = "0" + (time % (1000 * 60)) + "";
        } else if (sec.length() == 3) {
            sec = "00" + (time % (1000 * 60)) + "";
        } else if (sec.length() == 2) {
            sec = "000" + (time % (1000 * 60)) + "";
        } else if (sec.length() == 1) {
            sec = "0000" + (time % (1000 * 60)) + "";
        }
        return min + ":" + sec.trim().substring(0, 2);
    }

    //本方法判断自己的某个service是否已经运行
    public static boolean isWorked(Context context, String className) {
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager
                .getRunningServices(Integer.MAX_VALUE);

        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            ActivityManager.RunningServiceInfo serviceInfo = serviceList.get(i);
            ComponentName serviceName = serviceInfo.service;

            if (serviceName.getClassName().equals(className)) {
                return true;
            }
        }
        return false;
    }

//    //Uri中包含中文，编码后返回
//    public static Uri encodedUri(String url) {
//        int start = 0;
//        if (url.startsWith("http:") || url.startsWith("rtsp:")) {
//            start = url.indexOf(':') + 3;
//        }
//        start = url.indexOf('/', start);
//        String temp = url.substring(start + 1);
//        String[] paths = temp.split("/");
//        String prefix = url.substring(0, start + 1);
//        StringBuilder sb = new StringBuilder();
//        sb.append(prefix);
//        try {
//            for (int i = 0; i < paths.length; i++) {
//                paths[i] = URLEncoder.encode(paths[i], "utf-8").replaceAll("\\+", "%20");
//                paths[i] = paths[i].replaceAll("%3A", ":").replaceAll("%2F", "/").replaceAll(" ", "%20");
//                sb.append(paths[i]);
//                if (i < paths.length - 1) {
//                    sb.append('/');
//                }
//            }
//        } catch (UnsupportedEncodingException e) {
//            //保存异常到日志中
//            FileEditUtils.saveCrashInfo2File(e, FileEditUtils.collectDeviceInfo());
//        }
//        return Uri.parse(sb.toString());
//    }
//
//    /**
//     * 返回音频投屏的参数
//     *
//     * @param path
//     * @param fileListResultBean
//     * @return
//     */
//    public static String getMusicDLNAmeteData(String path, FileListResultBean fileListResultBean) {
//        if (fileListResultBean == null) {
//            return path;
//        }
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("<DIDL-Lite xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\" xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:sec=\"http://www.sec.co.kr/\"><item id=\"0/Audio/");
//        if (!TextUtils.isEmpty(fileListResultBean.getFileName())) {
//            stringBuilder.append(fileListResultBean.getFileName() + "\" ").append("parentID=\"0/Audio\" restricted=\"1\">").append("<dc:title>");
//            if (fileListResultBean.getFileName().lastIndexOf(".") != -1) {
//                stringBuilder.append(fileListResultBean.getFileName().substring(0, fileListResultBean.getFileName().lastIndexOf("."))).append("</dc:title>");
//            }
//        } else {
//            stringBuilder.append("").append("parentID=\"0/Audio\" restricted=\"1\">").append("<dc:title> </dc:title>");
//        }
//        stringBuilder.append("<dc:creator>Unknown</dc:creator><upnp:class>object.item.audioItem.musicTrack</upnp:class><upnp:genre>Unknown</upnp:genre><res protocolInfo=\"http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01500000000000000000000000000000\" size=");
//        if (TextUtils.isEmpty(fileListResultBean.getFileSize())) {
//            stringBuilder.append("\"0\">");
//        } else {
//            stringBuilder.append("\"" + fileListResultBean.getFileSize() + "\"" + ">");
//        }
//        stringBuilder.append(path).append("</res></item></DIDL-Lite>");
//        return stringBuilder.toString();
//    }

    public static boolean isNum(String txt) {
        StringBuilder builder = new StringBuilder(txt);
        if (txt.contains(".")) {
            builder.deleteCharAt(builder.indexOf("."));
        }
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(builder.toString());
        return m.matches();
    }

    public static Bitmap getLocalPhoto(String path, int reqLength) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opt);
        int height = opt.outHeight;
        int width = opt.outWidth;
        int inSampleSize = 1;
        if (height > reqLength || width > reqLength) {
            int heightRatio = Math.round((float) height / (float) reqLength);
            int widthRatio = Math.round((float) width / (float) reqLength);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        opt.inSampleSize = inSampleSize;
        opt.inJustDecodeBounds = false;
        Bitmap returnBitmap = null;
        Bitmap bitmap = BitmapFactory.decodeFile(path, opt);
        if (bitmap != null) {
            int degree = 0;
            try {
                ExifInterface exif = new ExifInterface(path);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
                Matrix matrix = new Matrix();
                matrix.postRotate(degree);
                int edgeLength = bitmap.getWidth() > bitmap.getHeight() ? bitmap.getHeight() : bitmap.getWidth();
                returnBitmap = Bitmap.createBitmap(bitmap, ((bitmap.getWidth() - edgeLength) / 2), ((bitmap.getHeight() - edgeLength) / 2), edgeLength, edgeLength, matrix, true);
            } catch (IOException e) {
                returnBitmap = bitmap;
            }
        }
        return returnBitmap;
    }

    public static String formatTime(Date date) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("%02d", date.getDate()));
        builder.append("/");
        builder.append(String.format("%02d", (date.getMonth() + 1)));
        builder.append("/");
        builder.append(date.getYear() + 1900);
        builder.append(" ");
        builder.append(String.format("%02d", date.getHours()));
        builder.append(":");
        builder.append(String.format("%02d", date.getMinutes()));
        return builder.toString();
    }

    //double 保留一位小数
    public static double changeDouble(Double dou) {
        NumberFormat nf = new DecimalFormat("0.0");
        dou = Double.parseDouble(nf.format(dou));
        return dou;
    }

    public static int getColorByValues(int colorId) {
        return ImiNrApplication.getApplication().getResources().getColor(colorId);
    }

    /**
     * 投屏的是否是音响
     *
     * @param description
     * @return
     */
    public static boolean isAudio(String description) {
        if (TextUtils.isEmpty(description)) {
            return false;
        } else {
            String modelDescription = description.toUpperCase();
            return (modelDescription.contains("AUDIO") || modelDescription.contains("MUSIC") || modelDescription.contains("SOUND"));
        }
    }

//    public static String transAirInfo(String info) {
//        if (null == info) {
//            return "";
//        } else {
//            int length = info.length();
//            if (length > 0) {
//                char last = info.charAt(length - 1);
//                switch (last) {
//                    case AppConstants.AIR_EXCELLENT:
//                        return Utils.getStringByValues(R.string.home_climate_air_excellent);
//                    case AppConstants.AIR_GOOD:
//                        return Utils.getStringByValues(R.string.home_climate_air_good);
//                    case AppConstants.AIR_INFERIOR:
//                        return Utils.getStringByValues(R.string.home_climate_air_inferior);
//                    case AppConstants.AIR_POOR:
//                        return Utils.getStringByValues(R.string.home_climate_air_poor);
//                    default:
//                        return info;
//                }
//            }
//        }
//        return info;
//    }
//
//    public static String transPMInfo(int pm) {
//        String info;
//        if (pm <= 36) {
//            info = Utils.getStringByValues(R.string.home_climate_air_excellent);
//        } else if (36 < pm && pm <= 75) {
//            info = Utils.getStringByValues(R.string.home_climate_air_good);
//        } else if (75 < pm && pm <= 115) {
//            info = Utils.getStringByValues(R.string.Slight_Pollution);
//
//        } else if (115 < pm && pm <= 150) {
//            info = Utils.getStringByValues(R.string.Middle_Pollution);
//        } else if (150 < pm && pm <= 250) {
//            info = Utils.getStringByValues(R.string.Heavy_Pollution);
//        } else {
//            info = Utils.getStringByValues(R.string.Most_Pollution);
//        }
//        return info;
//    }


    /**
     * 双击返回键退出
     */
    private static long mExitTime;//时间戳

    public static boolean isDoubleClick() {
        long current = System.currentTimeMillis();
        if ((current - mExitTime) > 2000) {
            ToastUtils.showToast(ImiNrApplication.getApplication(), Utils.getStringByValues(R.string.ClickAgain_ExitApplication_Tip));
            mExitTime = current;
            return false;
        } else {
            ToastUtils.cancel();
            return true;
        }
    }

    /*
     * 获取设备唯一标识GUID
     */
    @SuppressLint("MissingPermission")
    public static String getDeviceId() {
        final TelephonyManager tm = (TelephonyManager) ImiNrApplication.getApplication().
                getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + Settings.Secure.getString(ImiNrApplication.getApplication().
                getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }

    /**
     * use the binary search method find the closest title position before the current position
     *
     * @param arrayTitlePos      search array
     * @param coordinatePosition coordinate position
     * @return
     */
    public static int findRecentMinValue(ArrayList<Integer> arrayTitlePos, int coordinatePosition) {
        int resultPos = Integer.MIN_VALUE;
        if (arrayTitlePos == null) {
            return resultPos;
        }
        int low = 0, high = arrayTitlePos.size() - 1;
        while (high - low >= 0) {
            int halfPosition = low + ((high - low) >> 1);
            if (arrayTitlePos.get(halfPosition) < coordinatePosition) {
                if ((halfPosition + 1) < arrayTitlePos.size()) {
                    if (arrayTitlePos.get(halfPosition + 1) > coordinatePosition) {
                        resultPos = halfPosition;
                        break;
                    } else {
                        low = halfPosition + 1;
                    }
                } else {
                    resultPos = halfPosition;
                    break;
                }
            } else if (arrayTitlePos.get(halfPosition) > coordinatePosition) {
                if (halfPosition - 1 < 0) {
                    break;
                } else {
                    if (arrayTitlePos.get(halfPosition - 1) < coordinatePosition) {
                        resultPos = halfPosition - 1;
                        break;
                    } else {
                        high = halfPosition - 1;
                    }
                }
            } else {
                resultPos = halfPosition;
                break;
            }
        }
        return resultPos >= 0 && resultPos < arrayTitlePos.size() ? arrayTitlePos.get(resultPos) : resultPos;
    }


    /**
     * use the binary search method finds the closest title position after the current position
     *
     * @param arrayTitlePos      search array
     * @param coordinatePosition coordinate position
     * @return
     */
    public static int findRecentMaxValue(ArrayList<Integer> arrayTitlePos, int coordinatePosition) {
        int resultPos = Integer.MIN_VALUE;
        if (arrayTitlePos == null) {
            return resultPos;
        }
        int low = 0, high = arrayTitlePos.size() - 1;
        while (high - low >= 0) {
            int halfPosition = low + ((high - low) >> 1);
            if (arrayTitlePos.get(halfPosition) < coordinatePosition) {
                low = halfPosition + 1;

                if (low < arrayTitlePos.size() && arrayTitlePos.get(low) > coordinatePosition) {
                    resultPos = low;
                    break;
                }
            } else if (arrayTitlePos.get(halfPosition) > coordinatePosition) {
                if (halfPosition - 1 < 0) {
                    resultPos = halfPosition;
                    break;
                } else {
                    if (arrayTitlePos.get(halfPosition - 1) < coordinatePosition) {
                        resultPos = halfPosition;
                        break;
                    } else {
                        high = halfPosition - 1;
                    }
                }
            } else {
                resultPos = halfPosition;
                break;
            }
        }
        return resultPos >= 0 && resultPos < arrayTitlePos.size() ? arrayTitlePos.get(resultPos) : resultPos;
    }

    /**
     * 判断手机号是否合格
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {
        //"[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        String telRegex = "[1][35678]\\d{9}";
        return !StringUtils.isEmpty(mobiles) && mobiles.matches(telRegex);
    }


    /**
     * 判断密码是否合格 新账号体系 设置新的8-16位密码，需要包含字母和数字：
     *
     * @param password
     * @param min      最小个数
     * @param max      最大个数
     * @return
     */
    public static boolean isNewPasswordCorrect(String password, int min, int max) {
        if (password.length() < min || password.length() > max) {
            return false;
        }
        //w 是正则表达式 表示  匹配任何字类字符，包括下划线。与“[A-Za-z0-9_]”等效
        if (password.matches("\\w+")) {
            Pattern p1 = Pattern.compile("[a-zA-Z]+");
            Pattern p2 = Pattern.compile("[0-9]+");
            Matcher m = p1.matcher(password);
            if (!m.find())
                return false;
            else {
                m.reset().usePattern(p2);
                return m.find();
            }
        } else {
            return false;
        }
    }

    /**
     * 判断admin密码是否合格 新账号体系 设置新的8-16位密码，需要包含字母和数字：
     *
     * @param password
     * @param min      最小个数
     * @param max      最大个数
     * @return
     */
    public static boolean isAdminPasswordCorrect(String password, int min, int max) {
        if (password.length() < min || password.length() > max) {
            return false;
        }
        //w 是正则表达式 表示  匹配任何字类字符，包括下划线。与“[A-Za-z0-9_]”等效
        if (password.matches("\\w+")) {
            return true;
        } else {
            return false;
        }
    }

    /* 获得imei作为手机的唯一标示
    *
    * @param activity
    */
    public static String getImei(Activity activity) {
        if (null == activity) {
            return "";
        }
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission")
        String imei = telephonyManager.getDeviceId();
        return imei;
    }

//    /**
//     * @param param 网络配置模块：接口地址、表单数据
//     * @return
//     */
//    public static Map<String, Object> createWanMondParam(JSONObject param) {
//        Map<String, Object> map = new HashMap<>();
//        try {
//            map.put(AppConstants.TOKEN, AppConstantInfo.localToken);
//
//            JSONObject requestData = new JSONObject();
//            requestData.put(AppConstants.CMD, AppConstants.WAN_MOND);
//            requestData.put(AppConstants.PARAM, param);
//            requestData.put(AppConstants.LANG, LanguageConfig.getInstance().getLangFlg());
//            requestData.put(AppConstants.VERSION, LanguageConfig.getInstance().getVersionMsg(1));
//
//            map.put(AppConstants.REQUESTDATA, requestData);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return map;
//    }
//
//    public static void showLoading(BaseActivity activity, LoadingFrameDialogFragment loading) {
//        if (loading != null) {
//            loading.show(activity.getSupportFragmentManager(), "LoadingFrameDialogFragment");
//        }
//    }
//
//    public static void dismissLoading(BaseActivity activity, LoadingFrameDialogFragment loading) {
//        if (loading != null) {
//            loading.dismiss(activity);
//        }
//    }

//    /**
//     * 把秒转换成  dd天HH小时mm分钟ss秒
//     *
//     * @param s
//     * @return
//     */
//    public static String formatTimet(Long s) {
//        Integer mi = 60;
//        Integer hh = mi * 60;
//        Integer dd = hh * 24;
//
//
//        Long day = s / dd;
//        Long hour = (s - day * dd) / hh;
//        Long minute = (s - day * dd - hour * hh) / mi;
//        Long second = (s - day * dd - hour * hh - minute * mi);
//
//        StringBuilder sb = new StringBuilder();
//
//        if (day > 0) {
//
//            if (LanguageConfig.getInstance().getLanguageValue().equals(LanguageEntity.LANGUAGE_OPTION_ZH)) {
//                sb.append(day);
//                sb.append(Utils.getStringByValues(R.string.Sky));
//            } else {
//                sb.append(day);
//                sb.append(" ");
//                if (day > 1) {
//                    sb.append(Utils.getStringByValues(R.string.Days));
//                } else {
//                    sb.append(Utils.getStringByValues(R.string.Day));
//                }
//                sb.append(" ");
//            }
//
//        }
//        if (hour > 0) {
//            if (LanguageConfig.getInstance().getLanguageValue().equals(LanguageEntity.LANGUAGE_OPTION_ZH)) {
//                sb.append(hour);
//                sb.append(Utils.getStringByValues(R.string.Hour));
//            } else {
//                sb.append(hour);
//                sb.append(" ");
//                sb.append(Utils.getStringByValues(R.string.Hour));
//                sb.append(" ");
//            }
//
//        }
//        if (minute > 0) {
//            if (LanguageConfig.getInstance().getLanguageValue().equals(LanguageEntity.LANGUAGE_OPTION_ZH)) {
//                sb.append(minute);
//                sb.append(Utils.getStringByValues(R.string.Minute));
//            } else {
//                sb.append(minute);
//                sb.append(" ");
//                sb.append(Utils.getStringByValues(R.string.Minute));
//                sb.append(" ");
//            }
//
//        }
//        if (second > 0) {
//            if (LanguageConfig.getInstance().getLanguageValue().equals(LanguageEntity.LANGUAGE_OPTION_ZH)) {
//                sb.append(second);
//                sb.append(Utils.getStringByValues(R.string.Second));
//            } else {
//                sb.append(second);
//                sb.append(" ");
//                sb.append(Utils.getStringByValues(R.string.Second));
//            }
//
//        }
//        return sb.toString();
//    }

    public static String getRandomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 获取字符集字节长度
     *
     * @param strs
     * @return
     */
    public static int getByteCount(String strs) {
        if (strs == null) {
            return -1;
        }
        try {
            return strs.getBytes("UTF-8").length;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * @param oneChar
     * @return
     */
    public static boolean isWifiPwdFormat(String oneChar) {
        // 只允许Ascii
        String regEx = "[\u0000-\u00ff]";
        return Pattern.matches(regEx, oneChar);
    }

    public static boolean isAscii(String password) {
        char s[] = password.toCharArray();
        for (int i = 0; i < s.length; i++) {
            if (!isWifiPwdFormat(Character.toString(s[i]))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 同时包含字符和数字
     * @param password
     * @param min
     * @param max
     * @return
     */
    public static boolean isNumAndLetter(String password, int min, int max) {
        if (password.length() < min || password.length() > max) {
            return false;
        }
        Pattern p1 = Pattern.compile("[a-zA-Z]+");
        Pattern p2 = Pattern.compile("[0-9]+");
        Matcher m = p1.matcher(password);
        if (!m.find())
            return false;
        else {
            m.reset().usePattern(p2);
            return m.find();
        }
    }


    /*
     *替换路径中的特殊字符
     */
    public static String encodeUrl(String urlStr) {
        String url = "";
        url = urlStr.contains("%") ? urlStr.replace("%", "%25") : urlStr;
        url = url.contains("+") ? url.replace("+", "%2B") : url;
        url = url.contains(" ") ? url.replace(" ", "%20") : url;
        return url;
    }


    public static String setSubstitution(int stringId, String character) {
        String sInfoFormat = Utils.getStringByValues(stringId);
        return String.format(sInfoFormat, character);
    }

    public static String formatTime(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty())
            format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds) * 1000));

    }

    public static int getScreenHeightPx(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getRealMetrics(dm);
            return dm.heightPixels;
        }
        return 0;

    }

    public static int getStatusBarHeight(Context context) {
        int statusBarHeight = 0;
        Resources res = context.getResources();
        int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = res.getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }



    public static int getNavigationBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height","dimen", "android");
        int height = resources.getDimensionPixelSize(resourceId);
        return height;
    }

    /**
     * 得到屏幕高度
     */
    public static int getHeight() {
        WindowManager wm = (WindowManager) ImiNrApplication.getApplication()
                .getSystemService(Context.WINDOW_SERVICE);

        return wm.getDefaultDisplay().getHeight();
    }

    public static int getWidth() {
        WindowManager wm = (WindowManager) ImiNrApplication.getApplication()
                .getSystemService(Context.WINDOW_SERVICE);

        return wm.getDefaultDisplay().getWidth();
    }
}
