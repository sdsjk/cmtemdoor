package com.hjimi.cmptemdoor.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

import com.hjimi.cmptemdoor.engine.ImiNrApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class NetUtils {
    /**
     * 判断网络是否连接
     *
     * @return
     */
//    public static boolean isNetWorkConnected(Context context) {
//        if (context != null) {
//            ConnectivityManager connectivityManager = (ConnectivityManager) context.
//                    getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//            if (networkInfo != null) {
//                return networkInfo.isAvailable();
//            }
//        }
//        return false;
//    }
    public static boolean isNetWorkConnected(Context context) {
        Runtime runtime = Runtime.getRuntime();
        int msg = -1;
        try {
            //如果status==0则表示网络可用，其中参数-c 1是指ping的次数为1次，-w是指超时时间单位为s
            Process pingProcess = runtime.exec("/system/bin/ping -c 1 -w 1 www.baidu.com");
            InputStreamReader isr = new InputStreamReader(pingProcess.getInputStream());
            BufferedReader buf = new BufferedReader(isr);
            if (buf.readLine() == null) {
                msg = -1;
            } else {
                msg = 0;
            }
            buf.close();
            isr.close();
        } catch (Exception e) {
            msg = -1;
            e.printStackTrace();
        } finally {
            runtime.gc();
            return msg == 0;
        }
    }

    /**
     * 判断WIFI网络是否连接可用
     *
     * @return
     */
    public static boolean isWifiConnected(Context context) {
        if (context != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo != null) {
                NetworkInfo.State state = networkInfo.getState();
                if (state != null) {
                    if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 判断手机网络是否连接可用
     *
     * @return
     */
    public static boolean isWifi(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkINfo = cm.getActiveNetworkInfo();
        return networkINfo != null && networkINfo.getType() == ConnectivityManager.TYPE_WIFI;
    }


    public static String getWiFiSSID() {
        WifiManager mWifiManager = (WifiManager) ImiNrApplication.getApplication().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
        String CurInfoStr = mWifiInfo.toString();
        String CurSsidStr = mWifiInfo.getSSID();
        if (CurInfoStr.contains(CurSsidStr)) {
            return CurSsidStr;
        } else {
            return CurSsidStr.replaceAll("\"", "");
        }

    }

    public static String getGatewayIp() {
        WifiManager mWifiManager = (WifiManager) ImiNrApplication.getApplication().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        DhcpInfo di = mWifiManager.getDhcpInfo();
        long getewayIpL = di.gateway;
        return String.valueOf((int) (getewayIpL & 0xff)) +
                '.' +
                String.valueOf((int) ((getewayIpL >> 8) & 0xff)) +
                '.' +
                String.valueOf((int) ((getewayIpL >> 16) & 0xff)) +
                '.' +
                String.valueOf((int) ((getewayIpL >> 24) & 0xff));
    }

    public static String getIp() {
        try {

            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {

                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> ipAddr = intf.getInetAddresses(); ipAddr.hasMoreElements(); ) {

                    InetAddress inetAddress = ipAddr.nextElement();
                    // ipv4地址
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {

                        return inetAddress.getHostAddress();

                    }

                }

            }

        } catch (Exception ex) {

        }

        return "";

    }

    /**
     * 有时候我们连接上一个没有外网连接的WiFi或者需要输入账号和密码才能链接外网的网络，就会出现虽然网络可用，但是外网却不可以访问
     *
     * @return
     */
//    public static boolean isConnectionOuterNet() {
//
//        Process p = null;
//        try {
//            p = Runtime.getRuntime().exec("/system/bin/ping -c 1 www.baidu.com");
////获取连接子进程的正常输出的输入流
//            final InputStream is1 = p.getInputStream();
////获取连接子进程的错误输出的输入流
//            final InputStream is2 = p.getErrorStream();
////启动两个线程，一个线程读输入流，另一个读错误流
//            DefaultThreadPool.getMaxPool().execute(new Thread() {
//                public void run() {
//                    BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
//                    try {
//                        while (br1.readLine() != null) {
//                        }
//                    } catch (IOException e) {
//                        Logger.e(e.getMessage());
//                    } finally {
//                        try {
//                            if (is1 != null) {
//                                is1.close();
//                            }
//                        } catch (IOException e) {
//                            Logger.e(e.getMessage());
//                        }
//                    }
//                }
//            });
//            DefaultThreadPool.getMaxPool().execute(new Thread() {
//                public void run() {
//                    BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
//                    try {
//                        while (br2.readLine() != null) {
//                        }
//                    } catch (IOException e) {
//                        Logger.e(e.getMessage());
//                    } finally {
//                        try {
//                            is2.close();
//                        } catch (IOException e) {
//                            Logger.e(e.getMessage());
//                        }
//                    }
//                }
//            });
//            return p.waitFor() == 0;
//        } catch (Exception e) {
//            return false;
//        } finally {
//            try {
//                if (p != null) {
//                    p.getErrorStream().close();
//                    p.getInputStream().close();
//                    p.getOutputStream().close();
//                    p.destroy();
//                }
//            } catch (IOException e) {
//                Logger.e(e.getMessage());
//            }
//        }
//    }

    /**
     * 检测p2p是否通畅
     *
     * @return
     */
    public static boolean isP2pConnection(String host) {
        try {
            //例如ping http://127.0.0.1:10240/的时候ping 127.0.0.1
            Process p = Runtime.getRuntime().exec("/system/bin/ping -c 1 " + host.substring(host.indexOf("/") + 2, host.lastIndexOf(":")));
            return p.waitFor() == 0;
        } catch (IOException | InterruptedException e) {
            return false;
        }
    }

    public static String getMac() {
        StringBuilder macSerial = new StringBuilder();
        try {
            Process pp = Runtime.getRuntime().exec(
                    "cat /sys/class/net/wlan0/address");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            String line;
            while ((line = input.readLine()) != null) {
                macSerial.append(line.trim());
            }
            input.close();
            if (!TextUtils.isEmpty(macSerial.toString())) {
                return macSerial.toString();
            }
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iF = interfaces.nextElement();
                byte[] addr = iF.getHardwareAddress();
                if (addr == null || addr.length == 0) {
                    continue;
                }
                StringBuilder buf = new StringBuilder();
                for (byte b : addr) {
                    buf.append(String.format("%02X:", b));
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                String mac = buf.toString();
                if ("wlan0".equals(iF.getName())) {
                    return mac;
                }
            }
        } catch (IOException e) {

        }

        return macSerial.toString();
    }
}


