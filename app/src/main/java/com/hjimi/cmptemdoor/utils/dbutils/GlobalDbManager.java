package com.hjimi.cmptemdoor.utils.dbutils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.hjimi.cmptemdoor.engine.ImiNrApplication;


public class GlobalDbManager {

    private final static String dbName = "cmtemdoor_global_db";
    private GlobalOpenHelper openHelper;
    private Context context;

    private GlobalDbManager() {
        context = ImiNrApplication.getApplication();
        openHelper = new GlobalOpenHelper(context, dbName);
    }

    private static class DbManagerHolder {
        private static final GlobalDbManager INSTANCE = new GlobalDbManager();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static GlobalDbManager getInstance() {
        return DbManagerHolder.INSTANCE;
    }

    /**
     * 获取可读数据库
     */
    public SQLiteDatabase getReadableDatabase() {
        if (openHelper == null) {
            openHelper = new GlobalOpenHelper(context, dbName);
        }
        SQLiteDatabase db = openHelper.getReadableDatabase();
        return db;
    }

    /**
     * 获取可写数据库
     */
    public SQLiteDatabase getWritableDatabase() {
        if (openHelper == null) {
            openHelper = new GlobalOpenHelper(context, dbName);
        }
        SQLiteDatabase db = openHelper.getWritableDatabase();
        return db;
    }

}
