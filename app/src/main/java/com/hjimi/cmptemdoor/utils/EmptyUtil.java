package com.hjimi.cmptemdoor.utils;


import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by myseterycode on 2018/1/23 0023.
 * Description:
 * Update:
 */

public class EmptyUtil {
    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isString( String str) {
        if (null == str || "".equals(str.trim()) || "null".equals(str)) {
            return true;
        }
        return false;
    }

    /**
     * 日期转化
     *
     * @param str
     * @return >9 返回2010-01-01格式
     * >4 返回2010
     * 返回-
     */
    public static String isData(String str) {
        if (isString(str) || "-".equals(str))
            return "-";
        if (str.length() > 10)
            return str.substring(0, 11);
        if (str.length() > 9)
            return str.substring(0, 10);
        if (str.length() > 7)
            return str.substring(0, 8);
        if (str.length() > 3)
            return str.substring(0, 4);
        return str;
    }

    /**
     * 判断列表是否存在、是否为空
     *
     * @param list
     * @return
     */
    public static boolean isList( List list) {
        if (null == list || 0 == list.size()) {
            return true;
        }
        return false;
    }

    /**
     * 手机号码校验
     *
     * @param mobile
     * @return
     */
    public static boolean isPhone(String mobile) {
        String regex = "(\\+\\d+)?1[1234567890]\\d{9}$";
        return Pattern.matches(regex, mobile);
    }

    /**
     * 是固定电话、移动电话、邮箱
     *
     * @param mobile
     * @return
     */
    public static boolean isContact(String mobile) {

        String TPHONE_REGEX = "^(0[0-9]{2,3}/-)?([2-9][0-9]{6,7})+(/-[0-9]{1,4})?$";
        String MPHONE_REGEX = "(\\+\\d+)?1[1234567890]\\d{9}$";
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if (Pattern.matches(TPHONE_REGEX, mobile)) {
            return true;
        } else if (Pattern.matches(MPHONE_REGEX, mobile)) {
            return true;
        } else if (Pattern.matches(EMAIL_REGEX, mobile)) {
            return true;
        }
        return false;
    }

    /**
     * 密码校验
     *
     * @param code
     * @return
     */
    public static boolean isCode(String code) {
        String regex = "\\w{6,20}";
        return Pattern.matches(regex, code);
    }


    /**
     * 短信验证码校验
     *
     * @param msgCode
     * @return
     */
    public static boolean isMsgCode(String msgCode) {
        String regex = "[0-9]{4}";
        return Pattern.matches(regex, msgCode);
    }

    /**
     * 颜色验证
     *
     * @param color
     * @return
     */
    public static String isColor(String color) {
        if (isString(color) || "-".equals(color)) {
            return "000000";
        }
        return color;
    }
}
