package com.hjimi.cmptemdoor.utils;


import android.content.Context;
import android.telephony.TelephonyManager;

import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;

import java.util.UUID;

public class ImeiUtils {

    private String imei = "";

    private static class ImeiHolder {
        private static final ImeiUtils INSTANCE = new ImeiUtils();
    }

    /**
     * 获取单例
     *
     * @return
     */
    public static ImeiUtils getInstance() {
        return ImeiUtils.ImeiHolder.INSTANCE;
    }

    /**
     * 获得imei作为手机的唯一标示
     *
     * @param
     * @return
     */
    public String getImei() {
        if (StringUtils.isEmpty(imei)) {
            String id = SpUtil.getString(PreferenceConstants.UNIQUE_IDENTIFIER);
            if (StringUtils.isEmpty(id)) {
                TelephonyManager telephonyManager = (TelephonyManager) ImiNrApplication.getApplication().getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                id = telephonyManager.getDeviceId();
                if (StringUtils.isEmpty(id)) {
                    id = telephonyManager.getSimSerialNumber();
                    if (StringUtils.isEmpty(id)) {
                        id = UUID.randomUUID().toString();
                    }
                }
                SpUtil.putString(PreferenceConstants.UNIQUE_IDENTIFIER, id);
            }
            imei = id;
        }
        return imei;
    }
}
