package com.hjimi.cmptemdoor.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.SplashActivity;
import com.hjimi.cmptemdoor.base.BaseDialogFragment;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtils {
    /**
     * @return 用来返回 Head头 信息
     */
    public static String getVersionMsg() {
        String VersionName = PackageUtils.getVersionName(ImiNrApplication.getApplication());
        return "2-01" + "-" + VersionName;
    }

    /**
     * 将byte[]转换成Bitmap
     *
     * @param bytes
     * @param width
     * @param height
     * @return
     */
    public static Bitmap getBitmapFromByte(byte[] bytes, int width, int height) {
        final YuvImage image = new YuvImage(bytes, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream os = new ByteArrayOutputStream(bytes.length);
        if (!image.compressToJpeg(new Rect(0, 0, width, height), 100, os)) {
            return null;
        }
        byte[] tmp = os.toByteArray();
        Bitmap bmp = BitmapFactory.decodeByteArray(tmp, 0, tmp.length);
        bmp = adjustPhotoRotation(bmp, 90);
        return bmp;
    }

    private static Bitmap adjustPhotoRotation(Bitmap bm, final int orientationDegree) {

        Matrix m = new Matrix();
        m.setRotate(orientationDegree, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);

        try {
            Bitmap bm1 = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);

            return bm1;

        } catch (OutOfMemoryError ex) {
        }
        return null;

    }

    public static String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED);//判断sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();//获取跟目录
        }
        return sdDir.toString();
    }

    /**
     * 压缩图片（质量压缩）
     *
     * @param bitmap
     */
    public static File compressImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 500) {  //循环判断如果压缩后图片是否大于500kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            options -= 10;//每次都减少10
            bitmap.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            long length = baos.toByteArray().length;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        String filename = format.format(date);
        File file = new File(Environment.getExternalStorageDirectory(), filename + ".png");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            try {
                fos.write(baos.toByteArray());
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        recycleBitmap(bitmap);
        return file;
    }

    public static void recycleBitmap(Bitmap... bitmaps) {
        if (bitmaps == null) {
            return;
        }
        for (Bitmap bm : bitmaps) {
            if (null != bm && !bm.isRecycled()) {
                bm.recycle();
            }
        }
    }

    //替换变量
    public static String setSubstitution(int stringId, String character) {
        String sInfoFormat = Utils.getStringByValues(stringId);
        return String.format(sInfoFormat, character);
    }


    public static void loadLocalPicture(SimpleDraweeView simpleDraweeView, String path) {
        Uri uri = Uri.fromFile(new File(path));
        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)
                .setOldController(simpleDraweeView.getController()).build();
        simpleDraweeView.setController(draweeController);
    }

    public static void dissmissDialog(BaseDialogFragment fragment) {
        if (fragment != null && fragment.getActivity() != null) {
            FragmentManager fragmentManager = fragment.getActivity().getSupportFragmentManager();
            final FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment != null && ft != null && fragment.getActivity() != null) {
                ft.remove(fragment).commitAllowingStateLoss();

            }
        }
    }

    public static void toLogin() {

        //由于MainActivity会杀掉进程，所以跳转到SplashActivity，否则sp中数据未取出
        Intent intent = new Intent(ImiNrApplication.getApplication(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ImiNrApplication.getApplication().startActivity(intent);
    }


    //获取图片角度
    public static int getBitmapDegree(String path) {
        int degree = 0;
        try {
            // 从指定路径下读取图片，并获取其EXIF信息
            ExifInterface exifInterface = new ExifInterface(path);
            // 获取图片的旋转信息
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (Exception e) {
        } finally {

            return degree;
        }
    }

    //旋转图片
    public static Bitmap rotateBitmapByDegree(Bitmap bm, int degree) {
        Bitmap returnBm = null;
        // 根据旋转角度，生成旋转矩阵
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        try {
            // 将原始图片按照旋转矩阵进行旋转，并得到新的图片
            returnBm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
        } catch (Exception e) {
        } finally {
            if (returnBm == null) {
                returnBm = bm;
            }
            if (bm != returnBm) {
                bm.recycle();
            }
        }
        return returnBm;
    }

    //将图片保存到本地
    public static void saveBitmap(Bitmap bitmap, String localPath) {
        try {
            File file = new File(localPath);
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //将图片保存到data/data/报名/files
    public static void saveBitmapFiles(Bitmap bitmap, String localPath) {
        if (bitmap == null)
            return;
        FileOutputStream outStream = null;
        try {
//            File file = new File(localPath);
//            if (file.exists()) {
//                file.delete();
//            }
            outStream = ImiNrApplication.getApplication().openFileOutput(localPath, Context.MODE_PRIVATE);
            outStream.write(Bitmap2Bytes(bitmap));
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }


    public static String getContent(String oldText, int maxLength) {
        if (!StringUtils.isEmpty(oldText) && oldText.length() > maxLength) {
            return setSubstitution(R.string.maxlength_text, oldText.substring(0, getLength(oldText, maxLength)));
        } else {
            return oldText;
        }
    }

    private static int getLength(String etstring, int maxLen) {
        int varlength = 0;
        char[] ch = etstring.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] >= 0x4e00 && ch[i] <= 0x9fbb) {
                varlength = varlength + 2;
            } else {
                varlength++;
            }
            if (varlength > maxLen) {
                return i;
            }
        }
        return etstring.length();

    }


}
