package com.hjimi.cmptemdoor.utils.frescoUtil.exception;

public class FPNullPointerException extends NullPointerException {

    public FPNullPointerException(String detailMessage) {
        super(detailMessage);
    }

}
