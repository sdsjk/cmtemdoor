package com.hjimi.cmptemdoor.utils.dbutils;

import android.content.Context;


import com.hjimi.cmptemdoor.databean.DaoMaster;
import com.hjimi.cmptemdoor.databean.FaceRecordInfoDao;
import com.hjimi.cmptemdoor.databean.RecordInfoDao;
import com.hjimi.cmptemdoor.databean.UserInfoDao;

import org.greenrobot.greendao.database.Database;

public class GlobalOpenHelper extends DaoMaster.DevOpenHelper {
    public GlobalOpenHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {

        //把需要管理的数据库表DAO作为最后一个参数传入到方法中
        DBMigrationHelper.getInstance().migrate(db, UserInfoDao.class,FaceRecordInfoDao.class,RecordInfoDao.class);
    }

}
