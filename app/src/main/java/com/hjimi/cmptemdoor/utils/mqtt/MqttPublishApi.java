package com.hjimi.cmptemdoor.utils.mqtt;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.cmiot.huadong.andshi.mqtt.MqttManager;
import com.cmiot.huadong.andshi.mqtt.MqttRequest;
import com.cmiot.huadong.andshi.mqtt.protocol.ActionEnum;
import com.cmiot.huadong.andshi.mqtt.protocol.Function;
import com.cmiot.huadong.andshi.mqtt.protocol.StringMessage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.fusesource.mqtt.client.Callback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MqttPublishApi {
    private Context mContext;
    private static MqttPublishApi mInstance;
    private Gson mGson = new Gson();

    private MqttPublishApi(Context context) {
        this.mContext = context.getApplicationContext();
    }

    private Callback callback = new Callback() {
        @Override
        public void onSuccess(Object value) {

        }

        @Override
        public void onFailure(Throwable value) {

        }
    };


    public static MqttPublishApi getInstance(Context context) {
        Class var1 = MqttPublishApi.class;
        synchronized (MqttPublishApi.class) {
            if (null == mInstance) {
                mInstance = new MqttPublishApi(context);
            }
        }

        return mInstance;
    }

    public StringMessage send(int cmd, JsonObject json) {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            MqttRequest request = new MqttRequest(cmd, json);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "自定义消息上报\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage devConfigUpload(String fdfsSecretKey, String aiSecretKey, boolean aiSecretKeyValid, int faceSetId, int faceSetVersion, boolean faceSetOnline, boolean faceSetUpgradeResult, boolean faceSetUpgrading) {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map devConfig = new HashMap();
            Map fdfsConfig = new HashMap();
            Map aiConfig = new HashMap();
            Map faceSetConfig = new HashMap();
            fdfsConfig.put("isBind", !TextUtils.isEmpty(fdfsSecretKey));
            fdfsConfig.put("key", fdfsSecretKey);
            aiConfig.put("isBind", !TextUtils.isEmpty(aiSecretKey));
            aiConfig.put("key", aiSecretKey);
            aiConfig.put("isValid", aiSecretKeyValid);
            faceSetConfig.put("bindId", faceSetId);
            faceSetConfig.put("ver", faceSetVersion);
            faceSetConfig.put("isOnline", faceSetOnline);
            faceSetConfig.put("isSyncSuc", faceSetUpgradeResult);
            faceSetConfig.put("isSyncNow", faceSetUpgrading);
            devConfig.put("faceSet", faceSetConfig);
            devConfig.put("aiSdk", aiConfig);
            devConfig.put("isFdfsTokenGot", fdfsConfig);
            MqttRequest request = new MqttRequest(100, devConfig);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "设备配置上报\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage faceSetUpgradeVersionError(int version) {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map result = new HashMap();
            Map errors = new HashMap();
            Map versionError = new HashMap();
            versionError.put("curVersion", version);
            errors.put("versionFail", versionError);
            result.put("fail", errors);
            result.put("res", 10001);
            result.put("isOnline", false);
            MqttRequest request = new MqttRequest(105, result);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage faceSetUpgradeAiSecretKeyMiss() {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map result = new HashMap();
            result.put("res", 10006);
            result.put("isOnline", false);
            MqttRequest request = new MqttRequest(105, result);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage faceSetUpgradeAiSecretKeyError() {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map result = new HashMap();
            result.put("res", 10008);
            result.put("isOnline", false);
            MqttRequest request = new MqttRequest(105, result);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage faceSetUpgradeFdfsSecretKeyMiss() {
        Map result = new HashMap();
        result.put("res", 10005);
        result.put("isOnline", false);
        MqttRequest request = new MqttRequest(105, result);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));

        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
        } else {
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
        }
        return stringMessage;
    }

    public StringMessage faceSetUpgradeBusy(boolean isOnline) {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map result = new HashMap();
            result.put("res", 10004);
            result.put("isOnline", isOnline);
            MqttRequest request = new MqttRequest(105, result);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage faceSetUpgradeCsvDownloadError() {
        Map result = new HashMap();
        result.put("res", 10002);
        result.put("isOnline", false);
        MqttRequest request = new MqttRequest(105, result);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));

        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
        } else {
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
        }
        return stringMessage;
    }

    public StringMessage faceSetUpgradeCsvContentError() {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map result = new HashMap();
            result.put("res", 10007);
            result.put("isOnline", false);
            MqttRequest request = new MqttRequest(105, result);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }

    public StringMessage faceSetUpgradeCsvExecuteError(List<String> images) {
        Map result = new HashMap();
        Map errors = new HashMap();
        List<Map> imageErrors = new ArrayList();
        Iterator var5 = images.iterator();

        while (var5.hasNext()) {
            String s = (String) var5.next();
            String[] keyValues = s.split("-");
            Map map = new HashMap();
            map.put("faceId", keyValues[0]);
            map.put("type", keyValues[1]);
            imageErrors.add(map);
        }

        errors.put("imgFail", imageErrors);
        result.put("fail", errors);
        result.put("res", 10003);
        result.put("isOnline", false);
        MqttRequest request = new MqttRequest(105, result);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));

        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
        } else {
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
        }
        return stringMessage;
    }

    public StringMessage faceSetUpgradeSuccess(int faceSetId, int fromFaceSetVersion, int toFaceSetVersion, boolean isOnline) {
        Map result = new HashMap();
        Map success = new HashMap();
        success.put("faceSetId", faceSetId);
        success.put("faceStartVer", fromFaceSetVersion);
        success.put("faceCurrentVer", toFaceSetVersion);
        result.put("success", success);
        result.put("res", 200);
        result.put("isOnline", isOnline);
        MqttRequest request = new MqttRequest(105, result);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));

        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
        } else {
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库升级结果\n" + (String) stringMessage.getValue());
        }
        return stringMessage;
    }

    public StringMessage faceSetCheckUpgrades(int faceSetId, int faceSetVersion) {
        Map upgrade = new HashMap();
        Map faceSet = new HashMap();
        faceSet.put("bindId", faceSetId);
        faceSet.put("ver", faceSetVersion);
        upgrade.put("faceSet", faceSet);
        MqttRequest request = new MqttRequest(117, upgrade);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));

        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
        } else {
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "人脸库检查更新\n" + (String) stringMessage.getValue());
        }
        return stringMessage;
    }

    public StringMessage faceRecordUpload(long id, int faceSetId, int faceSetVersion, int faceId, String name, String info, String base64, float score, long timestamp, boolean state, float tem) {
        Map result = new HashMap();
        result.put("faceCommitId", id);
        result.put("faceSetId", faceSetId);
        result.put("faceSetVer", faceSetVersion);
        result.put("faceId", faceId);
        result.put("name", name);
        result.put("info", info);
        result.put("faceImg", base64);
        result.put("similarity", score);
        result.put("timestamp", timestamp);
        result.put("isIdentified", state);
        Map deviceInfo = new HashMap();
        deviceInfo.put("temperature", tem);
        result.put("deviceInfo", deviceInfo);

        MqttRequest request = new MqttRequest(111, result);
        StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));

        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
        } else {
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "上报刷脸记录\n" + (String) stringMessage.getValue());
        }
        return stringMessage;
    }

    public StringMessage faceRecognizeOnline(int id, int faceSetId, long timeStamp, String base64) {
        if (!MqttManager.getInstance(this.mContext).isConnected()) {
            Log.e("andshi_mqtt", "设备不在线");
            return null;
        } else {
            Map map = new HashMap();
            map.put("onlinefaceCommitId", id);
            map.put("faceSetId", faceSetId);
            map.put("timeStamp", timeStamp);
            map.put("faceImgBuff", base64);
            MqttRequest request = new MqttRequest(115, map);
            StringMessage stringMessage = new StringMessage(ActionEnum.NOTIFY, Function.TEST2, this.mGson.toJson(request));
            MqttManager.getInstance(this.mContext).publish(stringMessage.toBytes(), callback);
            Log.d("andshi_mqtt", "在线人脸识别\n" + (String) stringMessage.getValue());
            return stringMessage;
        }
    }
}
