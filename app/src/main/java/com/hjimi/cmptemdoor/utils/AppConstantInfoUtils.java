package com.hjimi.cmptemdoor.utils;

import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;

public class AppConstantInfoUtils {

    public static void setAppConstantInfoParms() {
        DefaultThreadPool.getMaxPool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                AppConstantInfo.getInstance().setFaceSetId(SpUtil.getLong(PreferenceConstants.FACE_SET_ID, -1));
                AppConstantInfo.getInstance().setFaceSetVer(SpUtil.getLong(PreferenceConstants.FACE_SET_VER, 0));
                AppConstantInfo.getInstance().setMenuPwd(SpUtil.getString(PreferenceConstants.MENU_PASSWORD, AppConstants.MENU_PASSWORD));
                AppConstantInfo.getInstance().setCompareThreshold(SpUtil.getFloat(PreferenceConstants.FACE_COMPARE_THRESHOLD, 60));
                AppConstantInfo.getInstance().setLivnessThreshold(SpUtil.getFloat(PreferenceConstants.FACE_LIVENESS_THRESHOLD, 20));
                AppConstantInfo.getInstance().setNeedLiveness(SpUtil.getBoolean(PreferenceConstants.FACE_NEED_LIVENESS, true));
                AppConstantInfo.getInstance().setVoiceOpen(SpUtil.getBoolean(PreferenceConstants.VOICE_OPEN));
                AppConstantInfo.getInstance().setFdfsToken(SpUtil.getString(PreferenceConstants.FDFS_TOKEN));
                AppConstantInfo.getInstance().setActive(SpUtil.getBoolean(PreferenceConstants.IS_ACTIVE, true));
                AppConstantInfo.getInstance().setBind(SpUtil.getBoolean(PreferenceConstants.IS_BIND, true));

                AppConstantInfo.getInstance().setTem(SpUtil.getBoolean(PreferenceConstants.TEM_OPEN, true));
                AppConstantInfo.getInstance().setTemThreshold(SpUtil.getFloat(PreferenceConstants.TEM_THRESHOLD, 37.2f));
            }
        });
    }

    public static void clearAppConstantInfoParms() {
        AppConstantInfo.getInstance().setFaceSetId(-1L);
        SpUtil.putLong(PreferenceConstants.FACE_SET_ID, -1L);

        AppConstantInfo.getInstance().setFaceSetVer(0L);
        SpUtil.putLong(PreferenceConstants.FACE_SET_VER, 0L);

        AppConstantInfo.getInstance().setCompareThreshold(60);
        SpUtil.putFloat(PreferenceConstants.FACE_COMPARE_THRESHOLD, 60);

        AppConstantInfo.getInstance().setLivnessThreshold(20);
        SpUtil.putFloat(PreferenceConstants.FACE_LIVENESS_THRESHOLD, 20);

        AppConstantInfo.getInstance().setTemThreshold(37.2f);
        SpUtil.putFloat(PreferenceConstants.TEM_THRESHOLD, 37.2f);

        AppConstantInfo.getInstance().setFdfsToken("");
        SpUtil.putString(PreferenceConstants.FDFS_TOKEN, "");
    }

    //绑定新的人脸库
    public static void bindNewFaceSet() {
        AppConstantInfo.getInstance().setFaceSetId(-1L);
        SpUtil.putLong(PreferenceConstants.FACE_SET_ID, -1L);

        AppConstantInfo.getInstance().setFaceSetVer(0L);
        SpUtil.putLong(PreferenceConstants.FACE_SET_VER, 0L);
    }
}
