package com.hjimi.cmptemdoor.utils.dbutils;

import com.hjimi.cmptemdoor.databean.DaoMaster;
import com.hjimi.cmptemdoor.databean.DaoSession;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.databean.RecordInfoDao;

import java.util.List;

public class RecordInfoDBUtil {

    public static void insertRecordInfo(RecordInfo recordInfo) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        dao.insert(recordInfo);

    }

    public static void insertRecordInfos(List<RecordInfo> recordInfos) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        dao.insertInTx(recordInfos);

    }

    /**
     * @param id
     * @param recordInfo
     */
    public static void updateRecordInfo(long id, RecordInfo recordInfo) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();

        if (queryRecordInfoById(id) != null) {
            dao.update(recordInfo);
        }
    }

    public static RecordInfo queryRecordInfoById(long id) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        return dao.queryBuilder()
                .where(RecordInfoDao.Properties.Id.eq(id)).unique();

    }

    public static RecordInfo queryRecordInfoByPath(String path) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        return dao.queryBuilder()
                .where(RecordInfoDao.Properties.Photo.eq(path)).unique();

    }

    public static List<RecordInfo> queryAllRecordInfo() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        return dao.queryBuilder().orderDesc(RecordInfoDao.Properties.Id).list();
    }

    public static List<RecordInfo> queryAllRecordInfoBySearch(String search) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        return dao.queryBuilder().whereOr(RecordInfoDao.Properties.Name.like("%" + search + "%"),
                RecordInfoDao.Properties.Branch.like("%" + search + "%"),
                RecordInfoDao.Properties.JobNo.like("%" + search + "%")).list();
    }

    public static List<RecordInfo> queryAllRecordInfoByPage(int page) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getReadableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        return dao.queryBuilder().orderDesc(RecordInfoDao.Properties.Id).offset(page * 10).limit(10).list();
    }

    public static void deleteAllRecordInfo() {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        dao.deleteAll();

    }

    /**
     * 删除超过一周的
     * <p>
     * 1. > : gt
     * 2. < : lt
     * 3. >= : ge
     * 4. <= : le
     *
     * @param currentTime
     */
    public static void deleteFaceRecordInfoOverWeek(long currentTime) {
        DaoMaster daoMaster = new DaoMaster(GlobalDbManager.getInstance().getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        RecordInfoDao dao = daoSession.getRecordInfoDao();
        List<RecordInfo> list = dao.queryBuilder().where(RecordInfoDao.Properties.Time.lt(currentTime - 60 * 60 * 1000 * 24 * 7)).list();
        if (list != null && !list.isEmpty()) {
            dao.deleteInTx(list);
        }

    }
}
