package com.hjimi.cmptemdoor.utils.toast;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.Toast;

import com.hjimi.cmptemdoor.utils.StringUtils;


/**
 * 提供与Toast工具方法
 */
public class ToastUtils {
    private static final long DISTANCE = 2500;
    private static String oldMsg;
    protected static SmartToast toast;
    private static long firstTime, lastTime;

    /**
     * 这个只能在主线程中用啦
     *
     * @param ctx
     * @param text
     */
    public static void showToast(Context ctx, String text) {
        if (ctx == null || StringUtils.isEmpty(text)) {
            return;
        }
        if (toast == null) {
            ctx = ctx.getApplicationContext();
            //toast = Toast.makeText(ctx, text, Toast.LENGTH_SHORT);
            toast = new SmartToast();
            toast.show(ctx, text);
            firstTime = System.currentTimeMillis();
        } else {
            lastTime = System.currentTimeMillis();
            if (text.equals(oldMsg)) {
                //大于10秒则展示
                if (lastTime - firstTime > DISTANCE) {
                    //toast之后更新时间
                    firstTime = lastTime;
                    toast.show(ctx, text);
                }
            } else {
                firstTime = lastTime;
                oldMsg = text;
                toast.show(ctx, text);
            }
        }
    }

    /**
     * 处理连续显示Toast
     *
     * @param ctx
     * @param text
     * @param duration
     */
    private static synchronized void showToast(Context ctx, String text, int duration) {
        if (ctx == null) {
            return;
        }
        if (toast == null) {
            Context con = ctx.getApplicationContext();
            toast = new SmartToast();
            toast.show(ctx, text);
            firstTime = System.currentTimeMillis();
        } else {
            lastTime = System.currentTimeMillis();
            if (text.equals(oldMsg)) {
                //大于10秒则展示
                if (lastTime - firstTime > DISTANCE) {
                    //toast之后更新时间
                    firstTime = lastTime;
                    toast.show(ctx, text);
                }
            } else {
                firstTime = lastTime;
                oldMsg = text;
                toast.show(ctx, text);
            }
        }
    }

    /**
     * 提供Toast工具类，可以在非UI线程中运行；
     *
     * @param activity
     * @param msg
     */

    public static void showToast(final Activity activity, final String msg) {
        if (activity != null && !TextUtils.isEmpty(msg)) {
            final Context con = activity.getApplicationContext();
            if (Looper.myLooper() == Looper.getMainLooper()) {
                showToast(con, msg, Toast.LENGTH_SHORT);
            } else {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(con, msg, Toast.LENGTH_SHORT);
                    }
                });
            }
        }
    }


    public static void showToast(final Activity activity, final int id) {
        if (activity != null) {
            final Context con = activity.getApplicationContext();
            final String msg = activity.getResources().getString(id);
            if (Looper.myLooper() == Looper.getMainLooper()) {
                showToast(con, msg, Toast.LENGTH_SHORT);
            } else {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(con, msg, Toast.LENGTH_SHORT);
                    }
                });
            }
        }
    }

    public static void cancel() {
        if (toast != null) {
            toast.cancel();
        }
    }

}
