package com.hjimi.cmptemdoor.databean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class FaceRecordInfo {

    @Id(autoincrement = true)
    private Long id;
    private String name;
    private String faceImg;         //base64
    private Integer faceSetVer;
    private Integer faceSetId;       //: 18，//人脸库id
    private Integer faceId;           //: 12，//人脸id（对应服务器端的faceId）
    private Long faceCommitId;    //: 2， //刷脸记录id（设备侧sqlite自增长主键）
    private Float similarity;      //: 72， //相似度
    private Boolean isIdentified;       //: true，
    private Long timestamp;       //: 2010-10-31 17:00:00，
    private float tem;       //: 2010-10-31 17:00:00，

    @Generated(hash = 336939453)
    public FaceRecordInfo(Long id, String name, String faceImg, Integer faceSetVer, Integer faceSetId, Integer faceId, Long faceCommitId, Float similarity, Boolean isIdentified, Long timestamp, float tem) {
        this.id = id;
        this.name = name;
        this.faceImg = faceImg;
        this.faceSetVer = faceSetVer;
        this.faceSetId = faceSetId;
        this.faceId = faceId;
        this.faceCommitId = faceCommitId;
        this.similarity = similarity;
        this.isIdentified = isIdentified;
        this.timestamp = timestamp;
        this.tem = tem;
    }

    @Generated(hash = 426916499)
    public FaceRecordInfo() {
    }

    public float getTem() {
        return tem;
    }

    public void setTem(float tem) {
        this.tem = tem;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFaceImg() {
        return this.faceImg;
    }

    public void setFaceImg(String faceImg) {
        this.faceImg = faceImg;
    }

    public Integer getFaceSetVer() {
        return this.faceSetVer;
    }

    public void setFaceSetVer(Integer faceSetVer) {
        this.faceSetVer = faceSetVer;
    }

    public Integer getFaceSetId() {
        return this.faceSetId;
    }

    public void setFaceSetId(Integer faceSetId) {
        this.faceSetId = faceSetId;
    }

    public Integer getFaceId() {
        return this.faceId;
    }

    public void setFaceId(Integer faceId) {
        this.faceId = faceId;
    }

    public Long getFaceCommitId() {
        return this.faceCommitId;
    }

    public void setFaceCommitId(Long faceCommitId) {
        this.faceCommitId = faceCommitId;
    }

    public Float getSimilarity() {
        return this.similarity;
    }

    public void setSimilarity(Float similarity) {
        this.similarity = similarity;
    }

    public Boolean getIsIdentified() {
        return this.isIdentified;
    }

    public void setIsIdentified(Boolean isIdentified) {
        this.isIdentified = isIdentified;
    }

    public Long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }


}
