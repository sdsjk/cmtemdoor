package com.hjimi.cmptemdoor.databean;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.greendao.converter.PropertyConverter;

public class FaceFeature_Converter implements PropertyConverter<float[], String> {
    @Override
    public float[] convertToEntityProperty(String databaseValue) {
        if (databaseValue == null) {
            return null;
        }
        // 先得获得这个，然后再typeToken.getType()，否则会异常
        TypeToken<float[]> typeToken = new TypeToken<float[]>() {
        };
        return new Gson().fromJson(databaseValue, typeToken.getType());
    }

    @Override
    public String convertToDatabaseValue(float[] entityProperty) {
        if (entityProperty == null || entityProperty.length == 0) {
            return null;
        } else {
            String sb = new Gson().toJson(entityProperty);
            return sb;

        }
    }
}
