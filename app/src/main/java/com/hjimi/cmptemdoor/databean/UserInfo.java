package com.hjimi.cmptemdoor.databean;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class UserInfo {

    @Id(autoincrement = true)
    private Long id;
    public String userName;
    public String branch;
    public String localPath;

    private String userId;//用户id
    private String url;//网络地址
    private Boolean downFinish;

    private Integer downState;//-1：未下载   1：下载成功  2：下载失败
    private Integer generateState;//-1：未生成  1：生成成功
    private String workerId;
    private String phone;
    private String localName;

    @Convert(columnType = String.class, converter = FaceFeature_Converter.class)
    private float[] faceFeature = new float[]{0};

    @Generated(hash = 1597655198)
    public UserInfo(Long id, String userName, String branch, String localPath,
                    String userId, String url, Boolean downFinish, Integer downState,
                    Integer generateState, String workerId, String phone, String localName,
                    float[] faceFeature) {
        this.id = id;
        this.userName = userName;
        this.branch = branch;
        this.localPath = localPath;
        this.userId = userId;
        this.url = url;
        this.downFinish = downFinish;
        this.downState = downState;
        this.generateState = generateState;
        this.workerId = workerId;
        this.phone = phone;
        this.localName = localName;
        this.faceFeature = faceFeature;
    }

    @Generated(hash = 1279772520)
    public UserInfo() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getLocalPath() {
        return this.localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getDownFinish() {
        return this.downFinish;
    }

    public void setDownFinish(Boolean downFinish) {
        this.downFinish = downFinish;
    }

    public Integer getDownState() {
        return this.downState;
    }

    public void setDownState(Integer downState) {
        this.downState = downState;
    }

    public Integer getGenerateState() {
        return this.generateState;
    }

    public void setGenerateState(Integer generateState) {
        this.generateState = generateState;
    }

    public String getWorkerId() {
        return this.workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocalName() {
        return this.localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public float[] getFaceFeature() {
        return this.faceFeature;
    }

    public void setFaceFeature(float[] faceFeature) {
        this.faceFeature = faceFeature;
    }


}
