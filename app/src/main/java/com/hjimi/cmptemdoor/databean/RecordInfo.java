package com.hjimi.cmptemdoor.databean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class RecordInfo {

    @Id(autoincrement = true)
    private Long id;
    public String name;
    public String jobNo;
    public String branch;
    public String photo;
    public Long time;
    public String amount;
    public String payMode;
    public String orderId;
    public String orderDate;
    public String orderTime;
    private Boolean isOffline;//是否是离线，离线直接显示消费成功，只先记录好
    private float tem;

    @Generated(hash = 756282322)
    public RecordInfo(Long id, String name, String jobNo, String branch, String photo, Long time, String amount, String payMode, String orderId, String orderDate, String orderTime, Boolean isOffline, float tem) {
        this.id = id;
        this.name = name;
        this.jobNo = jobNo;
        this.branch = branch;
        this.photo = photo;
        this.time = time;
        this.amount = amount;
        this.payMode = payMode;
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.orderTime = orderTime;
        this.isOffline = isOffline;
        this.tem = tem;
    }

    @Generated(hash = 1863816245)
    public RecordInfo() {
    }

    public float getTem() {
        return tem;
    }

    public void setTem(float tem) {
        this.tem = tem;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobNo() {
        return this.jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getTime() {
        return this.time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayMode() {
        return this.payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return this.orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderTime() {
        return this.orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Boolean getIsOffline() {
        return this.isOffline;
    }

    public void setIsOffline(Boolean isOffline) {
        this.isOffline = isOffline;
    }


}
