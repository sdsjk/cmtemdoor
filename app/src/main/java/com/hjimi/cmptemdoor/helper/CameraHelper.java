package com.hjimi.cmptemdoor.helper;

/**
 * 作者:jtl
 * 日期:Created in 2020/2/21 21:38
 * 描述:
 * 更改:
 */

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.util.Size;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * 作者:jtl
 * 日期:Created in 2020/2/11 14:53
 * 描述:
 * 更改:
 */
public class CameraHelper {
    private static final String TAG = CameraHelper.class.getName();
    private Camera mCamera;
    private int mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private SurfaceHolder mSurfaceHolder;

    public void openCamera(int CameraId) throws Exception {
        if (mCamera == null) {
            mCameraId = CameraId;
            mCamera = Camera.open(CameraId);
            Camera.Parameters parameters = mCamera.getParameters();
            List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
            Size preview = null;
            for (Camera.Size size : previewSizes) {
                if (size.width == 640 && size.height == 480) {
                    preview = new Size(640, 480);
                }

                LogHelper.w("相机分辨率：" + size.width + "x" + size.height);
            }

            if (preview == null) {
                throw new Exception("该机型不支持 分辨率640 * 480");
            }
//            if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//                //设置镜像效果，支持的值为flip-mode-values=off,flip-v,flip-h,flip-vh;
//                parameters.set("preview-flip", "flip-h");
//            }

            List<Integer> previewFormat = parameters.getSupportedPreviewFormats();
            HashMap<Integer, String> map = new HashMap<>();
            for (Integer format : previewFormat) {
                if (format == ImageFormat.UNKNOWN) {
                    map.put(format, "ImageFormat.UNKNOWN");
                } else if (format == ImageFormat.NV16) {
                    map.put(format, "ImageFormat.NV16");
                } else if (format == ImageFormat.NV21) {
                    map.put(format, "ImageFormat.NV21");
                } else if (format == ImageFormat.YUY2) {
                    map.put(format, "ImageFormat.YUY2");
                } else if (format == ImageFormat.YV12) {
                    map.put(format, "ImageFormat.YV12");
                } else if (format == ImageFormat.RGB_565) {
                    map.put(format, "ImageFormat.RGB_565");
                } else if (format == ImageFormat.JPEG) {
                    map.put(format, "ImageFormat.JPEG");
                }
            }
            if (map.get(ImageFormat.NV21) == null) {
                throw new Exception("该机型不支持NV21格式");
            }

//            parameters.setFocusMode(FOCUS_MODE_AUTO);
            parameters.setPreviewFormat(ImageFormat.NV21);
            parameters.setPreviewSize(640, 480);
            mCamera.setParameters(parameters);
        }

        LogHelper.w(TAG, "openCamera");
    }

    public void startPreview() {
        if (mCamera != null) {
//            mCamera.setDisplayOrientation(mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT ? 90 : 90);
            mCamera.startPreview();

            LogHelper.w(TAG, "startPreview");
        }
    }

    public void startPreview(SurfaceHolder surfaceHolder) {
        try {
            if (mCamera != null && surfaceHolder != null) {
                mSurfaceHolder = surfaceHolder;
                mCamera.setPreviewDisplay(surfaceHolder);
                mCamera.setDisplayOrientation(mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT ? 90 : 90);
                mCamera.startPreview();

                LogHelper.w(TAG, "startPreview");
            }
        } catch (IOException io) {
            LogHelper.w(TAG, io.getMessage());
        }
    }

    public void stopPreview() {
        if (mCamera != null) {
            mCamera.stopPreview();
        }

        LogHelper.w(TAG, "stopPreview");
    }

    public void closeCamera() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.release();

            mCamera = null;
        }

        LogHelper.w(TAG, "closeCamera");
    }

    public void switchCamera() throws Exception {
        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        } else {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }

        switchCamera(mCameraId);
    }

    public void switchCamera(int CameraId) throws Exception {
        closeCamera();
        openCamera(CameraId);
        startPreview(mSurfaceHolder);

        LogHelper.w(TAG, "switchCamera");
    }

    public void setPreviewCallback(Camera.PreviewCallback previewCallback) {
        if (mCamera != null) {
            mCamera.setPreviewCallback(previewCallback);
        }

        LogHelper.w(TAG, "setPreviewCallback");
    }

    public void removePreviewCallback(Camera.PreviewCallback previewCallback) {
        if (mCamera != null) {
            mCamera.setPreviewCallback(previewCallback);
        }

        LogHelper.w(TAG, "setPreviewCallback");
    }

    public Camera getCamera() {
        return mCamera;
    }

    public int getCameraId() {
        return mCameraId;
    }
}

