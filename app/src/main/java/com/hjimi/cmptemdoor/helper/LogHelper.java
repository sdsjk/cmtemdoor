package com.hjimi.cmptemdoor.helper;

import android.util.Log;

/**
 * 作者:jtl
 * 日期:Created in 2020/2/11 15:11
 * 描述:
 * 更改:
 * @author jtlpc
 */
public class LogHelper {
    private static final String TAG = "IMI_TEST";
    private static boolean isPrintLog =true;

    public void v(String message){
        if (isPrintLog){
            Log.v(TAG,message);
        }
    }

    public static void v(String tag, String message){
        if (isPrintLog){
            Log.v(tag,message);
        }
    }
    public static void i(String message){
        if (isPrintLog){
            Log.i(TAG,message);
        }
    }

    public static void i(String tag, String message){
        if (isPrintLog){
            Log.i(tag,message);
        }
    }

    public static void d(String message){
        if (isPrintLog){
            Log.d(TAG,message);
        }
    }

    public static void d(String tag, String message){
        if (isPrintLog){
            Log.d(tag,message);
        }
    }

    public static void w(String message){
        if (isPrintLog){
            Log.w(TAG,message);
        }
    }

    public static void w(String tag, String message){
        if (isPrintLog){
            Log.w(tag,message);
        }
    }

    public static void e(String message){
        if (isPrintLog){
            Log.e(TAG,message);
        }
    }

    public static void e(String tag, String message){
        if (isPrintLog){
            Log.e(tag,message);
        }
    }


}
