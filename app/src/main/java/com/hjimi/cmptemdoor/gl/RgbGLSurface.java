package com.hjimi.cmptemdoor.gl;

import android.content.Context;
import android.util.AttributeSet;

import com.imi.sdk.utils.Rect;

import java.nio.ByteBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * FaceSurfaceView
 *
 * @author jtl
 * @date 2019/9/26
 */
public class RgbGLSurface extends BaseGLSurfaceView {
    private ByteBuffer rgbImage;
    private Rect faceRect;

    private RgbRender rgbRender;
    private RectRender rectRender;

    public RgbGLSurface(Context context) {
        super(context);
    }

    public RgbGLSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void updateColorImage(ByteBuffer rgbImage, int renderType){
        this.rgbImage = rgbImage;
        setRenderType(renderType);
    }

    public void updateColorImage(ByteBuffer rgbImage){
        this.rgbImage = rgbImage;
    }
    public void updateColorImage(ByteBuffer rgbImage, int width, int height){
        this.rgbImage = rgbImage;
        this.width = width;
        this.height = height;
    }

    public void updateFaceRect(Rect rect){
        this.faceRect = rect;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);

        rgbRender = new RgbRender(getContext());
        rgbRender.createGlThread();

        rectRender = new RectRender(getContext());
        rectRender.createGlThread();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        if (rgbImage == null){
            return;
        }
        rgbRender.draw(rgbImage,width,height);

        if (faceRect != null){
            rectRender.draw(faceRect, width, height);
        }
    }

    public void setRenderType(int mode){
        if (rgbRender!=null){
            rgbRender.renderType(mode);
        }
    }
}
