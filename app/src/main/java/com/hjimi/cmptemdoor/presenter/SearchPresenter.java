package com.hjimi.cmptemdoor.presenter;


import android.os.Handler;
import android.os.Message;

import com.hjimi.cmptemdoor.activity.SearchActivity;
import com.hjimi.cmptemdoor.contract.SearchContract;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.utils.dbutils.RecordInfoDBUtil;
import com.hjimi.cmptemdoor.utils.dbutils.UserInfoDBUtil;

import java.lang.ref.SoftReference;
import java.util.List;


public class SearchPresenter implements SearchContract.Presenter {
    private final SoftReference<SearchActivity> mActivity;
    private SearchContract.View mView;
    private List<RecordInfo> recordInfos;
    private List<UserInfo> userInfos;

    public SearchPresenter(SearchActivity activity, SearchContract.View tasksView) {
        mActivity = new SoftReference<>(activity);
        mView = tasksView;
        tasksView.setPresenter(this);
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    mView.setRecordData(recordInfos);
                    break;
                case 2:
                    mView.setUserData(userInfos);
                    break;
            }
        }
    };

    @Override
    public void getUsers(String search) {
        Runnable downRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                userInfos = UserInfoDBUtil.queryAllUserInfoBySearch(search);
                Message msg = Message.obtain();
                msg.what = 2;
                handler.sendMessage(msg);
            }
        };
        DefaultThreadPool.getMaxPool().execute(downRunnable);
    }

    @Override
    public void getRecords(String search) {
        Runnable downRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                recordInfos = RecordInfoDBUtil.queryAllRecordInfoBySearch(search);
                Message msg = Message.obtain();
                msg.what = 1;
                handler.sendMessage(msg);
            }
        };
        DefaultThreadPool.getMaxPool().execute(downRunnable);
    }
}
