package com.hjimi.cmptemdoor.presenter;


import android.os.Handler;
import android.os.Message;

import com.hjimi.cmptemdoor.activity.UserListActivity;
import com.hjimi.cmptemdoor.contract.UserListContract;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.utils.dbutils.UserInfoDBUtil;

import java.lang.ref.SoftReference;
import java.util.List;


public class UserListPresenter implements UserListContract.Presenter {
    private final SoftReference<UserListActivity> mActivity;
    private UserListContract.View mView;
    private List<UserInfo> userInfos;

    public UserListPresenter(UserListActivity activity, UserListContract.View tasksView) {
        mActivity = new SoftReference<>(activity);
        mView = tasksView;
        tasksView.setPresenter(this);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mView.setData(userInfos, msg.what);
        }
    };


    @Override
    public void getUsers(int page) {
        Runnable downRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                userInfos = UserInfoDBUtil.queryAllUserInfoByPage(page - 1);
                Message msg = Message.obtain();
                msg.what = page;
                handler.sendMessage(msg);
            }
        };
        DefaultThreadPool.getMaxPool().execute(downRunnable);
    }
}
