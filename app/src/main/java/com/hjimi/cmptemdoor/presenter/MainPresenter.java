package com.hjimi.cmptemdoor.presenter;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.bean.Face;
import com.hjimi.cmptemdoor.contract.MainContract;
import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.entity.BusInfo;
import com.hjimi.cmptemdoor.interfaces.QueryFaceRecordCallBack;
import com.hjimi.cmptemdoor.utils.FileUtils;
import com.hjimi.cmptemdoor.utils.NetUtils;
import com.hjimi.cmptemdoor.utils.QueryFaceRecordUtils;
import com.hjimi.cmptemdoor.utils.TimerUtils;
import com.hjimi.cmptemdoor.utils.dbutils.FaceRecordInfoDBUtil;
import com.hjimi.cmptemdoor.utils.dbutils.RecordInfoDBUtil;
import com.hjimi.cmptemdoor.utils.dbutils.UserInfoDBUtil;
import com.hjimi.cmptemdoor.utils.download.DownLoadUtils;
import com.hjimi.cmptemdoor.utils.mqtt.FdfsUtils;
import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;
import com.imi.sdk.face.Frame;
import com.imi.sdk.face.Session;
import com.imi.sdk.utils.Rect;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class MainPresenter implements MainContract.Presenter {
    private final SoftReference<MainActivity> mActivity;
    private final String faceDataPath = Environment.getExternalStorageDirectory().getAbsolutePath() + AppConstants.USER_INFO;
    private MainContract.View mView;
    private final static int DINING_QUERY = 100;
    //开启app后，将已准备好的人脸库先拿出来，因为之后不会改变
    private List<UserInfo> userInfos = new ArrayList<>();
    private float similarity;
    private QueryHandler handler = new QueryHandler(this);
    private RecordInfo recordInfo = new RecordInfo();//记录通行记录
    private int rotateCount = 0;
    private long startQueryStatus = 0L;
    private Face face;
    private Runnable compareRunnable, upFaceRecordRunnable;
    private boolean isUping = false;
    private Runnable deleteOverWeekRecordRunnable;
    private FaceRecordInfo faceRecordInfo;//上报刷脸记录

    public MainPresenter(MainActivity activity, MainContract.View tasksView) {
        mActivity = new SoftReference<>(activity);
        mView = tasksView;
        tasksView.setPresenter(this);
        deleteOverWeek();

        startCyclicRunnable();
    }

    private static class QueryHandler extends Handler {
        private SoftReference<MainPresenter> mainPresenter;

        public QueryHandler(MainPresenter mainPresenter) {
            this.mainPresenter = new SoftReference<>(mainPresenter);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case DINING_QUERY:
                    //查询状态
                    RecordInfo info = (RecordInfo) msg.obj;
                    if (info != null) {
//                        mainPresenter.get().diningQuery(info.getOrderId(), info.getOrderDate());
                    }
                    break;
                case AppConstants.COMPARE_FINISH:
                    //人脸对比完成
//                    if (mainPresenter.get().compareRunnable != null) {
//                        DefaultThreadPool.getMaxPool().cancel(mainPresenter.get().compareRunnable);
//                    }

                    Logger.e("--------------识别结束:" + System.currentTimeMillis() + "----数目:" + mainPresenter.get().userInfos.size());
                    UserInfo userInfo = (UserInfo) msg.obj;
                    if (userInfo != null) {
                        mainPresenter.get().recordInfo.setName(userInfo.getUserName());
                        mainPresenter.get().recordInfo.setJobNo(userInfo.getWorkerId());
                        mainPresenter.get().recordInfo.setBranch(userInfo.getBranch());
                        mainPresenter.get().recordInfo.setPhoto(userInfo.getLocalName());
                    }
                    if (mainPresenter.get().face != null) {
                        mainPresenter.get().mView.setMatchFeaceData(userInfo, mainPresenter.get().face.getShoot());
                        mainPresenter.get().face = null;
                    } else {
                        mainPresenter.get().mView.setMatchFeaceData(userInfo, "");
                        Log.e("-----------", "-----------对比结果为空");
                    }
                    break;
                case AppConstants.UP_FACE_RECORD:
                    //上报刷脸记录
                    if (mainPresenter.get().upFaceRecordRunnable != null) {
                        DefaultThreadPool.getMaxPool().cancel(mainPresenter.get().upFaceRecordRunnable);
                    }

                    if (AppConstantInfo.getInstance().isNetConnect() && !mainPresenter.get().isUping) {
                        mainPresenter.get().isUping = true;
                        MqttUtils.faceRecordUpload(mainPresenter.get().faceRecordInfo);
                        Log.e("faceRecordUpload: ", "正常上报");
                    }

                    mainPresenter.get().faceRecordInfo = null;

                    break;
                case AppConstants.DELETE_OVER_WEEK_RECORD:
                    //删除超过一周的消费记录
                    if (mainPresenter.get().deleteOverWeekRecordRunnable != null) {
                        DefaultThreadPool.getMaxPool().cancel(mainPresenter.get().deleteOverWeekRecordRunnable);
                    }
                    break;

            }
        }

    }

    @Override
    public void getMatchInfo(final Face face) {
        final float[] feaceFeature = face.getFeaceFeature();
        if (feaceFeature == null) {
            mView.setMatchFeaceData(null, face.getShoot());
            return;
        }

        this.face = face;
//        Logger.e("--------------开始识别:" + System.currentTimeMillis());
//        compareRunnable = new Runnable() {
//            @Override
//            public void run() {
//                Log.e("thread", "--------------getMatchInfo:" + Thread.currentThread().getName());
//                UserInfo info = null;
//                if (userInfos != null && userInfos.size() != 0) {
//                    info = getMatchData(userInfos, feaceFeature);
//                }
//
//                Message message = handler.obtainMessage();
//                message.what = AppConstants.COMPARE_FINISH;
//                message.obj = info;
//                handler.sendMessage(message);
//            }
//        };
//        DefaultThreadPool.getMaxPool().execute(compareRunnable);
    }

    @Override
    public float getSimilarity() {
        return similarity;
    }

    //获取人脸库中相似度最高的
    private UserInfo getMatchData(List<UserInfo> list, float[] feaceFeature) {
        float compareFace = 0;
        float currentCompare = 0;
        UserInfo info = null;
        float threshold = AppConstantInfo.getInstance().getCompareThreshold() / 100;
        for (UserInfo dataInfo : list) {
            currentCompare = Frame.compareFace(dataInfo.getFaceFeature(), feaceFeature);
            if (currentCompare > threshold) {
                if (compareFace < currentCompare) {
                    compareFace = currentCompare;
                    info = dataInfo;
                }
            }

        }
        similarity = compareFace;
        return info;

    }

    @Override
    public void makeFeature() {
        Logger.e("----开始生成特征值:" + System.currentTimeMillis());
        List<UserInfo> list = UserInfoDBUtil.queryUserInfoToMakeFeature();
        Logger.e("----List<UserInfo>:" + (list == null ? "0" : list.size()));
        if (list != null && !list.isEmpty()) {
            Frame frame;
            Rect rect;
            float[] faceFeature;
            Session faceSession = mView.getFaceSession();
            int needMakeSize = list.size();
            UserInfo info;
            for (int i = 0; i < needMakeSize; i++) {
                info = list.get(i);
                try {
                    frame = faceSession.update(ImiNrApplication.getApplication().getFilesDir().getAbsolutePath() + File.separator + info.getLocalName());
                    //检测参数提供的彩色图中的人脸并返回人脸在图像中的矩形框
                    rect = frame.detectFace();
                    if (rect != null) {
                        faceFeature = frame.getFaceFeature(rect);
                        if (faceFeature != null) {
                            info.setFaceFeature(faceFeature);
                            info.setGenerateState(1);
                            UserInfoDBUtil.updateUserInfo(info.getId(), info);
                            Logger.e("----成功:" + info.getLocalName());
                        } else {
                            if (judgeRetryMakeFeature(info)) {
                                i--;
                                continue;
                            }
                        }
                    } else {
                        Logger.e("----未检测到人脸:" + info.getLocalName());
                        if (judgeRetryMakeFeature(info)) {
                            i--;
                            continue;
                        }
                    }
                } catch (Exception exception) {
                    Logger.e("----未检测到人脸:" + info.getLocalName());
                    if (judgeRetryMakeFeature(info)) {
                        i--;
                        continue;
                    }
                }
                BusInfo busInfo = new BusInfo();
                busInfo.setKey(AppConstants.SYNCHRONOUS_PROGRESS);
                busInfo.setMsg(String.valueOf(String.format("%.1f", (float) i / (float) needMakeSize * 90 + 10)) + "%");
                EventBus.getDefault().post(busInfo);
            }
        }

        if (AppConstantInfo.getInstance().isBack()) {
            MqttUtils.faceSetUpgradeCsvDownloadError();
        } else {
            //开始上报
            List<UserInfo> errorInfos = UserInfoDBUtil.queryUserInfoNeedUp();
            if (mActivity.get().getFaceUpdataInfo() != null) {
                MqttUtils.handleFaceResult(errorInfos, mActivity.get().getFaceUpdataInfo());

                AppConstantInfo.getInstance().setFaceSetId(mActivity.get().getFaceUpdataInfo().getFaceSetId());
                AppConstantInfo.getInstance().setFaceSetVer(mActivity.get().getFaceUpdataInfo().getFaceSetVerEnd());
                AppConstantInfo.getInstance().setFaceSetUpdataTime(System.currentTimeMillis());
            }
        }

        Log.e("---------- ", "--" + mActivity.get().fileList().length);
        //上报结束
        userInfos.clear();
        userInfos.addAll(UserInfoDBUtil.queryAllUserInfo());

        Logger.e("----生成特征值结束:" + System.currentTimeMillis());
        mView.makeDataFinish();

    }

    //生成特征值失败的，旋转重新生成

    private boolean judgeRetryMakeFeature(UserInfo errorInfo) {
        if (rotateCount >= 3) {
            rotateCount = 0;
            return false;
        }
        Bitmap bitmap = FileUtils.rotateBitmapByDegree(BitmapFactory.decodeFile(ImiNrApplication.getApplication().getFilesDir().getAbsolutePath() + File.separator + errorInfo.getLocalName()), 90);
        FileUtils.saveBitmapFiles(bitmap, errorInfo.getLocalName());
        rotateCount++;
        return true;

    }


    //获取密钥
//    @Override
//    public void diningMerc() {
//        Map<String, String> mParam = new HashMap<>();
//        mParam.put(AppConstants.PARAM_REQUEST_ID, ParmUtils.getRequestId());
//        mParam.put(AppConstants.PARAM_SIGN_TYPE, AppConstants.SIGN_TYPE);
//        mParam.put(AppConstants.PARAM_TYPE, AppConstants.DINING_MERC_KEY);
//        mParam.put(AppConstants.PARAM_VERSION, AppConstants.VERSION);
//        mParam.put(AppConstants.PARAM_TERMINAL_ID, MqttUtils.getDeviceSN());
//        mParam.put(AppConstants.PARAM_HMAC, ParmUtils.getHmac(mParam, AppConstants.LOCAL_PRIVATE_KEY));
//        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(mParam).toString());
//        NetUtil.getInstance().connect(Net.getInstance().getApiService().diningMerc(body),
//                new Obser<MercInfo>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e("onCompleted: ", "onCompleted");
//                    }
//
//                    @Override
//                    public void onSuccess(MercInfo mercInfo) {
//                        Log.e("onSuccess: ", mercInfo == null ? "为null" : mercInfo.getData().getMerchantId());
//
//                        if (mercInfo != null && AppConstants.TRANSACTION_SUCCESS_CODE.equals(mercInfo.getCode())) {
//                            String privateKey = null;
//                            try {
//                                privateKey = AESUtils.aesDecrypt(mercInfo.getData().getPrivateKey().getBytes());
//                                Log.e("onSuccess: ", privateKey);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            AppConstantInfo.getInstance().setPrivateKey(privateKey);
//                            AppConstantInfo.getInstance().setMerchantId(mercInfo.getData().getMerchantId());
//                        } else {
//                            //获取私钥及MerchantId失败
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Throwable t) {
//                        Log.e("onFailure: ", t.getMessage());
//                    }
//                });
//    }

    //支付
//    @Override
//    public void diningPay(String orderAmount, String phone, String payMode) {
//        String orderId = ParmUtils.getOrderId();
//        String orderDate = ParmUtils.getOrderDate();
//        recordInfo.setOrderDate(orderDate);
//        recordInfo.setOrderId(orderId);
//        recordInfo.setAmount(orderAmount);
//        recordInfo.setPayMode(payMode);
////        JudgeNetUtils.getInstance().judgeNetConnect(new JudgeNetCallBack() {
////            @Override
////            public void judgeNet(boolean isNetConnect) {
//        if (!AppConstantInfo.getInstance().isNetConnect()) {
//            //无网直接显示消费成功
////            if (AppConstants.MODE_SET_MEAL.equals(payMode)) {
////                mView.setPayResult(AppConstants.TRANSACTION_SUCCESS_CODE,
////                        Utils.getStringByValues(R.string.amount_2) + orderAmount + Utils.getStringByValues(R.string.unit),
////                        Utils.getStringByValues(R.string.set_meal_success));
////            } else {
////                String msg = Utils.getStringByValues(R.string.amount_1) + orderAmount + Utils.getStringByValues(R.string.unit);
////                mView.setPayResult(AppConstants.TRANSACTION_SUCCESS_CODE, msg, msg);
////            }
//            mView.setPayResult(AppConstants.TRANSACTION_SUCCESS_CODE,
//                    Utils.getStringByValues(R.string.net_error2),
//                    Utils.getStringByValues(R.string.net_error2));
//            long time = System.currentTimeMillis();
//            recordInfo.setTime(time);
//
//            recordInfo.setOrderTime(ParmUtils.getOrderTime2(time));
//            recordInfo.setIsOffline(true);
//            RecordInfoDBUtil.insertRecordInfo(recordInfo);
//            recordInfo = new RecordInfo();
//            return;
//        }
//
//
//        Map<String, String> mParam = new HashMap<>();
//        mParam.put(AppConstants.PARAM_REQUEST_ID, ParmUtils.getRequestId());
//        mParam.put(AppConstants.PARAM_SIGN_TYPE, AppConstants.SIGN_TYPE);
//        mParam.put(AppConstants.PARAM_TYPE, AppConstants.DINING_PAY);
//        mParam.put(AppConstants.PARAM_VERSION, AppConstants.VERSION);
//        mParam.put(AppConstants.PARAM_TERMINAL_ID, MqttUtils.getDeviceSN());
//        mParam.put(AppConstants.PARAM_MERCHANT_ID, AppConstantInfo.getInstance().getMerchantId());
//        mParam.put(AppConstants.PARAM_ORDER_ID, orderId);
//        mParam.put(AppConstants.PARAM_ORDER_DATE, orderDate);
//        mParam.put(AppConstants.PARAM_ORDER_TIME, ParmUtils.getOrderTime());
//        mParam.put(AppConstants.PARAM_ORDER_AMOUNT, String.valueOf(Double.parseDouble(orderAmount) * 100));
//        mParam.put(AppConstants.PARAM_PERIOD, "30");
//        mParam.put(AppConstants.PARAM_PERIOD_UNIT, String.valueOf(TimeUnit.MINUTES));
//        mParam.put(AppConstants.PARAM_MOBILE, phone);
//        mParam.put(AppConstants.PARAM_PRODUCT_NAME, "用餐");
//        mParam.put(AppConstants.PARAM_TERMINAL_WAY, "2");
//        mParam.put(AppConstants.PARAM_AMOUNT_SOURCE, "1");
//        mParam.put(AppConstants.PARAM_PIRCING_MODEL, "1");
//        mParam.put(AppConstants.PARAM_HMAC, ParmUtils.getHmac(mParam, AppConstantInfo.getInstance().getPrivateKey()));
//        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(mParam).toString());
//        NetUtil.getInstance().connect(Net.getInstance().getApiService().diningPay(body),
//                new Obser<PayInfo>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e("onCompleted: ", "onCompleted");
//                    }
//
//                    @Override
//                    public void onSuccess(PayInfo payInfo) {
//                        if (payInfo != null) {
//                            switch (payInfo.getCode()) {
//                                case AppConstants.TRANSACTION_SUCCESS_CODE:
//                                    //交易成功
//                                    if (AppConstants.MODE_SET_MEAL.equals(payMode)) {
//                                        mView.setPayResult(payInfo.getCode(),
//                                                Utils.getStringByValues(R.string.amount_2) + orderAmount + Utils.getStringByValues(R.string.unit),
//                                                Utils.getStringByValues(R.string.set_meal_success));
//                                    } else {
//                                        String msg = Utils.getStringByValues(R.string.amount_1) + orderAmount + Utils.getStringByValues(R.string.unit);
//                                        mView.setPayResult(payInfo.getCode(), msg, msg);
//                                    }
//                                    long time = System.currentTimeMillis();
//                                    recordInfo.setTime(time);
//                                    diningDate(orderId, orderDate, ParmUtils.getOrderTime2(time));
//                                    break;
//                                case AppConstants.INSUFFICIENT_FUNDS_CODE:
//                                    //余额不足
//                                    mView.setPayResult(payInfo.getCode(), Utils.getStringByValues(R.string.insufficient_funds), Utils.getStringByValues(R.string.insufficient_funds));
//                                    break;
//                                case AppConstants.TRANSACTION_PROCESSING_CODE:
//                                    //调用查询接口
//                                    //计时10s 还没写
//                                    startQueryStatus = System.currentTimeMillis();
//                                    query(orderId, orderDate);
//                                    break;
//                                default:
//                                    //各种交易失败
//                                    mView.setPayResult(payInfo.getCode(), payInfo.getMessage(), Utils.getStringByValues(R.string.pay_fail));
//                                    break;
//                            }
//                        } else {
//                            //交易失败
//                            mView.setPayResult("-1", Utils.getStringByValues(R.string.pay_fail), Utils.getStringByValues(R.string.pay_fail));
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Throwable t) {
//                        if (t instanceof SocketTimeoutException) {
//                            mView.setPayResult(AppConstants.TRANSACTION_FAILURE_CODE, Utils.getStringByValues(R.string.system_time_out), Utils.getStringByValues(R.string.system_time_out));
//                        } else {
//                            mView.setPayResult("-1", Utils.getStringByValues(R.string.pay_fail), Utils.getStringByValues(R.string.pay_fail));
//                        }
//                        Log.e("onFailure: ", t.getMessage());
//
//                    }
//                });
////            }
////        });
//    }


    //查询订单支付状态
//    @Override
//    public void diningQuery(String orderId, String orderDate) {
//        Map<String, String> mParam = new HashMap<>();
//        mParam.put(AppConstants.PARAM_REQUEST_ID, ParmUtils.getRequestId());
//        mParam.put(AppConstants.PARAM_SIGN_TYPE, AppConstants.SIGN_TYPE);
//        mParam.put(AppConstants.PARAM_TYPE, AppConstants.DINING_QUERY);
//        mParam.put(AppConstants.PARAM_VERSION, AppConstants.VERSION);
//        mParam.put(AppConstants.PARAM_TERMINAL_ID, MqttUtils.getDeviceSN());
//        mParam.put(AppConstants.PARAM_MERCHANT_ID, AppConstantInfo.getInstance().getMerchantId());
//        mParam.put(AppConstants.PARAM_ORDER_ID, orderId);
//        mParam.put(AppConstants.PARAM_ORDER_DATE, orderDate);
//        mParam.put(AppConstants.PARAM_HMAC, ParmUtils.getHmac(mParam, AppConstantInfo.getInstance().getPrivateKey()));
//        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(mParam).toString());
//        NetUtil.getInstance().connect(Net.getInstance().getApiService().diningQuery(body),
//                new Obser<PayInfo>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e("onCompleted: ", "onCompleted");
//                    }
//
//                    @Override
//                    public void onSuccess(PayInfo payInfo) {
//                        if (payInfo != null) {
//                            switch (payInfo.getCode()) {
//                                case AppConstants.TRANSACTION_SUCCESS_CODE:
//                                    //交易成功
//                                    String msg = Utils.getStringByValues(R.string.amount_1) + payInfo.getData().getAmount() + Utils.getStringByValues(R.string.unit);
//                                    mView.setPayResult(payInfo.getCode(), msg, msg);
//                                    long time = System.currentTimeMillis();
//                                    recordInfo.setTime(time);
//                                    diningDate(orderId, orderDate, ParmUtils.getOrderTime2(time));
//                                    break;
//                                case AppConstants.INSUFFICIENT_FUNDS_CODE:
//                                    //余额不足
//                                    mView.setPayResult(payInfo.getCode(), Utils.getStringByValues(R.string.insufficient_funds), Utils.getStringByValues(R.string.insufficient_funds));
//                                    break;
//                                case AppConstants.TRANSACTION_PROCESSING_CODE:
//                                    ////接着查
//                                    query(orderId, orderDate);
//                                    break;
//                                default:
//                                    //各种交易失败
//                                    mView.setPayResult(payInfo.getCode(), payInfo.getMessage(), Utils.getStringByValues(R.string.pay_fail));
//                                    break;
//                            }
//                        } else {
//                            //接着查
//                            query(orderId, orderDate);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Throwable t) {
//                        if (t instanceof SocketTimeoutException) {
//                            mView.setPayResult(AppConstants.TRANSACTION_FAILURE_CODE, Utils.getStringByValues(R.string.system_time_out), Utils.getStringByValues(R.string.system_time_out));
//                        } else {
//                            mView.setPayResult("-1", Utils.getStringByValues(R.string.pay_fail), Utils.getStringByValues(R.string.pay_fail));
//                        }
//                        Log.e("onFailure: ", t.getMessage());
//                    }
//                });
//    }

    //更新订单时间
//    @Override
//    public void diningDate(String orderId, String sucDate, String sucTime) {
//        Map<String, String> mParam = new HashMap<>();
//        mParam.put(AppConstants.PARAM_REQUEST_ID, ParmUtils.getRequestId());
//        mParam.put(AppConstants.PARAM_SIGN_TYPE, AppConstants.SIGN_TYPE);
//        mParam.put(AppConstants.PARAM_TYPE, AppConstants.DINING_DATE);
//        mParam.put(AppConstants.PARAM_VERSION, AppConstants.VERSION);
//        mParam.put(AppConstants.PARAM_TERMINAL_ID, MqttUtils.getDeviceSN());
//        mParam.put(AppConstants.PARAM_MERCHANT_ID, AppConstantInfo.getInstance().getMerchantId());
//        mParam.put(AppConstants.PARAM_ORDER_ID, orderId);
//        mParam.put(AppConstants.PARAM_SUC_DATE, sucDate);
//        mParam.put(AppConstants.PARAM_SUC_TIME, sucTime);
//        mParam.put(AppConstants.PARAM_HMAC, ParmUtils.getHmac(mParam, AppConstantInfo.getInstance().getPrivateKey()));
//        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(mParam).toString());
//        NetUtil.getInstance().connect(Net.getInstance().getApiService().diningDate(body),
//                new Obser<DateInfo>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e("onCompleted: ", "onCompleted");
//                    }
//
//                    @Override
//                    public void onSuccess(DateInfo dateInfo) {
//                        if (dateInfo != null && AppConstants.TRANSACTION_SUCCESS_CODE.equals(dateInfo.getCode())) {
//                            //保存订单
//                            recordInfo.setOrderTime(sucTime);
//                            RecordInfoDBUtil.insertRecordInfo(recordInfo);
//                            recordInfo = new RecordInfo();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Throwable t) {
//                        Log.e("onFailure: ", t.getMessage());
//                    }
//                });
//    }


    //删除照片
    @Override
    public void deletePic(String url) {
        Observable.just("")
                .map(new Func1<String, Object>() {
                    @Override
                    public Object call(String s) {
                        Log.e("Thread", "-----deletePic:" + Thread.currentThread().getName());

                        ImiNrApplication application = ImiNrApplication.getApplication();
                        String[] strings = application.fileList();
                        for (String string : strings) {
                            Log.e("----------deletePic", string);
                            if (string.endsWith(".png") || string.endsWith(".jpg") || string.endsWith(".PNG") || string.endsWith(".JPG")) {
                                application.deleteFile(string);
                            }
                        }
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object info) {
                        Log.e("----------deletePic", "删除完成");
                        if (!TextUtils.isEmpty(url)) {
                            DownLoadUtils.getInstance().downloadCSV(FdfsUtils.getUrl(url, AppConstantInfo.getInstance().getFdfsToken()));
                        }
                    }
                });
    }

    /**
     * 上报刷脸记录
     *
     * @param info
     */
    @Override
    public void upFaceRecord(FaceRecordInfo info) {
        this.faceRecordInfo = info;
//        upFaceRecordRunnable = new Runnable() {
//            @Override
//            public void run() {
//                //先保存  再上报
//                Log.e("thread", "--------------upFaceRecord:" + Thread.currentThread().getName());
//                long l = FaceRecordInfoDBUtil.insertFaceRecordInfo(info);
//                info.setFaceCommitId(l);
//                if (NetUtils.isNetWorkConnected(mActivity.get()) && !isUping) {
//                    isUping = true;
//                    MqttUtils.faceRecordUpload(info);
//                    Log.e("faceRecordUpload: ", "正常上报" + l);
//                }
//                Message message = handler.obtainMessage();
//                message.what = AppConstants.UP_FACE_RECORD;
//                handler.sendMessage(message);
//            }
//        };
//        DefaultThreadPool.getMaxPool().execute(upFaceRecordRunnable);
    }

    @Override
    public void openDoor(float tem) {
        //todo：测温  设置温度
        recordInfo.setTem(tem);
        //开门保存通行记录
        long time = System.currentTimeMillis();
        recordInfo.setTime(time);
        RecordInfoDBUtil.insertRecordInfo(recordInfo);
        recordInfo = new RecordInfo();

        mView.setResult("0000",
                "欢迎光临",
                "欢迎光临");
    }

    @Override
    public void checkTem() {
        float[] floats = {36.5f, 37.1f, 36.3f, 37.2f, 35.5f, 37.4f,};
        mView.setTemData(floats[new Random().nextInt(6)]);
    }

    public void faceRecordsUpload() {
        QueryFaceRecordUtils.getInstance().queryFaceRecord(new QueryFaceRecordCallBack() {
            @Override
            public void queryFaceRecord(FaceRecordInfo info) {
                if (info != null) {
                    info.setFaceCommitId(info.getId());
                    MqttUtils.faceRecordUpload(info);
                    setUping(true);
                    Log.e("faceRecordUpload: ", "恢复网络 上报未上报的第一条" + info.getFaceCommitId());
                } else {
                    setUping(false);
                }
            }
        });

    }

    public void setUping(boolean uping) {
        isUping = uping;
    }

    private void query(String orderId, String orderDate) {
        if (System.currentTimeMillis() - startQueryStatus > 10000) {
            //超过10秒后则展示消费失败，接口超时
//            mView.setPayResult(AppConstants.TRANSACTION_FAILURE_CODE, Utils.getStringByValues(R.string.pay_time_out), Utils.getStringByValues(R.string.pay_fail));
            return;
        }
        Message message = Message.obtain();
        message.what = DINING_QUERY;
        RecordInfo info = new RecordInfo();
        info.setOrderId(orderId);
        info.setOrderDate(orderDate);
        message.obj = info;
        if (handler.hasMessages(DINING_QUERY)) {
            handler.removeMessages(DINING_QUERY);
        }
        handler.sendMessageDelayed(message, 1000);
    }

    /**
     * 登录成功，判断消费记录是否已保存一周，是的话删除
     */
    public void deleteOverWeek() {
        deleteOverWeekRecordRunnable = new Runnable() {
            @Override
            public void run() {
                Log.e("Thread", "-----deleteOverWeek:" + Thread.currentThread().getName());
                RecordInfoDBUtil.deleteFaceRecordInfoOverWeek(System.currentTimeMillis());
                Message message = handler.obtainMessage();
                message.what = AppConstants.DELETE_OVER_WEEK_RECORD;
                if (handler.hasMessages(AppConstants.DELETE_OVER_WEEK_RECORD)) {
                    handler.removeMessages(AppConstants.DELETE_OVER_WEEK_RECORD);
                }
                handler.sendMessage(message);
            }
        };
        DefaultThreadPool.getMaxPool().execute(deleteOverWeekRecordRunnable);
    }


    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        TimerUtils.getInstance().cancelTimer();
        if (deleteOverWeekRecordRunnable != null) {
            DefaultThreadPool.getMaxPool().cancel(deleteOverWeekRecordRunnable);
        }

        if (cyclicRunnable != null) {
            DefaultThreadPool.getMaxPool().cancel(cyclicRunnable);
        }
    }

    private Runnable cyclicRunnable;

    public void startCyclicRunnable() {
        cyclicRunnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        judgeNetConnect();
                    } catch (Exception e) {

                    }

                    try {
                        compareFace();
                    } catch (Exception e) {

                    }

                    try {
                        upFace();
                    } catch (Exception e) {

                    }
                }
            }
        };
        DefaultThreadPool.getMaxPool().execute(cyclicRunnable);
    }

    //判断网络
    private void judgeNetConnect() {
        AppConstantInfo.getInstance().setNetConnect(NetUtils.isNetWorkConnected(mActivity.get()));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //对比人脸
    private void compareFace() {
        if (face != null) {
            Logger.e("--------------开始识别:" + System.currentTimeMillis());
            UserInfo info = null;
            if (face.getFeaceFeature() != null) {
                if (userInfos != null && userInfos.size() != 0) {
                    info = getMatchData(userInfos, face.getFeaceFeature());
                }
            }
            if (handler.hasMessages(AppConstants.COMPARE_FINISH)) {
                handler.removeMessages(AppConstants.COMPARE_FINISH);
            }
            Message message = handler.obtainMessage();
            message.what = AppConstants.COMPARE_FINISH;
            message.obj = info;
            handler.sendMessage(message);
        }
    }

    //上报人脸
    private void upFace() {
        if (faceRecordInfo == null) {
            return;
        }
        long l = FaceRecordInfoDBUtil.insertFaceRecordInfo(faceRecordInfo);
        faceRecordInfo.setFaceCommitId(l);
        FaceRecordInfoDBUtil.updateFaceRecordInfo(l, faceRecordInfo);
//        if (AppConstantInfo.getInstance().isNetConnect() && !isUping) {
//            isUping = true;
//            MqttUtils.faceRecordUpload(faceRecordInfo);
//            Log.e("faceRecordUpload: ", "正常上报" + l);
//        }
//        faceRecordInfo = null;
        if (handler.hasMessages(AppConstants.UP_FACE_RECORD)) {
            handler.removeMessages(AppConstants.UP_FACE_RECORD);
        }
        Message message = handler.obtainMessage();
        message.what = AppConstants.UP_FACE_RECORD;
        handler.sendMessage(message);


    }
}
