package com.hjimi.cmptemdoor.presenter;


import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.hjimi.cmptemdoor.activity.SignInActivity;
import com.hjimi.cmptemdoor.activity.SplashActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.contract.SplashContract;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.interfaces.JudgeNetCallBack;
import com.hjimi.cmptemdoor.utils.JudgeNetUtils;
import com.hjimi.cmptemdoor.utils.PushModelToLocalUtils;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.view.SplashView;

import java.io.File;
import java.lang.ref.SoftReference;


public class SplashPresenter implements SplashContract.Presenter {
    private final SoftReference<SplashActivity> mActivity;
    private Runnable runnable;
    private SplashHandler handler = new SplashHandler(this);

    public SplashPresenter(SplashActivity activity, SplashView tasksView) {
        mActivity = new SoftReference<>(activity);
        tasksView.setPresenter(this);
        copyModel();
    }

    private void copyModel() {
        createAppFolder();
        runnable = new Runnable() {
            @Override
            public void run() {
                Log.e("Thread", "-----copyModel:" + Thread.currentThread().getName());
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //从sp取出数据
                AppConstantInfo.getInstance().setFaceSetId(SpUtil.getLong(PreferenceConstants.FACE_SET_ID, -1));
                AppConstantInfo.getInstance().setFaceSetVer(SpUtil.getLong(PreferenceConstants.FACE_SET_VER, 0));

                AppConstantInfo.getInstance().setMenuPwd(SpUtil.getString(PreferenceConstants.MENU_PASSWORD, AppConstants.MENU_PASSWORD));
                AppConstantInfo.getInstance().setCompareThreshold(SpUtil.getFloat(PreferenceConstants.FACE_COMPARE_THRESHOLD, 60));
                AppConstantInfo.getInstance().setLivnessThreshold(SpUtil.getFloat(PreferenceConstants.FACE_LIVENESS_THRESHOLD, 20));
                AppConstantInfo.getInstance().setNeedLiveness(SpUtil.getBoolean(PreferenceConstants.FACE_NEED_LIVENESS, true));
                AppConstantInfo.getInstance().setVoiceOpen(SpUtil.getBoolean(PreferenceConstants.VOICE_OPEN));
                AppConstantInfo.getInstance().setFdfsToken(SpUtil.getString(PreferenceConstants.FDFS_TOKEN));
                AppConstantInfo.getInstance().setActive(SpUtil.getBoolean(PreferenceConstants.IS_ACTIVE, true));
                AppConstantInfo.getInstance().setBind(SpUtil.getBoolean(PreferenceConstants.IS_BIND, true));

                AppConstantInfo.getInstance().setTem(SpUtil.getBoolean(PreferenceConstants.TEM_OPEN, true));
                AppConstantInfo.getInstance().setTemThreshold(SpUtil.getFloat(PreferenceConstants.TEM_THRESHOLD, 37.2f));

                PushModelToLocalUtils.pushLicense();
                PushModelToLocalUtils.pushConfig();

                Message message = handler.obtainMessage();
                message.what = AppConstants.SPLASH_READY;
                handler.sendMessage(message);
            }
        };
        DefaultThreadPool.getMaxPool().execute(runnable);
    }

    //创建应用的目录
    private void createAppFolder() {
        File modelFile = new File(AppConstants.MODEL_PATH);
        if (!modelFile.exists()) {
            modelFile.mkdirs();
        }

//        File csvFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + AppConstants.CSV_PATH);
//        if (!csvFile.exists()) {
//            csvFile.mkdirs();
//        }
//
//
//        File offlineFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + AppConstants.OFFLINE_PIC);
//        if (!offlineFile.exists()) {
//            offlineFile.mkdirs();
//        }
//
//        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + AppConstants.USER_INFO);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
    }

    private static class SplashHandler extends Handler {
        private SoftReference<SplashPresenter> mPresenter;

        private SplashHandler(SplashPresenter presenter) {
            mPresenter = new SoftReference<>(presenter);
        }


        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case AppConstants.SPLASH_READY:
                    if (mPresenter.get().runnable != null) {
                        DefaultThreadPool.getMaxPool().cancel(mPresenter.get().runnable);
                    }
                    JudgeNetUtils.getInstance().judgeNetConnect(new JudgeNetCallBack() {
                        @Override
                        public void judgeNet(boolean isNetConnect) {
                            if (isNetConnect) {
                                mPresenter.get().mActivity.get().startActivity(new Intent(mPresenter.get().mActivity.get(), SignInActivity.class));
                                mPresenter.get().mActivity.get().finish();
                            } else {
                                JudgeNetUtils.getInstance().toWifiSetting(mPresenter.get().mActivity.get());
                            }
                        }
                    });

                    break;
            }
        }
    }

}
