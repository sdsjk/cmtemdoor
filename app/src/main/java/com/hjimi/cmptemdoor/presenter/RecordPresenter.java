package com.hjimi.cmptemdoor.presenter;


import android.os.Handler;
import android.os.Message;

import com.hjimi.cmptemdoor.activity.RecordActivity;
import com.hjimi.cmptemdoor.contract.RecordContract;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.utils.dbutils.RecordInfoDBUtil;

import java.lang.ref.SoftReference;
import java.util.List;


public class RecordPresenter implements RecordContract.Presenter {
    private final SoftReference<RecordActivity> mActivity;
    private RecordContract.View mView;
    private List<RecordInfo> recordInfos;

    public RecordPresenter(RecordActivity activity, RecordContract.View tasksView) {
        mActivity = new SoftReference<>(activity);
        mView = tasksView;
        tasksView.setPresenter(this);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mView.setData(recordInfos, msg.what);
        }
    };

    @Override
    public void getUsers(int page) {
        Runnable downRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                recordInfos = RecordInfoDBUtil.queryAllRecordInfoByPage(page - 1);
                Message msg = Message.obtain();
                msg.what = page;
                handler.sendMessage(msg);
            }
        };
        DefaultThreadPool.getMaxPool().execute(downRunnable);
    }
}
