package com.hjimi.cmptemdoor.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.gyf.barlibrary.ImmersionBar;


//import com.umeng.analytics.MobclickAgent;

public abstract class BaseDialogFragment extends DialogFragment {

    //沉浸
    private Activity mActivity;
    protected ImmersionBar mImmersionBar;
    private Window mWindow;
    private int mWidth;  //屏幕宽度
    private int mHeight;  //屏幕高度

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = initView(inflater, container);
        initImmersionBar(view);
        return view;
    }

    protected abstract View initView(LayoutInflater inflater, ViewGroup container);


    @Override
    public void onDestroy() {
        // 在fragment销毁的时候同时设置停止请求，停止线程请求回调
        if (mImmersionBar != null && mImmersionBar.getBarParams() != null) {
            mImmersionBar.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getDialog() != null) {
            getDialog().show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(true);  //点击外部消失
        mWindow = dialog.getWindow();
        //测量宽高
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getRealMetrics(dm);
            mWidth = dm.widthPixels;
            mHeight = dm.heightPixels;
        } else {
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            mWidth = metrics.widthPixels;
            mHeight = metrics.heightPixels;
        }
    }

    protected abstract String getTAG();


    /**
     * 初始化沉浸式
     */
    private void initImmersionBar(View view) {
//        mImmersionBar = ImmersionBar.with(this, getDialog());
//        View titleLayout = view.findViewById(R.id.dialog_title_layout);
//        mImmersionBar.statusBarDarkFont(false);
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            mImmersionBar.statusBarColor(R.color.status_color);
//        }
//        mImmersionBar.init();
//        if (titleLayout != null) {
//            mImmersionBar.titleBar(titleLayout).init();
//        }
    }
}
