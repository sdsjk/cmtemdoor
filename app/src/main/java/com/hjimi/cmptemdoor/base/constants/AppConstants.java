package com.hjimi.cmptemdoor.base.constants;

public class AppConstants {
    public final static String PACKAGE_NAME = "com.hjimi.cmtemdoor";

    public final static String LOCAL_BASE = "/Android/data/" + PACKAGE_NAME;
    public final static String LOCAL_PATH = LOCAL_BASE + "/data";
    public final static String OFFLINE_PIC = LOCAL_PATH + "/offlinePic";//存放离线抓拍的图片
    public final static String USER_INFO = LOCAL_PATH + "/userInfo";//存放人员信息的图片
    public final static String CSV_PATH = LOCAL_PATH + "/csvPath";//csv文件

    public static final String MODEL_PATH = "/sdcard/FaceConfig"; //算法初始化所需要的model文件
    public static final String LICENSE_PATH = "/sdcard/"; //算法初始化所需要的license文件
    public static final String CER_PATH = "/sdcard/"; //MQTT ssl证书

    //TODO:需要提供
    public static final String TERMINAL_ID = ""; //荷包接口终端编号
    //提供本地公钥
    public static final String LOCAL_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC31xahnO/PttmVgjPZ+KQEIASqx8XSqgCLpdmfIPcwgFZPxKGcuMFtIlq2JqFp3G50lrp5Eojsd4DwyadIu1qeVjTUJTT36nlK1YD7NlDlwK7UvdD3hZzLscBAzcHG7fiXI8V9iULc6hBJz5e3/s7NEqq4fBNu8X/0HCzcUdRYVQIDAQAB";
    //提供本地私钥
    public static final String LOCAL_PRIVATE_KEY = "30820276020100300D06092A864886F70D0101010500048202603082025C02010002818100B7D716A19CEFCFB6D9958233D9F8A4042004AAC7C5D2AA008BA5D99F20F73080564FC4A19CB8C16D225AB626A169DC6E7496BA791288EC7780F0C9A748BB5A9E5634D42534F7EA794AD580FB3650E5C0AED4BDD0F7859CCBB1C040CDC1C6EDF89723C57D8942DCEA1049CF97B7FECECD12AAB87C136EF17FF41C2CDC51D4585502030100010281801981D155091648E6A0D7DAA1E11B5B7E6B06DABF2F16649A00C63924980954046A7554DDEB85C2E8A67C998B5098048A9AC89304DB6A7E0CB2AA36006CD9F9DF83F2E0636E444FE05B2D177E51B1A547FFDD79377AD656E401633630C6C9614C3778046052712C3A2CA585B455535EEC11EA07A8290804B55515E5194F90B07D024100DF464A74E5789E039078461C6D82E3A1C6DAFC446E7BB6F2A2254D11E253266D0C188F143CE89A0FE31FB1C270F6A8B86DB0494E6A6DB132B55BC0BF36649913024100D2C923F61CE26C56C9290AE9C98271E674478ABC4106EBD5BB237AFBD69B8EDAF151A832C51E059BCAB128BB8B5732664420982A1F21CDD1D36C8C3B280C9DF70240591C08A356E228CE6B5E553D0B47EDE9A71FC706D8C16F66AEE80C365309B49058FF843B238401D8CD60DCE529888815F3512430B3B14BC35D365D5AF32E7B19024100A61D5DF9D7499997CB704CE971A51B9E42851F10BD2E772D390DFD1DFB431C91EF1C2B31D7A69144F5B82821A4EE5309C526EAC447449CAF7FAF4BD452FB9E8102403A874916D6C8F52BEFEE5086430F37D6C8A4129D465BF27A9BD677973F1BBCBC38F905C38D1286624040319C77EF2CAA482649794831FA9377B7A0B458518C01";

    public static final String MQTT_SERVICE_PACKAGE = PACKAGE_NAME + ".service.MQTTService";
    public static final String UPDATE_MQTT = "updateMQTT";

    public static final String ID = "id";
    public static final String PAY_MODE = "pay_mode";
    public static final String MODE_SET_MEAL = "set_meal";
    public static final String MODE_ANY_AMOUNT = "any_amount";
    public static final String KEYBOARD_FACE_PAY = "FACE_PAY";
    public static final String KEYBOARD_SCAN_PAY = "SCAN_PAY";
    public static final String KEYBOARD_CANCEL = "CANCEL";
    public static final String KEYBOARD_CLEAR = "CLEAR";
    public static final String KEYBOARD_LIST = "LIST";
    public static final String KEYBOARD_REFUND = "REFUND";
    public static final String KEYBOARD_F1 = "F1";
    public static final String KEYBOARD_F2 = "F2";
    public static final String KEYBOARD_F3 = "F3";
    public static final String KEYBOARD_BACKSPACE = "Backspace";
    public static final String KEYBOARD_OPT = "OPT";
    public static final String KEYBOARD_0 = "0";

    public static final int PERMISSION_REQUEST_CODE = 100;
    public static final int PERMISSION_APP_SETTING_REQUEST = 200;

    public final static String SUCCESS_CODE = "100";
    public static final int NEW_GET_DEVICE = 28;//是盒子wify
    public static final int NEW_GET_DEVICE_FAILED = 29;//不是盒子wify

    public static final int PAGE_COUNT = 10;

    public static final String MENU_PASSWORD = "12344321";


    public static final String PARAM_REQUEST_ID = "requestId";
    public static final String PARAM_SIGN_TYPE = "signType";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_VERSION = "version";
    public static final String PARAM_MERCHANT_ID = "merchantId";
    public static final String PARAM_TERMINAL_ID = "terminalId";
    public static final String PARAM_ORDER_ID = "orderId";
    public static final String PARAM_ORDER_DATE = "orderDate";
    public static final String PARAM_ORDER_TIME = "orderTime";
    public static final String PARAM_ORDER_AMOUNT = "orderAmount";
    public static final String PARAM_PERIOD = "period";
    public static final String PARAM_PERIOD_UNIT = "periodUnit";
    public static final String PARAM_MOBILE = "mobile";
    public static final String PARAM_PRODUCT_NAME = "productName";
    public static final String PARAM_RESERVED1 = "reserved1";
    public static final String PARAM_RESERVED2 = "reserved2";
    public static final String PARAM_USER_TOKEN = "userToken";
    public static final String PARAM_TERMINAL_WAY = "terminalWay";
    public static final String PARAM_AMOUNT_SOURCE = "amountSource";
    public static final String PARAM_PIRCING_MODEL = "pircingModel";
    public static final String PARAM_DINING_NUMBER = "diningNumber";
    public static final String PARAM_SUC_DATE = "sucDate";
    public static final String PARAM_SUC_TIME = "sucTime";
    public static final String PARAM_HMAC = "hmac";


    public static final String SIGN_TYPE = "RSA";
    public static final String DINING_MERC_KEY = "DiningMercKey";
    public static final String DINING_PAY = "DiningPay";
    public static final String DINING_QUERY = "DiningQuery";
    public static final String DINING_DATE = "DiningDate";
    public static final String VERSION = "2.0.0";

    public static final String DIALOG_TITLE = "dialogTitle";

    public static final String MQTT_REGIST_SUCCESS = "mqttRegistSuccess"; //MQTT 注册成功

    public static final String FDFS_TOKEN = "fdfsToken";
    public static final String VAL = "val";
    public static final String NET_WORK_CONNECTED = "NetWorkConnected";
    public static final String PARMS_TYPE = "type";
    public static final String IS_BIND = "isBind";//是否解绑

    public static final String STRING_0 = "0";
    public static final String STRING_1 = "1";

    public static final String WORKER_ID = "employeeNumber";
    public static final String PHONE = "phone";
    public static final String ORG_NAME = "orgName";

    public static final String TO_MAKE_FEATURE = "toMakeFeature";
    public static final String DOWNLOAD_CSV_FAIL = "downloadCSVFail";

    public static final String SYNCHRONOUS_PROGRESS = "synchronous_progress";

    public static final String FACE_COMMIT_ID = "faceCommitId";
    public static final String RES = "res";
    public static final String FAIL = "fail";
    public static final String APP_URL = "appUrl";

    public static final String TRANSACTION_SUCCESS_CODE = "0000";
    public static final String INSUFFICIENT_FUNDS_CODE = "2016";
    public static final String TRANSACTION_PROCESSING_CODE = "1000";
    public static final String TRANSACTION_FAILURE_CODE = "2000";

    public static final String NUM_DOT = ".";

    public static final String IS_ACTIVE = "isActive";
    public static final String DEV_PARA = "devPara";
    public static final String IS_VOICE_ON = "isVoiceOn";
    public static final String SIMILARITY_THRESHOLD = "similarityThreshold";

    public static final String AGAIN_TO_MAINACTIVITY_TYPE = "againMainActivityType";
    public static final String AGAIN_MAINACTIVITY_TYPE_ACTIVE = "againTypeActive";
    public static final String AGAIN_MAINACTIVITY_TYPE_CSV = "againTypeCSV";

    public static final int UP_DEVICE_CONFIG = 1;//每隔一段时间需要上报配置信息

    public static final int COMPARE_FINISH = 2;//人脸对比结果完成
    public static final int UP_FACE_RECORD = 3;//上报刷脸记录
    public static final int DELETE_OVER_WEEK_RECORD = 4;//删除超过一周的消费记录

    public static final int SPLASH_READY = 5;//splashActivity的准备工作

    public static final String UP_FACE_EXCEPTION = "upFaceException";//上报刷脸信息异常
    public static final String EXCEPTION_TO_RESTART = "exceptionToRestart";//异常捕获后重启app

    public static final int WIFI_REQUEST_CODE = 666;
}
