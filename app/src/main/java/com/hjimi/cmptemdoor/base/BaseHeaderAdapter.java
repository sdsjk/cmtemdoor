package com.hjimi.cmptemdoor.base;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseHeaderAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * view的基本类型，这里只有头/底部/普通，在子类中可以扩展
     */
    static class VIEW_TYPES {

        public static final int HEADER = 1;
        public static final int ITEM = 2;
        public static final int FOOTER = 3;
    }

    private View mHeaderView = null; //header view
    private View mFooterView = null; //footer view

    protected BaseHeaderAdapter() {
        setHasStableIds(true);
    }

    //添加头部
    public void addHeaderView(View headerView) {
        mHeaderView = headerView;
    }

    //添加尾部
    public void addFooterView(View footerView) {
        mFooterView = footerView;
    }

    public OnItemClickListener mOnItemClickListener;
    private OnItemLongClickListener mOnLongItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface OnItemLongClickListener {
        void onLongItemClick(View view, int position);
    }

    @Override
    public int getItemViewType(int position) {
        if (mFooterView != null && position == getItemCount() - 1) {
            return VIEW_TYPES.FOOTER;
        } else if (mHeaderView != null && position == 0) {
            return VIEW_TYPES.HEADER;
        } else {
            if (mHeaderView != null) {
                return getAdvanceViewType(position - 1);
            }
            return getAdvanceViewType(position);
        }
    }

    @Override
    public long getItemId(int position) {
        if (mFooterView != null && position == getItemCount() - 1) {
            return mFooterView.hashCode();
        } else if (mHeaderView != null && position == 0) {
            return mHeaderView.hashCode();
        } else {
            if (mHeaderView != null) {
                return getAdvanceItemId(position - 1);
            }
            return getAdvanceItemId(position);
        }
    }

    protected abstract int getAdvanceViewType(int position);

    /**
     * The value returned can not be 0!!!
     *
     * @return
     */
    protected abstract int getAdvanceItemId(int position);

    protected abstract int getAdvanceCount();

    protected abstract void onBindAdvanceViewHolder(RecyclerView.ViewHolder holder, int i);

    protected abstract VH onCreateAdvanceViewHolder(ViewGroup parent, int viewType);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPES.HEADER && mHeaderView != null) {
            return new HolderBase(mHeaderView);
        } else if (viewType == VIEW_TYPES.FOOTER && mFooterView != null) {
            return new HolderBase(mFooterView);
        }
        return onCreateAdvanceViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
        if ((mHeaderView != null && position == 0) || (mFooterView != null && position == getItemCount() - 1)) {
            if (lp instanceof StaggeredGridLayoutManager.LayoutParams) {
                ((StaggeredGridLayoutManager.LayoutParams) lp).setFullSpan(true);
            }
            // 如果是header或者是footer则不处理
        } else {
            if (lp instanceof StaggeredGridLayoutManager.LayoutParams) {
                ((StaggeredGridLayoutManager.LayoutParams) lp).setFullSpan(false);
            }

            if (mHeaderView != null) {
                position--;
            }
            onBindAdvanceViewHolder(holder, position);

            final int pos = position;
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(pos);
                    }
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mOnLongItemClickListener != null) {
                        mOnLongItemClickListener.onLongItemClick(v, pos);
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        int headerOrFooter = 0;
        if (mHeaderView != null) {
            headerOrFooter++;
        }
        if (mFooterView != null) {
            headerOrFooter++;
        }
        return getAdvanceCount() + headerOrFooter;
    }

    public boolean isHeader(int position) {
        return mHeaderView != null && position == 0;

    }

    public boolean haveHeadView() {
        return mHeaderView != null;
    }

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickListener = mOnItemClickLitener;
    }

    public void setOnLongItemClickListener(OnItemLongClickListener mOnLongItemClickListener) {
        this.mOnLongItemClickListener = mOnLongItemClickListener;
    }

    public static class HolderBase extends RecyclerView.ViewHolder {
        public HolderBase(View itemView) {
            super(itemView);
        }
    }

    public OnItemClickListener getmOnItemClickListener() {
        return mOnItemClickListener;
    }
}
