package com.hjimi.cmptemdoor.base;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
