package com.hjimi.cmptemdoor.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public abstract class BaseFragment extends Fragment {

    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return initView(inflater, container);
    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        if (view != null) {
//            View titleBar = view.findViewById(R.id.title_layout);
//            if (titleBar != null) {
//                ImmersionBar.setTitleBar(mActivity, titleBar);
//            }
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    protected abstract View initView(LayoutInflater inflater, ViewGroup container);


    @Override
    public void onDestroy() {
        //在fragment销毁的时候同时设置停止请求，停止线程请求回调
        super.onDestroy();
    }

    public abstract String getTAG();


//    public void onViewCreated(View view) {
//        if (view != null) {
//            View titleBar = view.findViewById(R.id.title_layout);
//            if (titleBar != null) {
//                ImmersionBar.setTitleBar(mActivity, titleBar);
//            }
//        }
//    }
}
