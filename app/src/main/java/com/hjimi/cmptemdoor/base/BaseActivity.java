package com.hjimi.cmptemdoor.base;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.View;

import com.gyf.barlibrary.ImmersionBar;
import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.SplashActivity;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

public abstract class BaseActivity extends RxAppCompatActivity {
    private boolean isSplashActivity = false;
    protected ImmersionBar mImmersionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews(savedInstanceState);
        initImmersionBar();
        loadData();
        ImiNrApplication.getApplication().getAllActivities().add(this);
    }

    protected abstract void initViews(Bundle savedInstanceState);

    protected void loadData() {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isSplashActivity) {
            for (String PERMISSION : SplashActivity.getPERMISSIONS()) {
                if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(this, PERMISSION)) {
                    ImiNrApplication.getApplication().exitApp();
                    return;
                }
            }
        }
    }

    public abstract Object getTag();

    public void setSplashActivity() {
        isSplashActivity = true;
    }

    /**
     * 初始化沉浸式
     */
    private void initImmersionBar() {
        //在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this);
        View view = findViewById(R.id.sv_color_view);
        if (view != null) {
            mImmersionBar.titleBar(view);
        }
        mImmersionBar.statusBarDarkFont(false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mImmersionBar.statusBarColor(R.color.color_333333);
        }
        mImmersionBar.init();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }


    @Override
    public void finish() {
        if (mImmersionBar != null && mImmersionBar.getBarParams() != null) {
            mImmersionBar.destroy();  //在BaseActivity里销毁
        }
        super.finish();
    }
}
