package com.hjimi.cmptemdoor.base.constants;


public class PreferenceConstants {
    public static final String UNIQUE_IDENTIFIER = "unique_identifier";//设备的唯一标识

    public static final String PAY_MODE = "pay_mode";//消费模式

    public static final String MENU_PASSWORD = "menu_password";//进入菜单密码

    public static final String FACE_COMPARE_THRESHOLD = "faceCompareThreshold";//人脸对比阈值
    public static final String FACE_LIVENESS_THRESHOLD = "faceLivenessThreshold";//活体阈值
    public static final String FACE_NEED_LIVENESS = "faceNeedLiveness";//活体检测
    public static final String VOICE_OPEN = "isVoiceOpen";//语音播报
    public static final String TEM_OPEN = "isTem";//体温测量
    public static final String TEM_THRESHOLD = "temThreshold";//体温阈值

    public static final String TERMINAL_ID = "terminalId";//荷包接口终端编号
    public static final String MERCHANT_ID = "merchantId";//荷包接口商户编号
    public static final String PRIVATE_KEY = "privateKey";//荷包接口私钥
    public static final String FDFS_TOKEN = "fdfsToken";//fdfs密钥
    public static final String FACE_SET_ID = "faceSetId";//人脸库id
    public static final String FACE_SET_VER = "faceSetVer";//人脸库版本
    public static final String FACE_SET_UPDATA_TIME = "faceSetUpdataTime";//更新人脸库时间
    public static final String IS_ACTIVE = "isActive";//是否已激活
    public static final String SYNCHRONIZATION_STATUS = "synchronizationStatus";//同步状态
    public static final String IS_BIND = "isBind";//是否解绑

    public static final String MEAL_AMOUNT = "mealAmount";//套餐金额
    public static final String APP_VERSION = "appVersion";//版本
}
