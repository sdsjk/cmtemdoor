package com.hjimi.cmptemdoor.interfaces;

import com.hjimi.cmptemdoor.databean.FaceRecordInfo;

public interface QueryFaceRecordCallBack {
    void queryFaceRecord(FaceRecordInfo info);
}
