package com.hjimi.cmptemdoor.net;

/**
 * Created by mysterycode on 2018/1/15 0015.
 * Description：所有接口地址(其中包含HEAD字段多位前缀地址)
 */

public class AppNetConfig {

    public static String IP_BASE = "172.16.1.208:8005";

    public static final String BASE = "http://";

    //下单
    public static final String DINING_PAY = "/dining-pay";

    //订单查询
    public static final String DINING_QUERY = "/dining-query";

    //订单支付成功时间同步
    public static final String DINING_DATE = "/dining-date";

    //获取商户秘钥
    public static final String DINING_MERC = "/dining-merc";
}
