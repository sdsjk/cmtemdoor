package com.hjimi.cmptemdoor.net;

/**
 * Created by myseterycode on 2018/1/19 0019.
 * Description:FieldName,固定值
 * Update:
 */

public class FN {
    public static final String VERSION_NAME = "0.9.8";

    public static final String PACKAGE_NAME = "com.hjimi.cmtemdoor";

    public static final String CP_VERSION_ID = "1";

    public static final String TYPE = "type";
    public static final String CONTENT = "content";
    public static final String KEY = "key";
    public static final String ID = "id";
    public static final String CODE = "code";

    public static final String PENALTY_SOURCE = "penaltySource";

    //名称
    public static final String PACKAGE = "com.dataadt.qitongcha";
    public static final String DIVISION_JSON = "division.json";
    public static final String TRADE_JSON = "trade.json";
    public static final String QUA_TYPE_JSON = "QualificationTypes.json";
    public static final String BUWEI_JSON = "Buwei.json";
    public static final String CASE_TYPE = "CaseType.json";
    public static final String COURT = "Court.json";
    public static final String PROVINCE = "Province.json";
    public static final String BRAND = "brand.json";
    public static final String STATUS_JSON = "Status.json";
    public static final String PATENT = "Patent.json";
    public static final String PERSON = "person.json";
    public static final String FINANCING_ROTATION = "FinancingRotation.json";


    //网络状态
    public static final String NO_MORE_DATA = "没有更多数据";
    public static final String NO_NET = "网络波动";

    public static final String BUNDLE = "bundle";
    public static final String LIST = "list";
    public static final String FIRST = "first";
    public static final String SECOND = "second";
    public static final String THIRD = "third";
    public static final String FOUTH = "fouth";
    public static final String FIFTH = "fifth";
    public static final String SIXTH = "six";
    public static final String MAIN_ID = "main_id";
    public static final String HOME_ID = "home_id";

    public static final String USER_INFO = "user_info";
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String HISTORY = "history";
    public static final String COMPANY = "company";
    public static final String STANDARD = "standard";
    public static final String PRODUCT = "product";
    public static final String BID = "bid";
    public static final String URL = "url";

    public static final String NET_ERROR = "网络异常";
    public static final String HOT_WORD = "hot_word";
    public static final String SPLIT = ",";
    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";

    //错误码
    public static final int OVER_TIME = 100007;

    //刷新字段
    //刷新所有页面
    public static final String NOW = "now";

    //页面分类型标记
    //我的发票抬头
    public static final String BILL = "bill";
    //主要人员查看更多列表
    //法人
    public static final String LEGAL_PERSON = "legal_person";
    //股东
    public static final String SHARE_HOLDER = "share_holder";
    //高管
    public static final String MANAGER = "manager";
    public static final String STAFFNAME = "staff_name";
    //分支结构、股东结构（SHARE_HOLDER）、工商变更
    public static final String BRANCH = "branch";
    public static final String COMMERCE = "commerce_change";
    public static final java.lang.String HOME_ENTERPRISR = "home_enterprise";
    public static final String TITLE = "title";
    public static final String STAFFID = "staffId";

    public static final String QUERY_COMPANY = "refresh_history";
    public static final String FOCUS = "refresh_focus";
    public static final String FLAG = "flag";
    public static final String DEVICE_ID = "device_id";

    public static final String HOME_HOT_WORD = "home_hot_word";
    public static final java.lang.String HOME_BID = "home_bid";
    public static final String HOME_PRODUCT = "home_product";
    public static final String BID_HOT_WORD = "bid_hot_word";
    public static final String PRODUCT_HOT_WORD = "product_hot_word";
    public static final String STANDARD_HOT_WORD = "standard_hot_word";
    public static final String DATABASE_LIMIT = "20";
    public static final String DATABASE_ORDER = "id desc";//以id自增长，desc为倒叙查询声明
    public static final String COMPANY_NAME = "company_name";
    public static final String TAX_NUM = "tax_num";
    public static final String ADDRESS = "address";

    public static final String DELETE = "delete";
    public static final String VERSION_INFO = "version_info";
    public static final String TAG = "tag";
    public static final String MATCH_REGEX = "[`~!@#$%^*+=|{}':;',\\[\\]?~！@#￥%……【】‘；：”“’。，？]";
    public static final String ERROR = "加载异常";
    public static final long DELAY = 10;

    public static final String DEBUG = "debug";
    //统计标签
    public static final String MARK = "mark";
    public static final String RESULT = "result";

    //首页接口改版的缓存标记
    public static final String HOME = "home";
    //年报详情
    public static final String YEAR_ZERO = "网站";
    public static final String YEAR_ONE = "股东及出资信息";
    public static final String YEAR_TWO = "对外投资信息";
    public static final String YEAR_THREE = "对外提供保证担保信息";
    public static final String YEAR_FOUR = "股权变更信息";
    //动产抵押
    public static final String MORT_ZERO = "抵押权人信息";
    public static final String MORT_ONE = "被担保主债权信息";
    public static final String MORT_TWO = "抵押物信息";
    public static final String MORT_THREE = "变更信息";

    public static final String COPY_RIGHT_SOFT = "copy_right_soft";
    public static final String COPY_RIGHT_WORK = "copy_right_work";
    public static final String COPY_RIGHT_SOFT_SEARCH = "copy_right_soft_search";
    public static final String COPY_RIGHT_WORK_SEARCH = "copy_right_work_search";
    public static final String COPY_FINISH = "已成功复制到剪贴板";

    public static final String FILTER_DATA = "filter_data";
    public static final String FILTER_DATA_BUNDLE = "filter_data_bundle";
    public static final int FILTER_RESULT_CODE = 111;
    public static final int BTN_FILTER_RESULT_CODE = 222;

    public static final String EMAIL = "email";
    public static final String INFO = "info";

    public static final int SHARE_REQUEST_CODE = 200;
    public static final int SAVE_REQUEST_CODE = 300;
    public static final int WX_REQUEST_CODE = 400;
    public static final int QQ_REQUEST_CODE = 500;

    public static final String YEAR = "year";
    public static final String STATUS = "status";
}
