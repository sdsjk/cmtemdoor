package com.hjimi.cmptemdoor.net;

import com.hjimi.cmptemdoor.bean.DateInfo;
import com.hjimi.cmptemdoor.bean.MercInfo;
import com.hjimi.cmptemdoor.bean.PayInfo;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by myseterycode on 2018/1/19 0019.
 * Description:Retrofit2对应的接口地址
 * Update:
 */

public interface ApiService {

    @POST(AppNetConfig.DINING_PAY)
    Observable<PayInfo> diningPay(@Body RequestBody body);

    @POST(AppNetConfig.DINING_QUERY)
    Observable<PayInfo> diningQuery(@Body RequestBody body);

    @POST(AppNetConfig.DINING_DATE)
    Observable<DateInfo> diningDate(@Body RequestBody body);

    @POST(AppNetConfig.DINING_MERC)
    Observable<MercInfo> diningMerc(@Body RequestBody body);
}
