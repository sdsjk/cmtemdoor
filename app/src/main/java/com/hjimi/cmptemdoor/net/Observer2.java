package com.hjimi.cmptemdoor.net;


import rx.Observer;

/**
 * Created by mystery_code on 2018/4/24 0024.
 * Description:未使用
 * Update:
 */
public abstract class Observer2<T, F> implements Observer<T> {


    @Override
    public void onNext(T value) {
        Result<F> result = (Result<F>) value;
        int code = result.getCode();
        if (code == 0) {
            onSuccess(result.getData());
        } else if (code == 100007) {
//            String phone=SpUtil.getString(FN.PHONE);
//            UserDao userDao = new UserDao(EnterpriseInfoQuery.mContext);
//            userDao.delete(phone);
//            SpUtil.putString(FN.PHONE, "");
//            ToastUtil.showToast("登录过期，请重新登录");
//            EnterpriseInfoQuery.mContext.startActivity(new Intent(EnterpriseInfoQuery.mContext, LoginActivity.class));
        } else {
//            ToastUtil.showToast(result.getMsg());
        }
    }

    @Override
    public void onError(Throwable e) {
        onFailure(e);

    }

    public abstract void onSuccess(F bean);

    public abstract void onFailure(Throwable t);

}
