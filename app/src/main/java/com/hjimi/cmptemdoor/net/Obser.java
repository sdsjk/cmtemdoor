package com.hjimi.cmptemdoor.net;


import rx.Observer;

/**
 * Created by myseterycode on 2018/1/22 0022.
 * Description:网络数据返回的代理类
 * Update:
 */

public abstract class Obser<T> implements Observer<T> {


    @Override
    public void onNext(T t) {
        onSuccess(t);

    }

    @Override
    public void onError(Throwable t) {
        onFailure(t);

    }

    public abstract void onSuccess(T t);

    public abstract void onFailure(Throwable t);

}
