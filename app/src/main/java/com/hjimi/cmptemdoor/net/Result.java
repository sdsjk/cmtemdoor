package com.hjimi.cmptemdoor.net;

/**
 * Created by myseterycode on 2018/4/13 0013.
 * Description:
 * Update:
 */
public class Result<T> {

    /**
     * reqId : 0
     * code : 0
     * msg :
     * desc :
     * data : [{"companyId":0,"labelId":11,"labelCode":"sadds","labelName":"asddasdas"},{"companyId":0,"labelId":32,"labelCode":"sfsdfsd","labelName":"fdsafsfdsfds"}]
     * pageNo : 0
     * pageSize : 0
     * totalCount : 0
     * pageCount : 0
     */

    private int reqId;
    private int code;
    private String msg;
    private String desc;
    private int pageNo;
    private int pageSize;
    private int totalCount;
    private int pageCount;
    private T data;

    public int getReqId() {
        return reqId;
    }

    public void setReqId(int reqId) {
        this.reqId = reqId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
