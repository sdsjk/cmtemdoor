package com.hjimi.cmptemdoor.net;


import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by myseterycode on 2018/1/20 0020.
 * Description:网络封装类。Net的工具类
 * Update:
 */

public class NetUtil {
    public NetUtil() {
    }

    private static NetUtil instance = null;

    public static NetUtil getInstance() {
        if (null == instance) {
            synchronized (NetUtil.class) {
                if (null == instance) {
                    instance = new NetUtil();
                }
            }
        }
        return instance;
    }

    public boolean isVpnUsed() {
        try {
            Enumeration<NetworkInterface> niList = NetworkInterface.getNetworkInterfaces();
            if (niList != null) {
                for (NetworkInterface intf : Collections.list(niList)) {
                    if (!intf.isUp() || intf.getInterfaceAddresses().size() == 0) {
                        continue;
                    }
                    if ("tun0".equals(intf.getName()) || "ppp0".equals(intf.getName())) {
                        return true; // The VPN is up
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }

    public <T> void connect(Observable<T> observable, Obser<T> obser) {
        if (isVpnUsed()) {
            ToastUtils.showToast(ImiNrApplication.getApplication(),"请尝试关闭VPN，再打开");
            return;
        }
        if (null == observable) {
            ToastUtils.showToast(ImiNrApplication.getApplication(),"系统未知异常");
            return;
        }

        observable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(obser);
    }

    public <T> void build(Observable<Result<T>> observable, Observer2<Result<T>, T> observer2) {
        if (null == observable) {
            ToastUtils.showToast(ImiNrApplication.getApplication(),"系统未知异常");
            return;
        }

        observable.subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer2);
    }

}
