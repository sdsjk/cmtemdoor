package com.hjimi.cmptemdoor.net;


import android.util.Log;

import com.hjimi.cmptemdoor.BuildConfig;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.FileUtils;
import com.hjimi.cmptemdoor.utils.HttpsUtils;
import com.hjimi.cmptemdoor.utils.SpUtil;

import java.io.File;
import java.io.IOException;
import java.net.Proxy;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by myseterycode on 2018/1/19 0019.
 * Description:Retrofit封装类
 * Update:
 */

public class Net {
    private Retrofit mRetrofit;

    private Net() {
        Retrofit.Builder builder = new Retrofit.Builder();
        mRetrofit = builder.baseUrl(AppNetConfig.BASE + AppNetConfig.IP_BASE)
                .client(genericClient())
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

    }

    private static Net net = null;

    public static Net getInstance() {
        if (null == net) {
            synchronized (Net.class) {
                if (null == net) {
                    net = new Net();
                }
            }
        }
        return net;
    }

    public ApiService getApiService() {
        return mRetrofit.create(ApiService.class);
    }


    public OkHttpClient genericClient() {
        long SIZE_OF_CACHE = 10 * 1024 * 1024; // 10 MiB
        String cacheFile = ImiNrApplication.getApplication().getCacheDir() + "/http";
        Cache cache = new Cache(new File(cacheFile), SIZE_OF_CACHE);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpsUtils.SSLParams sslSocketFactory = HttpsUtils.getSslSocketFactory(null, null, null);
        return builder.connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                .proxy(BuildConfig.DEBUG ? null : Proxy.NO_PROXY)
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
//                        String[] userInfo = new UserDao(ImiNrApplication.mContext).queryToken(SpUtil.getString(FN.PHONE));
                        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VyX25hbWUiOiJhZG1pbiIsInVzZXJfdHlwZSI6IjAiLCJjbG91ZF9uYW1lIjoiIiwiZGlkIjoxMjMxLCJleHBzIjoiMTU0MDg3NzQyMSIsImlhdHMiOjE1NDA4Nzc0MjF9.gYOFJIjHZiL-MMEnW97jLYw0fHYtaBFZo1SGWLS7sXI";
                        String userId = "";
                        String userCode = SpUtil.getString(FN.PHONE);
                        return chain.proceed(chain.request()
                                .newBuilder()
                                .addHeader("Authorization", "")
                                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                                .addHeader("version", FileUtils.getVersionMsg())
                                .addHeader("connection", "keep-alive")
                                .addHeader("Charset", "utf-8")
                                .build());
                    }
                })
//                .addNetworkInterceptor(CachingControlInterceptor.REWRITE_RESPONSE_INTERCEPTOR)
                .addInterceptor(getHttpLoggingInterceptor())
                .sslSocketFactory(sslSocketFactory.sSLSocketFactory, sslSocketFactory.trustManager)
                .cache(cache)
                .build();
    }

    public HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            public void log(String message) {
                Log.e("http", "message:" + message);
            }
        });
        loggingInterceptor.setLevel(level);
        return loggingInterceptor;
    }

    private String getVerCode() {
        long l = System.currentTimeMillis();
        String s = Long.toHexString(l);
        String substring = s.substring(0, 1);
        String substring1 = s.substring(1, 2);
        String substring2 = s.substring(2, 3);
        String substring3 = s.substring(3, 4);
        String substring4 = s.substring(4);
        StringBuffer sb = new StringBuffer();
        sb.append(substring).append(getRandomCode()).append(substring1).append(getRandomCode()).append(substring2).append(getRandomCode()).append(substring3).append(getRandomCode()).append(substring4);
        return sb.toString().toUpperCase();
    }

    private String getRandomCode() {
        String str = "";
        int randomNum = getRandomNum() + getRandomNum();
        if (randomNum < 10) {
            str += randomNum + getRandomWord() + getRandomNum();
        } else {
            str += randomNum + getRandomWord();
        }
        return str.length() > 6 ? str.substring(0, 6) : str;
    }

    private int getRandomNum() {
        return new Random().nextInt(10);
    }

    private String getRandomWord() {
        String str = "";
        for (int i = 0; i < 4; i++) {
            str = str + (char) (Math.random() * 26 + 'a');
        }
        return str;
    }

    /**
     * udp成功后，将net置空
     */
    public void resetNet() {
        net = null;
    }

}
