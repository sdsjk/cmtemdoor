package com.hjimi.cmptemdoor.wight;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.hjimi.cmptemdoor.R;

import javax.annotation.Nullable;


public  class RecyclerLayout extends RelativeLayout {

    public enum RecyclerhState {
        NoDate,
        Normal_list,
        In_loading,
        Failure_canRetry,
        Failure_notRetry
    }

    private  RelativeLayout emptyLayout, mLoadingLayout;
    private TextView tvMsg, tvLoading;
    private OnClickRetryListener clickRetryListener;
    private OnRecyclerStateChangeListener stateChangeListener;
    private RecyclerhState currentState;
    private View containView;
    private ImageView ivNormal;


    public RecyclerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        View inflate = LayoutInflater.from(context).inflate(R.layout.recycler_layout, this);
        mLoadingLayout = (RelativeLayout) inflate.findViewById(R.id.loading_layout);
        emptyLayout = (RelativeLayout) inflate.findViewById(R.id.center_layout);

        SimpleDraweeView loadingProgress = (SimpleDraweeView) inflate.findViewById(R.id.iv_loading);
        loadGif(loadingProgress);

        ivNormal = (ImageView) inflate.findViewById(R.id.iv_normal);

        tvLoading = (TextView) inflate.findViewById(R.id.tv_loading_show);

        tvMsg = (TextView) inflate.findViewById(R.id.tv_progress_show);
        tvMsg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickRetryListener != null) {
                    clickRetryListener.onRetry();
                }
            }
        });
    }

    /**
     * 设置正常的页面
     * recyclerview或者其它view 均可
     *
     * @param id
     */
    public void setContainView(int id) {
        containView = findViewById(id);
    }

    /**
     * 状态变化
     *
     * @param state
     * @param msg
     */
    public void changeState(RecyclerhState state, @Nullable String msg) {
        changeState(state, msg, -1);
    }

    public void changeState(RecyclerhState state, @Nullable String msg, int resId) {
        currentState = state;
        if (containView == null) {
            return;
        }
        switch (state) {
            case Normal_list:
                containView.setVisibility(View.VISIBLE);
                emptyLayout.setVisibility(View.GONE);
                mLoadingLayout.setVisibility(View.GONE);
                break;
            case In_loading:
                containView.setVisibility(View.GONE);
                emptyLayout.setVisibility(View.GONE);
                mLoadingLayout.setVisibility(View.VISIBLE);
                tvLoading.setText(msg);
                break;
            case Failure_canRetry:
                containView.setVisibility(View.GONE);
                emptyLayout.setVisibility(View.VISIBLE);
                mLoadingLayout.setVisibility(View.GONE);
                tvMsg.setText(msg);
                tvMsg.setClickable(true);
                break;
            case NoDate:
            case Failure_notRetry:
                containView.setVisibility(View.GONE);
                mLoadingLayout.setVisibility(View.GONE);
                emptyLayout.setVisibility(View.VISIBLE);
                if (resId == -1) {
//                    ivNormal.setVisibility(View.GONE);
                } else {
                    initHolderImage(resId);
                }
                tvMsg.setText(msg);
                tvMsg.setClickable(false);
                break;

        }

        if (stateChangeListener != null) {
            stateChangeListener.changeState(state);
        }
    }

    public RecyclerhState getCurrentState() {
        return currentState;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        if (containView != null && containView instanceof RecyclerView) {
            ((RecyclerView) containView).setAdapter(adapter);
        }
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        if (containView != null && containView instanceof RecyclerView) {
            ((RecyclerView) containView).setLayoutManager(layoutManager);
        }
    }

    private void loadGif(SimpleDraweeView simpleDraweeView) {
        Uri uri = new Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(String.valueOf(R.drawable.loading))
                .build();
        DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)
                .setOldController(simpleDraweeView.getController()).build();
        simpleDraweeView.setController(draweeController);
    }

    private void initHolderImage(int resId) {
        if (ivNormal != null) {
            ivNormal.setImageResource(resId);
        }
    }

    public void setOnClickRetryListener(OnClickRetryListener retryListener) {
        clickRetryListener = retryListener;
    }

    public void setOnRecyclerStateChangeListener(OnRecyclerStateChangeListener stateChangeListener) {
        this.stateChangeListener = stateChangeListener;
    }

    public interface OnClickRetryListener {
        void onRetry();
    }

    public interface OnRecyclerStateChangeListener {
        void changeState(RecyclerhState state);
    }
}
