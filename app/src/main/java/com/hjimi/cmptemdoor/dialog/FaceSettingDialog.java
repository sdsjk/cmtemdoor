package com.hjimi.cmptemdoor.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.interfaces.OnFaceSettingListener;
import com.hjimi.cmptemdoor.utils.EditUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;

public class FaceSettingDialog extends Dialog implements View.OnClickListener {
    private Context mContext;
    private OnFaceSettingListener mListener;
    private TextView tvTitle;
    private EditText etSetting;
    private String title, setting;

    public FaceSettingDialog(Context context) {
        super(context);
    }

    public FaceSettingDialog(Context context, String title, String setting, OnFaceSettingListener listener) {
        super(context);
        mListener = listener;
        mContext = context;
        this.title = title;
        this.setting = setting;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);// 设置默认键盘不弹出
        setContentView(R.layout.dialog_face_setting);
        initView();
    }

    private void initView() {
        findViewById(R.id.tv_sure).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        etSetting = (EditText) findViewById(R.id.et_setting);
        tvTitle.setText(title);
        etSetting.setText(setting);
        etSetting.setSelection(setting.length());
        etSetting.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null && s.toString().equals(".")) {
                    etSetting.setText("");
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sure:
                String trim = etSetting.getText().toString().trim();
                if (TextUtils.isEmpty(trim)) {
                    ToastUtils.showToast(mContext, Utils.getStringByValues(R.string.input_empty));
                    return;
                }
                float threshold = Float.parseFloat(trim);
                if (Utils.getStringByValues(R.string.tem_setting_1).equals(title)) {
                    if (threshold > 40 || threshold < 35) {
                        ToastUtils.showToast(mContext, Utils.getStringByValues(R.string.threshold_area_1));
                        return;
                    }
                }
                if (threshold > 100 || threshold < 0) {
                    ToastUtils.showToast(mContext, Utils.getStringByValues(R.string.threshold_area));
                    return;
                }
                EditUtils.hideInput(mContext, etSetting);
                if (setting.equals(trim)) {
                    dismiss();
                    return;
                }
                mListener.onCallback(trim);
                dismiss();
                break;
            case R.id.tv_cancel:
                EditUtils.hideInput(mContext, etSetting);
                dismiss();
                break;
        }
    }
}
