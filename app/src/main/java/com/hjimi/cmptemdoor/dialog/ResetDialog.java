package com.hjimi.cmptemdoor.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.hjimi.cmptemdoor.R;

public class ResetDialog extends Dialog implements View.OnClickListener {
    private OnClickListener mListener;

    public ResetDialog(Context context) {
        super(context);
    }

    public ResetDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ResetDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public ResetDialog(Context context, OnClickListener listener) {
        super(context);
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_reset);
        initView();
    }

    private void initView() {
        findViewById(R.id.tv_sure).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sure:
                mListener.onClick(this, 0);
                dismiss();
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
        }
    }
}
