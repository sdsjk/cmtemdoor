package com.hjimi.cmptemdoor.dialog;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.BaseDialogFragment;
import com.hjimi.cmptemdoor.interfaces.SuccessCallBack;
import com.hjimi.cmptemdoor.view.InitErrorDialogView;


/**
 * 通用的常见的弹框
 */
public class InitErrorDialog extends BaseDialogFragment {
    private static final String TAG = "InitErrorDialog";
    private View view;
    private SuccessCallBack callBack;

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.pay_mode));
        WindowManager.LayoutParams layoutParams = getDialog().getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER;
        getDialog().getWindow().setAttributes(layoutParams);
        setCancelable(false);

    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container) {
        view = inflater.inflate(R.layout.dialog_init_error, null, false);
        InitErrorDialogView dialogView = InitErrorDialogView.newInstance(this);
        dialogView.setCallBack(callBack);
        return view;
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    public void setCallBack(SuccessCallBack callBack) {
        this.callBack = callBack;
    }

}
