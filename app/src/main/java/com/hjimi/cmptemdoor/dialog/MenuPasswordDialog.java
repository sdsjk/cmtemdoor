package com.hjimi.cmptemdoor.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.interfaces.OnFaceSettingListener;
import com.hjimi.cmptemdoor.utils.EditUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;

public class MenuPasswordDialog extends Dialog implements View.OnClickListener {
    private Context mContext;
    private OnFaceSettingListener mListener;
    private EditText etSetting;

    public MenuPasswordDialog(Context context) {
        super(context);
    }

    public MenuPasswordDialog(Context context, OnFaceSettingListener listener) {
        super(context);
        mListener = listener;
        mContext = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);// 设置默认键盘不弹出
        setContentView(R.layout.dialog_menu_password);
        initView();
    }

    private void initView() {
        findViewById(R.id.tv_sure).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        etSetting = (EditText) findViewById(R.id.et_setting);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sure:
                //正式要注释这行
//                mListener.onCallback("");
//                dismiss();

                String trim = etSetting.getText().toString().trim();
                if (TextUtils.isEmpty(trim)) {
                    ToastUtils.showToast(mContext, Utils.getStringByValues(R.string.input_empty));
                    return;
                }

                EditUtils.hideInput(mContext, etSetting);
                if (AppConstantInfo.getInstance().getMenuPwd().equals(trim)) {
                    mListener.onCallback(trim);
                    dismiss();
                } else {
                    ToastUtils.showToast(mContext, Utils.getStringByValues(R.string.password_error));
                    etSetting.setText("");
                }
                break;
            case R.id.tv_cancel:
                EditUtils.hideInput(mContext, etSetting);
                dismiss();
                break;
        }
    }
}
