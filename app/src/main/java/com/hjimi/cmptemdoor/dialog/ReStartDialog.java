package com.hjimi.cmptemdoor.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.hjimi.cmptemdoor.R;

public class ReStartDialog extends Dialog implements View.OnClickListener {
    private OnClickListener mListener;

    public ReStartDialog(Context context) {
        super(context);
    }

    public ReStartDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ReStartDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public ReStartDialog(Context context, OnClickListener listener) {
        super(context);
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_restart);
        initView();
    }

    private void initView() {
        findViewById(R.id.tv_sure).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sure:
                mListener.onClick(this, 0);
                dismiss();
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
        }
    }
}
