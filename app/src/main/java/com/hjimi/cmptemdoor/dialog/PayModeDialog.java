package com.hjimi.cmptemdoor.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.utils.SpUtil;

public class PayModeDialog extends Dialog implements View.OnClickListener {
    private DialogInterface.OnClickListener mListener;
    private ImageView ivArbitrarily, ivMeal;
    private int type = 0;

    public PayModeDialog(Context context) {
        super(context);
    }

    public PayModeDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected PayModeDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public PayModeDialog(Context context, OnClickListener listener) {
        super(context);
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_pay_mode);
        initView();
    }

    private void initView() {
        findViewById(R.id.tv_sure).setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        findViewById(R.id.ll_arbitrarily).setOnClickListener(this);
        findViewById(R.id.ll_meal).setOnClickListener(this);
        ivArbitrarily = (ImageView) findViewById(R.id.iv_arbitrarily);
        ivMeal = (ImageView) findViewById(R.id.iv_meal);
        String payMode = SpUtil.getString(PreferenceConstants.PAY_MODE);
        if (AppConstants.MODE_SET_MEAL.equals(payMode)){
            ivMeal.setImageResource(R.mipmap.img_choose);
            ivArbitrarily.setImageResource(R.mipmap.img_no_choose);
            type = 1;
        }else {
            ivArbitrarily.setImageResource(R.mipmap.img_choose);
            ivMeal.setImageResource(R.mipmap.img_no_choose);
            type = 0;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_sure:
                mListener.onClick(this, type);
                dismiss();
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.ll_arbitrarily:
                type = 0;
                ivArbitrarily.setImageResource(R.mipmap.img_choose);
                ivMeal.setImageResource(R.mipmap.img_no_choose);
                break;
            case R.id.ll_meal:
                type = 1;
                ivMeal.setImageResource(R.mipmap.img_choose);
                ivArbitrarily.setImageResource(R.mipmap.img_no_choose);
                break;
        }
    }
}
