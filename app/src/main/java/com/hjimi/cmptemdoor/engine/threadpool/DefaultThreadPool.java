package com.hjimi.cmptemdoor.engine.threadpool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;

public class DefaultThreadPool {


    private static ThreadPoolProxy mLongPool = null;
    private static Object mFrquentlyLock = new Object();

    /**
     * 做大量的短耗时任务，列如：文件操作，网络操作,数据库操作等
     */
    public static ThreadPoolProxy getMaxPool() {
        synchronized (mFrquentlyLock) {
            if (mLongPool == null) {
                mLongPool = new ThreadPoolProxy(0, 1024, 10, new SynchronousQueue<Runnable>());
            }
            return mLongPool;
        }
    }

    /**
     * 移除所有的任务，退出的时候用
     */
    public static void removeAllTask() {
        if (mLongPool != null) {
            mLongPool.removeAllTask();
            mLongPool.stop();
        }
    }

    public static class ThreadPoolProxy {
        private  ExecutorService executorService;

//        private ThreadPoolExecutor mPool;
        private int mCorePoolSize;
        private int mMaximumPoolSize;
        private long mKeepAliveTime;
        private BlockingQueue<Runnable> mWorkQueue;

        /**
         * 线程池构造方法中，赋值最小线程数，最大线程数，空闲存活时间
         */
        private ThreadPoolProxy(int corePoolSize, int maximumPoolSize, long keepAliveTime, BlockingQueue<Runnable> workQueue) {
            mCorePoolSize = corePoolSize;
            mMaximumPoolSize = maximumPoolSize;
            mKeepAliveTime = keepAliveTime;
            mWorkQueue = workQueue;
        }

        /**
         * 执行任务，当线程池处于关闭，将会重新创建新的线程池
         */
        public synchronized void execute(Runnable run) {
            if (run == null) {
                return;
            }
            if (executorService == null || executorService.isShutdown()) {
                executorService = Executors.newCachedThreadPool();
            }
            executorService.execute(run);
        }

        /**
         * 取消线程池中某个还未执行的任务
         */
        public synchronized void cancel(Runnable run) {

        }

//        /**
//         * 取消线程池中某个还未执行的任务
//         */
//        public synchronized boolean contains(Runnable run) {
//            if (executorService != null && (!executorService.isShutdown() || executorService.isTerminated())) {
//                return executorService.contains(run);
//            } else {
//                return false;
//            }
//        }

        /**
         * 删除队列中所有的任务
         */
        public synchronized void removeAllTask() {
            if (mWorkQueue != null && mWorkQueue.size() > 0) {
                mWorkQueue.clear();
            }
        }

        /**
         * 立刻关闭线程池，并且正在执行的任务也将会被中断
         */
        public void stop() {
            if (executorService != null && (!executorService.isShutdown() || executorService.isTerminated())) {
                executorService.shutdownNow();
            }
        }

        /**
         * 平缓关闭单任务线程池，但是会确保所有已经加入的任务都将会被执行完毕才关闭
         */
        public synchronized void shutdown() {
            if (executorService != null && (!executorService.isShutdown() || executorService.isTerminated())) {
                executorService.shutdown();
            }
        }
    }
}

