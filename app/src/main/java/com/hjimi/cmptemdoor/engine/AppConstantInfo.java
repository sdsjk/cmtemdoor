package com.hjimi.cmptemdoor.engine;

public class AppConstantInfo {
    //存放存储在sp中的参数，但是app内随时会用的数据
    private String menuPwd;//进入菜单的密码
    private float compareThreshold;//人脸对比阈值
    private float livnessThreshold;//活体阈值
    private boolean isNeedLiveness;//活体检测开关
    private boolean isVoiceOpen;//语音播报开关
    private float temThreshold;//人脸对比阈值
    private boolean isTem;//体温测量开关
    private String terminalId;//终端编号
    private String merchantId;//商户编号
    private String privateKey;//私钥
    private String fdfsToken;//fdfs密钥
    private long faceSetId;//人脸库id
    private long faceSetVer;//人脸库版本
    private long faceSetUpdataTime;
    private boolean isActive;//是否已激活
    private boolean isBack;//是否已激活
    private int synchronizationStatus;//同步状态 0：没有同步过 1：同步完成 2：同步中
    private boolean isBind;//是否解绑
    private boolean isNetConnect = false;//是否外网连接
    private String devSN;

    public String getDevSN() {
        return devSN;
    }

    public void setDevSN(String devSN) {
        this.devSN = devSN;
    }

    private AppConstantInfo() {
    }

    private static class AppConstantHolder {
        private static final AppConstantInfo INSTANCE = new AppConstantInfo();
    }

    public float getTemThreshold() {
        return temThreshold;
    }

    public void setTemThreshold(float temThreshold) {
        this.temThreshold = temThreshold;
    }

    public boolean isTem() {
        return isTem;
    }

    public void setTem(boolean tem) {
        isTem = tem;
    }

    public long getFaceSetId() {
        return faceSetId;
    }

    public void setFaceSetId(long faceSetId) {
        this.faceSetId = faceSetId;
    }

    public long getFaceSetVer() {
        return faceSetVer;
    }

    public boolean isBack() {
        return isBack;
    }

    public void setBack(boolean back) {
        isBack = back;
    }

    public void setFaceSetVer(long faceSetVer) {
        this.faceSetVer = faceSetVer;
    }

    public long getFaceSetUpdataTime() {
        return faceSetUpdataTime;
    }

    public void setFaceSetUpdataTime(long faceSetUpdataTime) {
        this.faceSetUpdataTime = faceSetUpdataTime;
    }

    public static AppConstantInfo getInstance() {
        return AppConstantHolder.INSTANCE;
    }

    public String getMenuPwd() {
        return menuPwd;
    }

    public void setMenuPwd(String menuPwd) {
        this.menuPwd = menuPwd;
    }

    public float getCompareThreshold() {
        return compareThreshold;
    }

    public void setCompareThreshold(float compareThreshold) {
        this.compareThreshold = compareThreshold;
    }

    public float getLivnessThreshold() {
        return livnessThreshold;
    }

    public void setLivnessThreshold(float livnessThreshold) {
        this.livnessThreshold = livnessThreshold;
    }

    public boolean isNeedLiveness() {
        return isNeedLiveness;
    }

    public void setNeedLiveness(boolean needLiveness) {
        isNeedLiveness = needLiveness;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getFdfsToken() {
        return fdfsToken;
    }

    public void setFdfsToken(String fdfsToken) {
        this.fdfsToken = fdfsToken;
    }

    public boolean isVoiceOpen() {
        return isVoiceOpen;
    }

    public void setVoiceOpen(boolean voiceOpen) {
        isVoiceOpen = voiceOpen;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getSynchronizationStatus() {
        return synchronizationStatus;
    }

    public void setSynchronizationStatus(int synchronizationStatus) {
        this.synchronizationStatus = synchronizationStatus;
    }

    public boolean isBind() {
        return isBind;
    }

    public void setBind(boolean bind) {
        isBind = bind;
    }

    public boolean isNetConnect() {
        return isNetConnect;
    }

    public void setNetConnect(boolean netConnect) {
        isNetConnect = netConnect;
    }
}
