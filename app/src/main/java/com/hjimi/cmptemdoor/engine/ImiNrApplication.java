package com.hjimi.cmptemdoor.engine;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;

import com.hjimi.cmptemdoor.BuildConfig;
import com.hjimi.cmptemdoor.base.BaseActivity;
import com.hjimi.cmptemdoor.service.net.ReceiverInstance;
import com.hjimi.cmptemdoor.utils.frescoUtil.FrescoPlusConfig;
import com.hjimi.cmptemdoor.utils.frescoUtil.FrescoPlusInitializer;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ImiNrApplication extends Application {
    private static final String TAG = "application";
    private static ImiNrApplication application;
    /**
     * 主线程ID
     */

    private int mMainThreadId = -1;
    /**
     * 主线程Looper
     */
    private Looper mMainLooper;


    /**
     * 记录所有活动的Activity
     */
    private List<BaseActivity> mActivities = new LinkedList<>();

    public static ImiNrApplication getApplication() {
        if (application == null) {
            application = new ImiNrApplication();
        }
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mMainThreadId = android.os.Process.myTid();
        mMainLooper = getMainLooper();
        application = this;
        if (BuildConfig.DEBUG) {
            Logger.init(TAG)
                    .logLevel(LogLevel.FULL); //Use LogLevel.NONE for the release versions
//            Stetho.initializeWithDefaults(this);
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .build());
        } else {
            Logger.init(TAG)
                    .logLevel(LogLevel.NONE);
        }


        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());

        FrescoPlusConfig config = FrescoPlusConfig.newBuilder(this)
                .setDebug(true)
                .setTag("smartPicture")
                .setBitmapConfig(Bitmap.Config.RGB_565)
                .setDiskCacheDir(this.getCacheDir())
                .setMaxDiskCacheSize(30)
                .build();
        FrescoPlusInitializer.getInstance().init(this, config);
        registerLiveListener();

        ReceiverInstance.getInstance(this);
    }


    public List<BaseActivity> getAllActivities() {
        return mActivities;
    }

    /**
     * 关闭所有Activity
     */
    public void finishAll() {
        List<BaseActivity> copy;
        synchronized (mActivities) {
            copy = new ArrayList<>(mActivities);
        }
        for (BaseActivity activity : copy) {
            activity.finish();
        }
    }

    public long getMainThreadId() {
        return mMainThreadId;
    }

    /**
     * 获取主线程的looper
     */

    public Looper getMainThreadLooper() {

        return mMainLooper;
    }

    private SoftReference<BaseActivity> currentActivity;

    public BaseActivity getCurrentActivity() {
        if (null == currentActivity) {
            return null;
        }
        return currentActivity.get();
    }

    private int mFinalCount;

    private void registerLiveListener() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                mFinalCount++;
                if (mFinalCount == 1) {
                    //前台

                }
            }

            @Override
            public void onActivityResumed(Activity activity) {
                if (activity instanceof BaseActivity) {
                    currentActivity = new SoftReference<>((BaseActivity) activity);
                }

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                mFinalCount--;
                if (mFinalCount == 0) {
                    //后台

                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    public boolean applicationIsFront() {
        if (mFinalCount == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 退出应用
     */
    public void exitApp() {
//        if (Utils.isWorked(application, AppConstants.MQTT_SERVICE_PACKAGE)) {
//            Intent intent = new Intent(application, MQTTService.class);
//            application.stopService(intent);
//        }
        finishAll();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }, 200);
    }

}
