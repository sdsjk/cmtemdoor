package com.hjimi.cmptemdoor.service.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.entity.BusInfo;
import com.hjimi.cmptemdoor.interfaces.JudgeNetCallBack;
import com.hjimi.cmptemdoor.utils.JudgeNetUtils;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

public class ReceiverInstance {
    private NetWorkReceiver netWorkReceiver;
    private Context context;

    public static ReceiverInstance getInstance(Context context) {
        return new ReceiverInstance(context);
    }

    private ReceiverInstance(Context context) {
        this.context = context;
        register();
    }

    private void register() {
        registerNetChangeReceiver();
    }

    /**
     * 注册网络变换广播接收器
     */
    private void registerNetChangeReceiver() {
        netWorkReceiver = new NetWorkReceiver();
        IntentFilter nFilter = new IntentFilter();
        nFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        nFilter.setPriority(1000);
        context.registerReceiver(netWorkReceiver, nFilter);//注册网路变化广播
    }

    /**
     * 监控网路变换，主要做整个应用的切换网络工作
     */
    public class NetWorkReceiver extends BroadcastReceiver {
        private long standerTime = 0;

        @Override
        public void onReceive(final Context context, Intent intent) {


            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            //移动数据
            assert connectivityManager != null;
            final NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            //wifi网络
            final NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);


            long current = System.currentTimeMillis();
            if ((current - standerTime) > 1500) {
                standerTime = current;
                Logger.e("网络切换");

                JudgeNetUtils.getInstance().judgeNetConnect(new JudgeNetCallBack() {
                    @Override
                    public void judgeNet(boolean isNetConnect) {
                        BusInfo info = new BusInfo();
                        info.setKey(AppConstants.NET_WORK_CONNECTED);
                        info.setObj(isNetConnect);
                        EventBus.getDefault().post(info);
                    }
                });
            }
        }

    }
}
