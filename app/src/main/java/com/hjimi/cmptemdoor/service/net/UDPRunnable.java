package com.hjimi.cmptemdoor.service.net;


import android.os.Handler;
import android.os.Message;
import android.os.Process;

import com.hjimi.cmptemdoor.base.constants.AppConstants;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class UDPRunnable implements Runnable {
    private static final String GROUP_IP = "224.0.0.1";
    private static final int MULTICAST_PORT = 9998;
    private static final String GET_DEVICE_TAG = "OPENSETv2.0";
    private Handler mHandler;

    public UDPRunnable(Handler handler) {
        mHandler = handler;
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        MulticastSocket multicastSocket = null;
        try {
            multicastSocket = new MulticastSocket(MULTICAST_PORT);
            multicastSocket.setLoopbackMode(true);
            multicastSocket.setSoTimeout(5000);
            InetAddress group = InetAddress.getByName(GROUP_IP);
            multicastSocket.joinGroup(group);
            byte[] sendData = GET_DEVICE_TAG.getBytes();
            DatagramPacket packet = new DatagramPacket(sendData, sendData.length, group, MULTICAST_PORT);
            multicastSocket.send(packet);
            byte[] receiveData = new byte[256];
            packet = new DatagramPacket(receiveData, receiveData.length);
            multicastSocket.receive(packet);
            String result = new String(receiveData, 0, packet.getLength());
            Message msg = mHandler.obtainMessage();
            msg.obj = result;
            msg.what = AppConstants.NEW_GET_DEVICE;//是盒子wify
            mHandler.sendMessage(msg);
            multicastSocket.close();
        } catch (IOException e) {
            Message msg = mHandler.obtainMessage();
            msg.what = AppConstants.NEW_GET_DEVICE_FAILED;//不是盒子wify
            msg.obj = e.getMessage();
            mHandler.sendMessage(msg);

        } finally {
            if (multicastSocket != null && !multicastSocket.isClosed()) {
                multicastSocket.close();
                multicastSocket = null;
            }
        }
    }
}
