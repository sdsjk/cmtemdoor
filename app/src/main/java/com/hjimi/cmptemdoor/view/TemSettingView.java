package com.hjimi.cmptemdoor.view;


import android.content.Intent;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.TemSettingActivity;
import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.contract.TemSettingContract;
import com.hjimi.cmptemdoor.dialog.FaceSettingDialog;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.interfaces.OnFaceSettingListener;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.Utils;

import java.lang.ref.SoftReference;


public class TemSettingView implements TemSettingContract.View, View.OnClickListener {
    private SoftReference<TemSettingActivity> mActivity;
    private TemSettingContract.Presenter mPresenter;
    private TextView tvTemThreshold;
    private Switch cbTemSwitch;

    public static TemSettingView newInstance(TemSettingActivity activity) {
        return new TemSettingView(activity);
    }

    private TemSettingView(TemSettingActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        mActivity.get().findViewById(R.id.iv_back).setOnClickListener(this);
        mActivity.get().findViewById(R.id.iv_home).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_tem_threshold).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_tem).setOnClickListener(this);

        tvTemThreshold = (TextView) mActivity.get().findViewById(R.id.tv_tem_threshold);
        cbTemSwitch = (Switch) mActivity.get().findViewById(R.id.checkbox_switch_tem);

        tvTemThreshold.setText(String.valueOf(AppConstantInfo.getInstance().getTemThreshold()));
        cbTemSwitch.setSelected(AppConstantInfo.getInstance().isTem());



        cbTemSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbTemSwitch.isSelected()) {
                    cbTemSwitch.setSelected(false);
                } else {
                    cbTemSwitch.setSelected(true);
                }
                AppConstantInfo.getInstance().setTem(cbTemSwitch.isSelected());
                SpUtil.putBoolean(PreferenceConstants.TEM_OPEN, cbTemSwitch.isSelected());
                //TODO:体温设置是否上报
//                MqttUtils.devConfigUpload(false);
            }
        });
    }

    @Override
    public void setPresenter(TemSettingContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_home:
                mActivity.get().startActivity(new Intent(mActivity.get(), MainActivity.class));
                mActivity.get().finish();
                break;
            case R.id.ll_tem_threshold:
                //人脸对比阈值
                showDialog(Utils.getStringByValues(R.string.tem_setting_1), tvTemThreshold);
                break;
            case R.id.iv_back:
                mActivity.get().finish();
                break;
        }
    }

    private void showDialog(String title, final TextView value) {
        new FaceSettingDialog(mActivity.get(), title, value.getText().toString(), new OnFaceSettingListener() {
            @Override
            public void onCallback(String setting) {
                saveSp(value, Float.valueOf(setting));
                value.setText(setting);
            }
        }).show();
    }

    private void saveSp(TextView textView, float value) {
        switch (textView.getId()) {
            case R.id.tv_tem_threshold:
                //温度阈值
                AppConstantInfo.getInstance().setTemThreshold(value);
                SpUtil.putFloat(PreferenceConstants.TEM_THRESHOLD, value);
                break;
        }
        //TODO:体温设置是否上报
//        MqttUtils.devConfigUpload(false);
    }
}