package com.hjimi.cmptemdoor.view;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.activity.SearchActivity;
import com.hjimi.cmptemdoor.activity.UserListActivity;
import com.hjimi.cmptemdoor.adapters.UserListAdapter;
import com.hjimi.cmptemdoor.base.BaseHeaderAdapter;
import com.hjimi.cmptemdoor.contract.UserListContract;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.wight.RecyclerLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;


public class UserListView implements UserListContract.View, View.OnClickListener {
    private SoftReference<UserListActivity> mActivity;
    private UserListContract.Presenter mPresenter;
    private RefreshLayout mRefreshLayout;
    private int page = 1;
    private RecyclerLayout recyclerLayout;
    private UserListAdapter adapter;
    private List<UserInfo> list=new ArrayList<>();

    public static UserListView newInstance(UserListActivity activity) {
        return new UserListView(activity);
    }

    private UserListView(UserListActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        mActivity.get().findViewById(R.id.iv_back).setOnClickListener(this);
        mActivity.get().findViewById(R.id.iv_home).setOnClickListener(this);
        mActivity.get().findViewById(R.id.iv_search).setOnClickListener(this);
        mRefreshLayout = (RefreshLayout) mActivity.get().findViewById(R.id.refreshLayout);
        recyclerLayout = (RecyclerLayout) mActivity.get().findViewById(R.id.recycler_layout);
        recyclerLayout.setContainView(R.id.recycler);

        recyclerLayout.setLayoutManager(new GridLayoutManager(mActivity.get(), 2));
        adapter = new UserListAdapter(mActivity.get());
        recyclerLayout.setAdapter(adapter);
        recyclerLayout.setOnRecyclerStateChangeListener(new RecyclerLayout.OnRecyclerStateChangeListener() {
            @Override
            public void changeState(RecyclerLayout.RecyclerhState state) {
                switch (state) {
                    case In_loading:
                        mRefreshLayout.setEnableRefresh(false);
                        mRefreshLayout.setEnableLoadMore(false);
                        break;
                    default:
                        mRefreshLayout.setEnableRefresh(true);
                        mRefreshLayout.setEnableLoadMore(true);
                        break;
                }
            }
        });
        adapter.setOnItemClickLitener(new BaseHeaderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Log.e("onItemClick: ", position + "");
            }
        });
    }

    @Override
    public void setPresenter(UserListContract.Presenter presenter) {
        this.mPresenter = presenter;
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refresh();
            }
        });
        //加载更多
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mPresenter.getUsers(page + 1);
            }
        });
        setGetList();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_home:
                mActivity.get().startActivity(new Intent(mActivity.get(), MainActivity.class));
                mActivity.get().finish();
                break;
            case R.id.iv_back:
                mActivity.get().finish();
                break;
            case R.id.iv_search:
                Intent intent = new Intent(mActivity.get(), SearchActivity.class);
                intent.putExtra(AppConstants.PARMS_TYPE, 0);
                mActivity.get().startActivity(intent);
                break;
        }
    }

    @Override
    public void setData(List<UserInfo> list, int page) {
        this.page = page;
        if (page == 1) {
            mRefreshLayout.finishRefresh();
            if (list == null || list.size() == 0) {
                recyclerLayout.changeState(RecyclerLayout.RecyclerhState.NoDate, Utils.getStringByValues(R.string.empty), R.mipmap.ic_fail);
            } else {
                recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Normal_list, null);
                this.list.clear();
                this.list.addAll(list);
                adapter.setList(this.list);
            }
        } else {
            recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Normal_list, null);
            this.list.addAll(list);
            adapter.addList(list);
        }
        if (list.size() < AppConstants.PAGE_COUNT) {
            mRefreshLayout.finishLoadMoreWithNoMoreData();
        } else {
            mRefreshLayout.finishLoadMore();
        }
    }

    private void setGetList() {
        recyclerLayout.changeState(RecyclerLayout.RecyclerhState.In_loading, Utils.getStringByValues(R.string.loading));
        refresh();
    }

    public void refresh() {
        closeRefreshState();
//        if (NetUtils.isNetWorkConnected(mActivity.get())) {
//            recyclerLayout.changeState(RecyclerLayout.RecyclerhState.NoDate, Utils.getStringByValues(R.string.empty), R.mipmap.ic_fail);
//        } else {
//            recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Failure_notRetry, Utils.getStringByValues(R.string.net_error));
//        }
        mPresenter.getUsers(1);
    }


    //收回下拉上拉状态
    private void closeRefreshState() {
        mRefreshLayout.finishRefresh(false);
        mRefreshLayout.finishLoadMore();
    }

    @Override
    public void setRetryVisible(String uiMsg, String toastMsg, boolean isCanClick) {
        closeRefreshState();
        if (recyclerLayout.getCurrentState() == RecyclerLayout.RecyclerhState.Normal_list) {
            ToastUtils.showToast(mActivity.get(), toastMsg);
        } else {
            recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Failure_notRetry, uiMsg);
        }
    }
}