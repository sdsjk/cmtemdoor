package com.hjimi.cmptemdoor.view;


import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.activity.PasswordActivity;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.contract.PasswordContract;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.utils.EditUtils;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.wight.SmartScrollView;

import java.lang.ref.SoftReference;


public class PasswordView implements PasswordContract.View, View.OnClickListener, View.OnFocusChangeListener {
    private SoftReference<PasswordActivity> mActivity;
    private PasswordContract.Presenter mPresenter;
    private EditText etOldPw, etNewPw, etNewPwAgain;
    private SmartScrollView mScrollView;
    private boolean isOpenSoft = false;//是否打开了键盘

    public static PasswordView newInstance(PasswordActivity activity) {
        return new PasswordView(activity);
    }

    private PasswordView(PasswordActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        mScrollView = (SmartScrollView) mActivity.get().findViewById(R.id.scroll_view);
        mActivity.get().findViewById(R.id.iv_back).setOnClickListener(this);
        mActivity.get().findViewById(R.id.iv_home).setOnClickListener(this);
        mActivity.get().findViewById(R.id.tv_sure).setOnClickListener(this);
        etOldPw = (EditText) mActivity.get().findViewById(R.id.et_old_pw);
        etNewPw = (EditText) mActivity.get().findViewById(R.id.et_new_pw);
        etNewPwAgain = (EditText) mActivity.get().findViewById(R.id.et_new_pw_again);

        etOldPw.setFocusable(true);
        etOldPw.setFocusableInTouchMode(true);
        etOldPw.requestFocus();

        etNewPw.setOnFocusChangeListener(this);
        etNewPw.setOnClickListener(this);
        etNewPwAgain.setOnFocusChangeListener(this);
        etNewPwAgain.setOnClickListener(this);

        final int mKeyHeight = Utils.getHeight() / 3; // sScreenHeight为屏幕高度
        mScrollView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom) > mKeyHeight) {
                    isOpenSoft = true;
                } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom) > mKeyHeight) {
                    isOpenSoft = false;
                }
            }
        });
    }


    @Override
    public void setPresenter(PasswordContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_home:
                EditUtils.hideInput(mActivity.get(), etNewPw);
                mActivity.get().startActivity(new Intent(mActivity.get(), MainActivity.class));
                mActivity.get().finish();
                break;
            case R.id.tv_sure:
                String old = etOldPw.getText().toString().trim();
                String newPw = etNewPw.getText().toString().trim();
                String newPwAgain = etNewPwAgain.getText().toString().trim();
                if (TextUtils.isEmpty(old) || TextUtils.isEmpty(newPw) || TextUtils.isEmpty(newPwAgain)) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.input_empty));
                    return;
                }
                if (!newPw.equals(newPwAgain)) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.input_different));
                    return;
                }
                if (!EditUtils.menuPasswordFilter(newPw)) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.password_style_error));
                    return;
                }

                if (!AppConstantInfo.getInstance().getMenuPwd().equals(old)) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.password_error));
                    return;
                }
                AppConstantInfo.getInstance().setMenuPwd(newPw);
                SpUtil.putString(PreferenceConstants.MENU_PASSWORD, newPw);
                ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.change_success));
                EditUtils.hideInput(mActivity.get(), etNewPw);
                mActivity.get().finish();
                break;
            case R.id.iv_back:
                EditUtils.hideInput(mActivity.get(), etNewPw);
                mActivity.get().finish();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (b) {
            switch (view.getId()) {
                case R.id.et_new_pw:
                case R.id.et_new_pw_again:
                    scrollBottom();
                    break;
            }
        }
    }

    private void scrollBottom() {
        int timeDelay = isOpenSoft ? 100 : 500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(ScrollView.FOCUS_DOWN, true);
            }
        }, timeDelay);
    }
}