package com.hjimi.cmptemdoor.view;


import android.content.Intent;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.FaceSettingActivity;
import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.contract.FaceSettingContract;
import com.hjimi.cmptemdoor.dialog.FaceSettingDialog;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.interfaces.OnFaceSettingListener;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;

import java.lang.ref.SoftReference;


public class FaceSettingView implements FaceSettingContract.View, View.OnClickListener {
    private SoftReference<FaceSettingActivity> mActivity;
    private FaceSettingContract.Presenter mPresenter;
    private TextView tvCompareThreshold, tvSilenceThreshold, tvLivingThreshold, tvMinFace;
    private Switch cbLiveness, cbVoiceSwitch;

    public static FaceSettingView newInstance(FaceSettingActivity activity) {
        return new FaceSettingView(activity);
    }

    private FaceSettingView(FaceSettingActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        mActivity.get().findViewById(R.id.iv_back).setOnClickListener(this);
        mActivity.get().findViewById(R.id.iv_home).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_compare_threshold).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_silence_threshold).setOnClickListener(this);
//        mActivity.get().findViewById(R.id.ll_living_threshold).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_min_face).setOnClickListener(this);
        tvCompareThreshold = (TextView) mActivity.get().findViewById(R.id.tv_compare_threshold);
        tvSilenceThreshold = (TextView) mActivity.get().findViewById(R.id.tv_silence_threshold);
        tvLivingThreshold = (TextView) mActivity.get().findViewById(R.id.tv_living_threshold);
        tvMinFace = (TextView) mActivity.get().findViewById(R.id.tv_min_face);
        cbLiveness = (Switch) mActivity.get().findViewById(R.id.checkbox_switch_liveness);
        cbVoiceSwitch = (Switch) mActivity.get().findViewById(R.id.checkbox_switch_voice);

        tvCompareThreshold.setText(String.valueOf(AppConstantInfo.getInstance().getCompareThreshold()));
        tvLivingThreshold.setText(String.valueOf(AppConstantInfo.getInstance().getLivnessThreshold()));
//        cbLiveness.setSelected(AppConstantInfo.getInstance().isNeedLiveness());
        cbVoiceSwitch.setSelected(AppConstantInfo.getInstance().isVoiceOpen());

        //2d无活体
//        cbLiveness.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (cbLiveness.isSelected()) {
//                    cbLiveness.setSelected(false);
//                } else {
//                    cbLiveness.setSelected(true);
//                }
//
//                AppConstantInfo.getInstance().setNeedLiveness(cbLiveness.isSelected());
//                SpUtil.putBoolean(PreferenceConstants.FACE_NEED_LIVENESS, cbLiveness.isSelected());
//                MqttUtils.devConfigUpload(false);
//            }
//        });

        cbVoiceSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbVoiceSwitch.isSelected()) {
                    cbVoiceSwitch.setSelected(false);
                } else {
                    cbVoiceSwitch.setSelected(true);
                }

                AppConstantInfo.getInstance().setVoiceOpen(cbVoiceSwitch.isSelected());
                SpUtil.putBoolean(PreferenceConstants.VOICE_OPEN, cbVoiceSwitch.isSelected());
                MqttUtils.devConfigUpload(false);
            }
        });
    }

    @Override
    public void setPresenter(FaceSettingContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_home:
                mActivity.get().startActivity(new Intent(mActivity.get(), MainActivity.class));
                mActivity.get().finish();
                break;
            case R.id.ll_compare_threshold:
                //人脸对比阈值
                showDialog(Utils.getStringByValues(R.string.face_setting_1), tvCompareThreshold);
                break;
            case R.id.ll_silence_threshold:
                //静默识别阈值
                showDialog(Utils.getStringByValues(R.string.face_setting_2), tvSilenceThreshold);
                break;
            case R.id.ll_living_threshold:
                //活体阈值
                showDialog(Utils.getStringByValues(R.string.face_setting_3), tvLivingThreshold);
                break;
            case R.id.ll_min_face:
                //最小面部识别
                showDialog(Utils.getStringByValues(R.string.face_setting_4), tvMinFace);
                break;
            case R.id.iv_back:
                mActivity.get().finish();
                break;
        }
    }

    private void showDialog(String title, final TextView value) {
        new FaceSettingDialog(mActivity.get(), title, value.getText().toString(), new OnFaceSettingListener() {
            @Override
            public void onCallback(String setting) {
                saveSp(value, Float.valueOf(setting));
                value.setText(setting);
            }
        }).show();
    }

    private void saveSp(TextView textView, float value) {
        switch (textView.getId()) {
            case R.id.tv_compare_threshold:
                //人脸对比
                AppConstantInfo.getInstance().setCompareThreshold(value);
                SpUtil.putFloat(PreferenceConstants.FACE_COMPARE_THRESHOLD, value);
                break;
            case R.id.tv_living_threshold:
                //活体阈值
                AppConstantInfo.getInstance().setLivnessThreshold(value);
                SpUtil.putFloat(PreferenceConstants.FACE_LIVENESS_THRESHOLD, value);
                break;
        }
        MqttUtils.devConfigUpload(false);
    }
}