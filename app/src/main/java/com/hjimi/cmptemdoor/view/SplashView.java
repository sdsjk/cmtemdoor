package com.hjimi.cmptemdoor.view;


import com.hjimi.cmptemdoor.activity.SplashActivity;
import com.hjimi.cmptemdoor.contract.SplashContract;

import java.lang.ref.SoftReference;

public class SplashView implements SplashContract.View {

    private SoftReference<SplashActivity> mActivity;

    public SplashView(SplashActivity activity) {
        mActivity = new SoftReference<>(activity);
    }

    @Override
    public void setPresenter(SplashContract.Presenter presenter) {

    }
}