package com.hjimi.cmptemdoor.view;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.view.View;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.FaceSettingActivity;
import com.hjimi.cmptemdoor.activity.MenuActivity;
import com.hjimi.cmptemdoor.activity.PasswordActivity;
import com.hjimi.cmptemdoor.activity.RecordActivity;
import com.hjimi.cmptemdoor.activity.TemSettingActivity;
import com.hjimi.cmptemdoor.activity.UserListActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.contract.MenuContract;
import com.hjimi.cmptemdoor.dialog.ReStartDialog;
import com.hjimi.cmptemdoor.dialog.ResetDialog;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.utils.FileUtils;
import com.hjimi.cmptemdoor.utils.PackageUtils;
import com.zed.zedlib.SystemHelper;

import java.lang.ref.SoftReference;


public class MenuView implements MenuContract.View, View.OnClickListener {
    private SoftReference<MenuActivity> mActivity;
    private MenuContract.Presenter mPresenter;

    public static MenuView newInstance(MenuActivity activity) {
        return new MenuView(activity);
    }

    private MenuView(MenuActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        mActivity.get().findViewById(R.id.iv_home).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_tem).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_wifi).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_record).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_users).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_setting).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_code).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_reset).setOnClickListener(this);
        mActivity.get().findViewById(R.id.ll_logout).setOnClickListener(this);
        TextView textVersion = (TextView) mActivity.get().findViewById(R.id.current_version);
        textVersion.setText(FileUtils.setSubstitution(R.string.current_version, PackageUtils.getVersionName(mActivity.get())));
    }

    @Override
    public void setPresenter(MenuContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_home:
                mActivity.get().finish();
                break;
            case R.id.ll_tem:
                //温度设置
                mActivity.get().startActivity(new Intent(mActivity.get(), TemSettingActivity.class));
                break;
            case R.id.ll_record:
                //通行记录
                mActivity.get().startActivity(new Intent(mActivity.get(), RecordActivity.class));
                break;
            case R.id.ll_users:
                //用户列表
                mActivity.get().startActivity(new Intent(mActivity.get(), UserListActivity.class));
                break;
            case R.id.ll_setting:
                //刷脸设置
                mActivity.get().startActivity(new Intent(mActivity.get(), FaceSettingActivity.class));
                break;
            case R.id.ll_code:
                //密码管理
                mActivity.get().startActivity(new Intent(mActivity.get(), PasswordActivity.class));
                break;
            case R.id.ll_wifi:
                //wifi
                Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
                intent.putExtra("only_access_points", true);
                intent.putExtra("extra_prefs_show_button_bar", true);
                intent.putExtra("extra_prefs_set_back_text", "返回");
                intent.putExtra("wifi_enable_next_on_connect", true);
                intent.putExtra(":settings:show_fragment_as_subsetting", true);
                intent.putExtra("extra_prefs_set_next_text", "完成");
                mActivity.get().startActivityForResult(intent, AppConstants.WIFI_REQUEST_CODE);
                break;
            case R.id.ll_reset:
                //一键重启
                new ResetDialog(mActivity.get(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SystemHelper.ZedRbootSys(ImiNrApplication.getApplication());
                    }
                }).show();
                break;
            case R.id.ll_logout:
                //退出登录
                new ReStartDialog(mActivity.get(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.toLogin();
                        mActivity.get().finish();
                    }
                }).show();

                break;
        }
    }
}