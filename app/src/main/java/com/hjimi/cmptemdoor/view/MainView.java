package com.hjimi.cmptemdoor.view;


import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.activity.MenuActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.base.constants.PreferenceConstants;
import com.hjimi.cmptemdoor.bean.Face;
import com.hjimi.cmptemdoor.bean.FaceUpdataInfo;
import com.hjimi.cmptemdoor.contract.MainContract;
import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.dialog.InitErrorDialog;
import com.hjimi.cmptemdoor.dialog.MenuPasswordDialog;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.engine.ImiNrApplication;
import com.hjimi.cmptemdoor.engine.threadpool.DefaultThreadPool;
import com.hjimi.cmptemdoor.gl.RgbGLSurface;
import com.hjimi.cmptemdoor.helper.CameraHelper;
import com.hjimi.cmptemdoor.helper.FileHelper;
import com.hjimi.cmptemdoor.helper.LogHelper;
import com.hjimi.cmptemdoor.interfaces.JudgeNetCallBack;
import com.hjimi.cmptemdoor.interfaces.OnFaceSettingListener;
import com.hjimi.cmptemdoor.interfaces.SuccessCallBack;
import com.hjimi.cmptemdoor.utils.AppConstantInfoUtils;
import com.hjimi.cmptemdoor.utils.JudgeNetUtils;
import com.hjimi.cmptemdoor.utils.SpUtil;
import com.hjimi.cmptemdoor.utils.SpeechUtils;
import com.hjimi.cmptemdoor.utils.StringUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.dbutils.RecordInfoDBUtil;
import com.hjimi.cmptemdoor.utils.dbutils.UserInfoDBUtil;
import com.hjimi.cmptemdoor.utils.download.DownLoadUtils;
import com.hjimi.cmptemdoor.utils.mqtt.FdfsUtils;
import com.hjimi.cmptemdoor.utils.utils.DataWrapper;
import com.imi.sdk.camera.ImageData;
import com.imi.sdk.face.Frame;
import com.imi.sdk.face.OnSessionInitializeListener;
import com.imi.sdk.face.Session;
import com.imi.sdk.face.SessionConfig;
import com.imi.sdk.facebase.utils.ResultCode;
import com.imi.sdk.utils.ImageFormat;
import com.imi.sdk.utils.ImageUtil;
import com.imi.sdk.utils.Rect;

import java.io.File;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import me.jessyan.autosize.utils.ScreenUtils;

public class MainView implements MainContract.View, SurfaceHolder.Callback, Camera.PreviewCallback, OnSessionInitializeListener {

    // 算法暂只支持640 * 480 分辨率
    private static final int IMAGE_WIDTH = 640;
    private static final int IMAGE_HEIGHT = 480;

    private static final String TAG = "MainView";
    private MainContract.Presenter mPresenter;
    private final static int GET_FACE_CODE = 999;
    private final static int FAIL_CODE = 404;
    private final static int SUCCESS_CODE = 100;
    private final static int CONTRAST_SUCCESS_CODE = 200;
    private final static int STANDBY_CODE = 88;
    private final static int RESULT_WAIT_CODE = 888;
    private final static int RESULT_PAY_COMPLETE = 500;
    private SoftReference<MainActivity> mActivity;
    private boolean mIsIdentify = false;
    private ImageView ivLine;

    private ImageView mFinalIv;
    private RelativeLayout relativeLayout;
    private SurfaceView mSurfaceView;
    private RgbGLSurface mGLSurfaceView;
    private float left, top, right, bottom;
    private GetFaceHandler handler = new GetFaceHandler(this);
    private int count;
    private LinearLayout llShoot;
    private float screenLeft, screenRight;
    private TextView tvName, tvTem,tvHint;
    private RelativeLayout rlSynchronization;
    private RelativeLayout rlResult,rlHint;
    private ObjectAnimator scale;

    private boolean isMakeDataFinish = false;//人脸库是否准备完成
    private boolean isDataMaking = false;//是否正在准备人脸库

    // 相机线程是否应当运行的标志位
    private volatile boolean shouldCameraThreadRun = false;
    // 人脸算法线程是否应当运行标志位
    private volatile boolean shouldFaceAlgThreadRun = false;
    // 相机模组锁
    private Semaphore cameraLock = new Semaphore(1);
    // 人脸算法锁
    private Semaphore faceLock = new Semaphore(1);

    // 相机图像重用对象池，用于复用对象
    private ConcurrentLinkedQueue<DataWrapper> imageReusePool = new ConcurrentLinkedQueue<>();
    // 相机图像队列，用于线程之间调度图像
    private final LinkedList<DataWrapper> imageQueue = new LinkedList<>();

    private Session mSession;
    private CameraHelper mCameraHelper;
    private int mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    private LinearLayout llWelcome;
    private boolean isCurrent = true;
    private TextView tvLoading;

    private Runnable cameraRunnable;
    private Runnable arithmeticRunnable;

    private TextView tvActionInfo, tvNotShutDown;

    //    private boolean isLive = true;//套餐模式下，成功了，等离开摄像头区域后，再进行下一次识别，防止误刷
    private MenuPasswordDialog menuPasswordDialog;
    private View topView, bottomView, leftView, rightView;
    private TextView tvWelcomeTime;


    public void synchronousProgress(String progress) {
        tvLoading.setText(Utils.getStringByValues(R.string.synchronization_21) + progress + Utils.getStringByValues(R.string.synchronization_22));
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.e(TAG, "surfaceCreated: ");
        if (mCameraHelper != null) {
            try {
                mCameraHelper.openCamera(mCameraId);
                mCameraHelper.setPreviewCallback(this);
                mCameraHelper.startPreview(mSurfaceView.getHolder());

                if (!isFaceStart) {
                    //相机打开成功，初始化算法
                    initSession();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.e(TAG, "surfaceDestroyed: ");
        if (mCameraHelper != null) {
            mCameraHelper.stopPreview();
            mCameraHelper.closeCamera();
        }
    }

    private ConcurrentLinkedQueue<ByteBuffer> mLinkedQueue = new ConcurrentLinkedQueue<>();
    private ArrayBlockingQueue<ByteBuffer> mBlockingQueue = new ArrayBlockingQueue<>(16);
    private ByteBuffer mYuvBuffer;
    private ByteBuffer mBgrSrcBuffer;
    private int width = 640;
    private int height = 480;

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
//        LogHelper.d("数据：" + data.length);
        camera.addCallbackBuffer(data);

        //填充ByteBuffer
//        LogHelper.w(data.length + "");
        mYuvBuffer.put(data).position(0);

        //判断
        ImageUtil.yuv2bgr(mBgrSrcBuffer, width, height, ImageFormat.IMGFMT_YUV_NV21, mYuvBuffer);

        //mLinkQueue用来复用ByteBuffer，防止一直创建
        if (mLinkedQueue.isEmpty()) {
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(width * height * 3).order(ByteOrder.nativeOrder());
            byteBuffer.position(0);
            mLinkedQueue.add(byteBuffer);
        }

        ByteBuffer mDataBuffer = mLinkedQueue.poll();
        //由于相机取出的数据是横向的，需要将数据进行90°(前置摄像头)，或者270度（后置摄像头）的旋转
        int direct = mCameraHelper.getCameraId() == Camera.CameraInfo.CAMERA_FACING_BACK ? 3 : 1;
        ImageUtil.rotateImage(mDataBuffer, width, height, ImageFormat.IMGFMT_BGR, mBgrSrcBuffer, direct);
        mGLSurfaceView.updateColorImage(mDataBuffer, height, width);
        mGLSurfaceView.requestRender();
        try {
            //用一个阻塞队列传递数据。只传递最新的一帧数据
            mBlockingQueue.clear();
            mBlockingQueue.put(mDataBuffer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static class GetFaceHandler extends Handler {
        private MainView view;

        public GetFaceHandler(MainView view) {
            this.view = view;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case GET_FACE_CODE:
                    //获取人脸消息
//                    Log.e(TAG, "--------------------------------接收到人脸消息");
                    view.dealFace((Face) msg.obj);
                    break;
                case FAIL_CODE:
                    //获取摄像头失败
                    view.exit((String) msg.obj);
                    break;
                case SUCCESS_CODE:
                    //获取摄像头成功
                    view.measure();
                    break;
                case RESULT_WAIT_CODE:
                    //结果回来后 三秒后继续分析人脸
                    view.changeBackground(true, "");
                    view.mFinalIv.setImageBitmap(null);
                    view.tvName.setText("");
                    view.tvTem.setText("");
                    view.mTemData = 0;
                    view.mBase64 = null;
                    view.mUserInfo = null;
                    view.isFaceComplete = false;
                    view.isTemComplete = false;
                    view.rlResult.setVisibility(View.INVISIBLE);
                    view.rlHint.setVisibility(View.VISIBLE);
                    view.tvHint.setText(Utils.getStringByValues(R.string.face_hint_1));
                    view.setmIsIdentify(false);
                    Log.e(TAG, "--------------------------------结果回来后 2秒后继续分析人脸");
                    break;
                case CONTRAST_SUCCESS_CODE:
                    UserInfo userInfo = (UserInfo) msg.obj;
                    //TODO:开门
//                    if (StringUtils.isEmpty(userInfo.getPhone())) {
//                        Log.e(TAG, "--------------------------------手机号为空");
//                        view.setPayResult("-1", Utils.getStringByValues(R.string.pay_fail_phone_empty), Utils.getStringByValues(R.string.pay_fail));
//                    } else {
//                        Log.e(TAG, "--------------------------------调用支付接口");
                    view.mPresenter.openDoor(view.mTemData);
//                    }
                    break;
                case STANDBY_CODE:
                    //5秒内始终没人脸 就待机
                    view.tvWelcomeTime.setText(view.getDate());
                    view.llWelcome.setVisibility(View.VISIBLE);
                    break;
            }
        }

    }

    public static MainView newInstance(MainActivity activity) {
        return new MainView(activity);
    }

    private MainView(MainActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();
    }

    private void initView() {
        mSurfaceView = (SurfaceView) mActivity.get().findViewById(R.id.sv_color_view);
        mGLSurfaceView = (RgbGLSurface) mActivity.get().findViewById(R.id.gl_rgb);
        ivLine = (ImageView) mActivity.get().findViewById(R.id.iv_line);
        relativeLayout = (RelativeLayout) mActivity.get().findViewById(R.id.rl_shoot);
        tvName = (TextView) mActivity.get().findViewById(R.id.tv_name);
        tvTem = (TextView) mActivity.get().findViewById(R.id.tv_tem);
        llWelcome = (LinearLayout) mActivity.get().findViewById(R.id.ll_welcome);
        tvWelcomeTime = (TextView) mActivity.get().findViewById(R.id.tv_welcome_time);
        tvWelcomeTime.setText(getDate());

        mFinalIv = (ImageView) mActivity.get().findViewById(R.id.final_iv);
        rlSynchronization = (RelativeLayout) mActivity.get().findViewById(R.id.rl_synchronization);
        rlResult = (RelativeLayout) mActivity.get().findViewById(R.id.rl_result);
        rlHint = (RelativeLayout) mActivity.get().findViewById(R.id.rl_hint);
        llShoot = (LinearLayout) mActivity.get().findViewById(R.id.ll_shoot);
        tvLoading = (TextView) mActivity.get().findViewById(R.id.tv_loading);
        tvActionInfo = (TextView) mActivity.get().findViewById(R.id.tv_action_info);
        tvNotShutDown = (TextView) mActivity.get().findViewById(R.id.tv_time_not_shut_down);
        tvHint= (TextView) mActivity.get().findViewById(R.id.tv_hint);
        topView = mActivity.get().findViewById(R.id.top);
        bottomView = mActivity.get().findViewById(R.id.bottom);
        leftView = mActivity.get().findViewById(R.id.left);
        rightView = mActivity.get().findViewById(R.id.right);

        mYuvBuffer = ByteBuffer.allocateDirect(width * height * 3 / 2);
        mYuvBuffer.order(ByteOrder.nativeOrder());

        mBgrSrcBuffer = ByteBuffer.allocateDirect(width * height * 3);
        mBgrSrcBuffer.order(ByteOrder.nativeOrder());

        openCamera();

        Message message = Message.obtain();
        message.what = SUCCESS_CODE;
        if (handler.hasMessages(SUCCESS_CODE)) {
            handler.removeMessages(SUCCESS_CODE);
        }

        handler.sendMessageDelayed(message, 1000);
        mActivity.get().findViewById(R.id.iv_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickMenu();
            }
        });
        startScale();
//        changePayMode(SpUtil.getString(PreferenceConstants.PAY_MODE));
    }


    //点击设置
    private void clickMenu() {
        if (menuPasswordDialog != null && menuPasswordDialog.isShowing()) {
            return;
        }
        menuPasswordDialog = null;
        menuPasswordDialog = new MenuPasswordDialog(mActivity.get(), new OnFaceSettingListener() {
            @Override
            public void onCallback(String setting) {
                mActivity.get().startActivity(new Intent(mActivity.get(), MenuActivity.class));
            }
        });
        menuPasswordDialog.show();
    }


    private void startScale() {
        scale = ObjectAnimator.ofFloat(ivLine, "scaleX", 1f, 0f, 1f);
        scale.setDuration(1500);
        scale.setRepeatCount(ValueAnimator.INFINITE);
        scale.start();

    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.mPresenter = presenter;
//        mPresenter.diningMerc();
    }

    private UserInfo mUserInfo;
    private String mBase64;
    private float mTemData;
    private boolean isTemComplete, isFaceComplete;

    //更新匹配结果界面
    @Override
    public void setMatchFeaceData(UserInfo data, String base64) {
        Log.e(TAG, "setMatchFeaceData: ");
        mUserInfo = data;
        mBase64 = base64;
        isFaceComplete = true;
        if (AppConstantInfo.getInstance().isTem()) {
            if (isTemComplete) {
                dealResult();
            }
        } else {
            dealResult();
        }

    }

    @Override
    public void setTemData(float temData) {
        Log.e(TAG, "setTemData: ");
        mTemData = temData;
        isTemComplete = true;
        if (isFaceComplete) {
            dealResult();
        }
    }


    public void initSession() {
        Log.e(TAG, "initSession: ");
        mSession = new Session(mActivity.get());
        SessionConfig sessionConfig = new SessionConfig();
        //模型路径
        sessionConfig.modelPath = FileHelper.getInstance().getFaceModelFolderPath();
        //设备ID
//        sessionConfig.deviceSn = "4FC013E989632FEC485C805CD17BEA1C";//Mate20 Pro
//        sessionConfig.deviceSn = "14053D63724CD4625C4DC703DEF20631";//SumSang S10
//        sessionConfig.deviceSn = "9426F412C8CD708375DA1235D83C6314";//1+
//        sessionConfig.deviceSn = "79FD3EF4E55DEBA4173FA24CB88CF3E5";//华为
//        sessionConfig.deviceSn = "64F51950F3D7BE93D4CAC9E3BE39CB1E";//三星
        String deviceSn = AppConstantInfo.getInstance().getDevSN();
        Log.e(TAG, "deviceSn: " + deviceSn);
        sessionConfig.deviceSn = deviceSn;
        mSession.configure(sessionConfig);
        //初始化算法
        mSession.initializeAsync(this);
    }

    @Override
    public void onSessionInitialized(int i, String s) {
        Log.e(TAG, "onSessionInitialized: ");
        if (i == ResultCode.OK) {
            isFaceStart = true;
            //运行算法
            startFaceAlgo();
        } else {
            exit(s);
        }
        LogHelper.e(i + " " + s);
    }

    private void dealResult() {
        Log.e(TAG, "--------------------------------更新识别结果页面：   " + System.currentTimeMillis());
        FaceRecordInfo info = new FaceRecordInfo();
        if (mUserInfo != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(ImiNrApplication.getApplication().getFilesDir().getAbsolutePath() + File.separator + mUserInfo.getLocalName());
            rlResult.setBackgroundResource(R.mipmap.img_success);
            mFinalIv.setVisibility(View.VISIBLE);
            mFinalIv.setImageBitmap(bitmap);


            info.setFaceId(Integer.parseInt(mUserInfo.getUserId()));
            info.setFaceCommitId(-1L);
            info.setFaceImg(mBase64);
            info.setName(mUserInfo.getUserName());
            info.setFaceSetVer((int) AppConstantInfo.getInstance().getFaceSetVer());
            info.setFaceSetId((int) AppConstantInfo.getInstance().getFaceSetId());
            info.setSimilarity(mPresenter.getSimilarity());
            info.setTimestamp(System.currentTimeMillis());
            info.setIsIdentified(false);
            info.setTem(mTemData);

            if (AppConstantInfo.getInstance().isTem() && mTemData > AppConstantInfo.getInstance().getTemThreshold()) {
                changeBackground(false, mUserInfo.getUserName());
                SpeechUtils.play(mActivity.get(), "体温偏高");
            } else {
                changeBackground(true, mUserInfo.getUserName());
                Message message = Message.obtain();
                message.what = CONTRAST_SUCCESS_CODE;
                message.obj = mUserInfo;
                if (handler.hasMessages(CONTRAST_SUCCESS_CODE)) {
                    handler.removeMessages(CONTRAST_SUCCESS_CODE);
                }
                handler.sendMessageDelayed(message, 2000);
            }
        } else {
            //  验证失败，请注册您的人脸信息
            changeBackground(false, Utils.getStringByValues(R.string.face_result_2));
            if (AppConstantInfo.getInstance().isTem() && mTemData > AppConstantInfo.getInstance().getTemThreshold()) {
                SpeechUtils.play(mActivity.get(), "检测失败,体温偏高");
            } else {
                SpeechUtils.play(mActivity.get(), "检测失败");
            }
            info.setFaceId(0);
            info.setFaceCommitId(-1L);
            info.setFaceImg(mBase64);
            info.setName("");
            info.setFaceSetVer((int) AppConstantInfo.getInstance().getFaceSetVer());
            info.setFaceSetId((int) AppConstantInfo.getInstance().getFaceSetId());
            info.setSimilarity(0f);
            info.setTimestamp(System.currentTimeMillis());
            info.setIsIdentified(false);
            info.setTem(mTemData);
        }
        Log.e(TAG, "--------------------------------更新识别结果结束并上报刷脸记录：   " + System.currentTimeMillis());
        //上报刷脸记录
        mPresenter.upFaceRecord(info);
    }


    private void measure() {
        float surfaceViewWidth = mGLSurfaceView.getHeight() * (480.0f / 640.0f);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mGLSurfaceView.getLayoutParams();
        layoutParams.width = (int) surfaceViewWidth;
        mGLSurfaceView.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) mSurfaceView.getLayoutParams();
        layoutParams1.width = (int) surfaceViewWidth;
        mSurfaceView.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams llShootLayoutParams = (LinearLayout.LayoutParams) llShoot.getLayoutParams();
        llShootLayoutParams.width = (int) surfaceViewWidth;
        llShoot.setLayoutParams(llShootLayoutParams);

        float proportion1 = (float) mGLSurfaceView.getWidth() / 480.0f;
        float proportion2 = (float) mGLSurfaceView.getHeight() / 640.0f;

        left = (relativeLayout.getLeft()) / proportion1;
        top = (relativeLayout.getTop() + llShoot.getTop()) / proportion2;
        right = (relativeLayout.getRight()) / proportion1;
        bottom = (relativeLayout.getBottom() + llShoot.getTop()) / proportion2;

        screenLeft = ((mGLSurfaceView.getWidth() - ScreenUtils.getScreenSize(mActivity.get())[0]) / 2) / proportion1;
        screenRight = (ScreenUtils.getScreenSize(mActivity.get())[0]) / proportion1 + screenLeft;

        ViewGroup.LayoutParams inp = llWelcome.getLayoutParams();
        inp.width = Utils.getWidth();
        llWelcome.setLayoutParams(inp);
    }

    private void dealFace(Face face) {
        if (face.getLiveness() != -4f) {
            handler.removeMessages(STANDBY_CODE);
            llWelcome.setVisibility(View.GONE);
        } else {
            //没有人脸
            if (!handler.hasMessages(STANDBY_CODE)) {
                handler.sendEmptyMessageDelayed(STANDBY_CODE, 5000);
            }
        }

        //判断生成特征值是否完成
        if (!isMakeDataFinish) {
            return;
        }

        //判断是否在识别中
        if (mIsIdentify) {
            return;
        }

        //判断是否在当前页面
        if (!isCurrent)
            return;

        //判断是否同步页面显示
        if (rlSynchronization.getVisibility() == View.VISIBLE) {
            return;
        }
        rlHint.setVisibility(View.VISIBLE);
        tvHint.setText(face.getResult());

        if (Utils.getStringByValues(R.string.face_result_4).equals(face.getResult())) {
            if (AppConstantInfo.getInstance().isTem()) {
                mPresenter.checkTem();
            }
            Log.e(TAG, "-------------------------- 获取人脸特征值 开始");
            setmIsIdentify(true);
            takePhoto(face);
        } else if (Utils.getStringByValues(R.string.face_hint_5).equals(face.getResult())) {
            //非活体
            if (AppConstantInfo.getInstance().isTem()) {
                mPresenter.checkTem();
            }
            Log.e(TAG, "-------------------------- 非活体");
            setmIsIdentify(true);
            changeBackground(false, Utils.getStringByValues(R.string.face_result_2));

            //TODO:上报刷脸记录
            FaceRecordInfo info = new FaceRecordInfo();
            info.setFaceId(0);
            info.setFaceCommitId(-1L);
            info.setFaceImg(face.getShoot());
            info.setName("");
            info.setFaceSetVer((int) AppConstantInfo.getInstance().getFaceSetVer());
            info.setFaceSetId((int) AppConstantInfo.getInstance().getFaceSetId());
            info.setSimilarity(0f);
            info.setTimestamp(System.currentTimeMillis());
            info.setIsIdentified(false);
            mPresenter.upFaceRecord(info);
        } else {
//            Log.e(TAG, "-------------------------- 无人脸");
        }
    }


    private void changeBackground(boolean isSuccess, String result) {
        rlHint.setVisibility(View.INVISIBLE);
        Log.e(TAG, "changeBackground前: " + System.currentTimeMillis());
        rlResult.setVisibility(View.VISIBLE);
        tvName.setText(result);
        if (AppConstantInfo.getInstance().isTem() && mTemData != 0) {
            tvTem.setVisibility(View.VISIBLE);
            tvTem.setText("体温:" + mTemData + "℃");
            tvTem.setTextColor(mActivity.get().getResources().getColor(mTemData <= AppConstantInfo.getInstance().getTemThreshold() ? R.color.color_00F6FF : R.color.color_E4007F));
        } else {
            tvTem.setVisibility(View.GONE);
        }
        int color = mActivity.get().getResources().getColor(isSuccess ? R.color.color_330051AA : R.color.color_1AE4007F);
        topView.setBackgroundColor(color);
        bottomView.setBackgroundColor(color);
        leftView.setBackgroundColor(color);
        rightView.setBackgroundColor(color);
        tvName.setTextColor(mActivity.get().getResources().getColor(isSuccess ? R.color.color_00F6FF : R.color.color_E4007F));
        int lineColor = mActivity.get().getResources().getColor(isSuccess ? R.color.color_ff00f8ff : R.color.color_E4007F);
        int view_id;
        for (int i = 0; i < 8; i++) {
            view_id = mActivity.get().getResources().getIdentifier("view_" + (i + 1), "id", AppConstants.PACKAGE_NAME);
            mActivity.get().findViewById(view_id).setBackgroundColor(lineColor);
        }

        if (!isSuccess) {
            rlResult.setBackgroundResource(R.mipmap.img_error);
            mFinalIv.setVisibility(View.GONE);
            Message obtain = Message.obtain();
            obtain.what = RESULT_WAIT_CODE;
            if (handler.hasMessages(RESULT_WAIT_CODE)) {
                handler.removeMessages(RESULT_WAIT_CODE);
            }
            handler.sendMessageDelayed(obtain, 2000);
        }
        Log.e(TAG, "changeBackground后: " + System.currentTimeMillis());
    }

    @Override
    public void makeDataFinish() {
        isMakeDataFinish = true;
        mActivity.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rlSynchronization.setVisibility(View.GONE);
                scale.end();
                tvLoading.setText(Utils.getStringByValues(R.string.synchronization_2));

                if (mActivity.get().getIntent() != null) {
                    String type = mActivity.get().getIntent().getStringExtra(AppConstants.AGAIN_TO_MAINACTIVITY_TYPE);
                    if (!StringUtils.isEmpty(type) && AppConstants.EXCEPTION_TO_RESTART.equals(type)) {
                        //崩溃进来的
                        String mode = SpUtil.getString(PreferenceConstants.PAY_MODE);
                        if (!StringUtils.isEmpty(mode) && AppConstants.MODE_SET_MEAL.equals(mode)) {
                            String price = SpUtil.getString(PreferenceConstants.MEAL_AMOUNT);
                            if (!StringUtils.isEmpty(price)) {
                                llWelcome.setVisibility(View.GONE);
                                Runtime.getRuntime().gc();
                                return;
                            }
                        }

                    }
                }

                Runtime.getRuntime().gc();
            }
        });
    }

    /**
     * 拍照
     */
    private void takePhoto(final Face face) {
        mActivity.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //将手动裁剪区域设置成与扫描框一样大
                if (mPresenter != null) {
                    //进行人脸库比对
                    mPresenter.getMatchInfo(face);

                }
            }
        });

    }

    private boolean isFaceDataOk = false;

    public void setFaceDataOk() {
        if (isFaceDataOk) {
            return;
        }
        isFaceDataOk = true;
        onResume();

    }

    private boolean isDownloacOk = false;

    public void onResume() {
        JudgeNetUtils.getInstance().judgeNetConnect(new JudgeNetCallBack() {
            @Override
            public void judgeNet(boolean isNetConnect) {
                isCurrent = true;
                if (mCameraHelper == null) {
                    //如果第一次没有网，就相当于人脸库准备完成
                    if (!isFaceDataOk) {
                        if (!isNetConnect) {
                            isFaceDataOk = true;
                        }
                    }
                    //等待人脸库准备完成后再进行生成特征值
                    if (isFaceDataOk && !isDownloacOk) {
                        isDownloacOk = true;
                    }
                } else {
                    if (!isFaceDataOk) {
                        if (!isNetConnect) {
                            isFaceDataOk = true;
                        }
                    }
                    //等待人脸库准备完成后再进行生成特征值
                    if (isFaceDataOk && !isDownloacOk) {
                        isDownloacOk = true;
                    }
                }
            }
        });
        isFacePause = false;
        if (mGLSurfaceView != null) {
            mGLSurfaceView.onResume();
        }
    }

    public void onPause() {
        isCurrent = false;
        isFacePause = true;
        if (mGLSurfaceView != null) {
            mGLSurfaceView.onPause();
        }
    }

    public void onDestroy() {
        shouldFaceAlgThreadRun = false;
        shouldCameraThreadRun = false;

        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }

        if (cameraRunnable != null) {
            DefaultThreadPool.getMaxPool().cancel(cameraRunnable);
        }
        if (arithmeticRunnable != null) {
            DefaultThreadPool.getMaxPool().cancel(arithmeticRunnable);
        }
        if (mFaceThread != null) {
            try {
                isFaceStart = false;
                //等待人脸线程结束
                mFaceThread.join(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (mSession != null) {
            mSession.release();
        }

        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void openCamera() {
        Log.e(TAG, "openCamera: ");
        mSurfaceView.getHolder().addCallback(this);
        mCameraHelper = new CameraHelper();
    }

    private Thread mFaceThread;
    private volatile long lastFaceTime = 0;
    private volatile boolean isFaceStart = false;
    private volatile boolean isFacePause = false;
    private Face face = null;

    //人脸算法
    public void startFaceAlgo() {
        if (mFaceThread == null) {
            mFaceThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (isFaceStart) {
                        try {
                            if (isFacePause || mCameraHelper == null) {
                                Thread.sleep(3);
                                continue;
                            }
                            //防止该线程一直竞争CPU时间片
                            if (lastFaceTime == 0) {
                                lastFaceTime = System.currentTimeMillis();
                            } else {
                                long time = System.currentTimeMillis() - lastFaceTime;
                                if (33 - time >= 0) {
                                    Thread.sleep(33 - time);
                                }
                                lastFaceTime = System.currentTimeMillis();
                            }


                            if (!isMakeDataFinish && !isDataMaking && isDownloacOk) {
                                isDataMaking = true;
                                isDownloacOk = false;
                                Log.e(TAG, "run: makeFeature");
                                mPresenter.makeFeature();
                                continue;
                            }

                            if (mSession != null) {
                                if (!isCurrent) {
                                    try {
                                        Thread.sleep(10);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    continue;
                                }
                                if (mIsIdentify) {
                                    try {
                                        Thread.sleep(10);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    continue;
                                }
                                if (rlSynchronization.getVisibility() == View.VISIBLE) {
                                    try {
                                        Thread.sleep(10);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    continue;
                                }

                                //获取相机数据 ByteBuffer
                                ByteBuffer dataBuffer = mBlockingQueue.take();
                                dataBuffer.position(0);

                                ImageData imageData = new ImageData(dataBuffer, height, width);
                                //传递数据
                                Frame cameraFrame = mSession.update(imageData);
                                String result = Utils.getStringByValues(R.string.face_hint_1);
                                face = new Face(null, -4f);
                                face.setResult(result);

                                if (cameraFrame == null) {
                                    LogHelper.e("Session未初始化");
                                    //回收byteBuffer
                                    mLinkedQueue.add(dataBuffer);
                                    getFace(face);
                                    continue;
                                }

                                //人脸识别
                                Rect cameraRect = cameraFrame.detectFace();
//                                mGLSurfaceView.updateFaceRect(cameraRect);
                                if (cameraRect == null) {
//                                    LogHelper.e("未检测到人脸");
                                    //回收byteBuffer
                                    mLinkedQueue.add(dataBuffer);
                                    getFace(face);
                                    continue;
                                }
                                Log.e(TAG, "run: " + Arrays.toString(cameraRect.getValueArray()));
                                if (cameraRect.getValueArray()[0] > left - 70 && cameraRect.getValueArray()[1] > top
                                        && cameraRect.getValueArray()[0] + cameraRect.getValueArray()[2] < right + 70 && cameraRect.getValueArray()[1] + cameraRect.getValueArray()[3] < bottom) {
                                    //相机实时流的人脸特征值
                                    float[] cameraFeature = cameraFrame.getFaceFeature(cameraRect);
                                    face.setFeaceFeature(cameraFeature);
                                    face.setLiveness(1);
                                    result = Utils.getStringByValues(R.string.face_result_4);
                                } else {
                                    face.setLiveness(-1);
                                    result = Utils.getStringByValues(R.string.face_hint_1);
                                }

                                face.setResult(result);
                                getFace(face);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, "FaceDemo");

            mFaceThread.start();
        }
    }

    private void getFace(Face face) {
        Message message = Message.obtain();
        message.what = GET_FACE_CODE;
        message.obj = face;
        if (handler.hasMessages(GET_FACE_CODE)) {
            handler.removeMessages(GET_FACE_CODE);
        }
        handler.sendMessage(message);

        //如果是正在识别，则延迟一会儿，否则会出现识别过程中还在检测，可能会影响后续检测、识别速率
        if (Utils.getStringByValues(R.string.face_result_4).equals(face.getResult())) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Session getFaceSession() {
        return mSession;
    }

    @Override
    public void setResult(String code, String speechMsg, String showMsg) {
        Log.e(TAG, "------------------------更新开门结果页面  " + System.currentTimeMillis());
        SpeechUtils.play(mActivity.get(), speechMsg);

        Log.e(TAG, "------------------------更新开门结果页面 完成 " + System.currentTimeMillis());
        Message obtain = Message.obtain();
        obtain.what = RESULT_WAIT_CODE;
        if (handler.hasMessages(RESULT_WAIT_CODE)) {
            handler.removeMessages(RESULT_WAIT_CODE);
        }
        handler.sendMessageDelayed(obtain, 2000);
    }

    private void exit(final String s) {
        mActivity.get().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                InitErrorDialog dialog = new InitErrorDialog();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.DIALOG_TITLE, s);
                dialog.setArguments(bundle);
                dialog.setCallBack(new SuccessCallBack() {
                    @Override
                    public void setSuccessCallBack() {
                        ImiNrApplication.getApplication().exitApp();
                    }
                });
                dialog.show(mActivity.get().getSupportFragmentManager(), "InitErrorDialog");
            }
        });
    }

    public boolean ismIsIdentify() {
        return mIsIdentify;
    }

    private void setmIsIdentify(boolean isIdentify) {
        mIsIdentify = isIdentify;
        if (!mIsIdentify) {

            if (!AppConstantInfo.getInstance().isActive()) {
                if (rlSynchronization.getVisibility() == View.GONE) {
                    rlSynchronization.setVisibility(View.VISIBLE);
                    scale.start();
                }
            } else {
                //检测、支付结束后，看下是否有新库，有的话，需要进行同步
                if (mActivity.get().isInUpdateFace()) {
                    //人脸库有更新
                    mActivity.get().setInUpdateFace(false);

                    Intent intent = new Intent(mActivity.get(), MainActivity.class);
                    intent.putExtra(AppConstants.AGAIN_TO_MAINACTIVITY_TYPE, AppConstants.AGAIN_MAINACTIVITY_TYPE_CSV);
                    mActivity.get().startActivity(intent);
                }
            }
        }
    }


    public void handleInUpdate() {
        if (rlSynchronization.getVisibility() != View.VISIBLE) {
            rlSynchronization.setVisibility(View.VISIBLE);
            scale.start();
        }

        FaceUpdataInfo faceUpdataInfo = mActivity.get().getFaceUpdataInfo();
        //如果绑定了新库，就删掉旧的
        if (faceUpdataInfo.isBindNewFaceSet()) {
            //删除旧的
            AppConstantInfoUtils.bindNewFaceSet();
            UserInfoDBUtil.deleteAllUserInfos();
            RecordInfoDBUtil.deleteAllRecordInfo();
            mPresenter.deletePic(faceUpdataInfo.getCsvUrl());
        } else {
            DownLoadUtils.getInstance().downloadCSV(FdfsUtils.getUrl(faceUpdataInfo.getCsvUrl(), AppConstantInfo.getInstance().getFdfsToken()));
        }
    }

    //需要再次进行人脸同步
    public void setRetryUpdateFace() {
        Log.e(TAG, "setRetryUpdateFace: ");
        isDataMaking = false;
        isMakeDataFinish = false;
    }

    public boolean isMakeDataFinish() {
        return isMakeDataFinish;
    }

    /**
     * 设置绑定与激活状态
     *
     * @param isActive
     * @param isBind
     */
    public void setActiveBindState(boolean isActive, boolean isBind) {
        if (!isBind) {
            //未绑定，删除人脸库等信息
            AppConstantInfoUtils.clearAppConstantInfoParms();
            //删除旧的
            UserInfoDBUtil.deleteAllUserInfos();
            RecordInfoDBUtil.deleteAllRecordInfo();

            tvActionInfo.setText(Utils.getStringByValues(R.string.please_to_bind));
            tvLoading.setVisibility(View.INVISIBLE);
            tvNotShutDown.setVisibility(View.INVISIBLE);
            mPresenter.deletePic("");

            if (rlSynchronization.getVisibility() == View.GONE) {
                rlSynchronization.setVisibility(View.VISIBLE);
                scale.start();
            }

            Intent intent = new Intent(mActivity.get(), MainActivity.class);
            intent.putExtra(AppConstants.AGAIN_TO_MAINACTIVITY_TYPE, AppConstants.AGAIN_MAINACTIVITY_TYPE_ACTIVE);
            mActivity.get().startActivity(intent);

            return;
        }
        //绑定状态，判断激活状态
        judgeActive(isActive);

    }

    /**
     * 是绑定状态，判断激活状态
     *
     * @param isActive
     */
    private void judgeActive(boolean isActive) {
        if (isActive) {
            tvActionInfo.setText(Utils.getStringByValues(R.string.synchronization_1));
            tvLoading.setVisibility(View.VISIBLE);
            tvNotShutDown.setVisibility(View.VISIBLE);

            if (AppConstantInfo.getInstance().getFaceSetId() != -1L) {
                //有绑定人脸库
                if (isMakeDataFinish()) {
                    if (mActivity.get().isInUpdateFace()) {
                        mActivity.get().setInUpdateFace(false);
                        handleInUpdate();
                    } else {
                        //已经绑定过人脸库了
                        if (rlSynchronization.getVisibility() == View.VISIBLE) {
                            rlSynchronization.setVisibility(View.GONE);
                            scale.end();
                        }
                    }
                }
            }

        } else {
            tvActionInfo.setText(Utils.getStringByValues(R.string.please_to_active));
            tvLoading.setVisibility(View.INVISIBLE);
            tvNotShutDown.setVisibility(View.INVISIBLE);
            if (!isMakeDataFinish()) {
                if (rlSynchronization.getVisibility() == View.GONE) {
                    rlSynchronization.setVisibility(View.VISIBLE);
                    scale.start();
                }
            } else {
                if (!mIsIdentify) {
                    if (rlSynchronization.getVisibility() == View.GONE) {
                        rlSynchronization.setVisibility(View.VISIBLE);
                        scale.start();
                    }

                    Intent intent = new Intent(mActivity.get(), MainActivity.class);
                    intent.putExtra(AppConstants.AGAIN_TO_MAINACTIVITY_TYPE, AppConstants.AGAIN_MAINACTIVITY_TYPE_ACTIVE);
                    mActivity.get().startActivity(intent);
                }
            }

        }
    }


    private String getDate() {
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date().getTime());
        return format + getWeek();
    }

    /*获取周几*/
    private String getWeek() {
        Calendar cal = Calendar.getInstance();
        int i = cal.get(Calendar.DAY_OF_WEEK);
        switch (i) {
            case 1:
                return "  周日";
            case 2:
                return "  周一";
            case 3:
                return "  周二";
            case 4:
                return "  周三";
            case 5:
                return "  周四";
            case 6:
                return "  周五";
            case 7:
                return "  周六";
            default:
                return "";
        }
    }
}