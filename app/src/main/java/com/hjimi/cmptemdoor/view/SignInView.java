package com.hjimi.cmptemdoor.view;


import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.MainActivity;
import com.hjimi.cmptemdoor.activity.SignInActivity;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.contract.SignInContract;
import com.hjimi.cmptemdoor.engine.AppConstantInfo;
import com.hjimi.cmptemdoor.utils.EditUtils;
import com.hjimi.cmptemdoor.utils.StringUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.mqtt.MqttUtils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.wight.SmartScrollView;

import java.lang.ref.SoftReference;


public class SignInView implements SignInContract.View, View.OnClickListener, View.OnFocusChangeListener {
    private SoftReference<SignInActivity> mActivity;
    private SignInContract.Presenter mPresenter;
    private EditText etUser, etPassword;
    private SmartScrollView mScrollView;
    private boolean isOpenSoft = true;//是否打开了键盘
    private EditText etId, etKey, etSN, etImiSn;

    public static SignInView newInstance(SignInActivity activity) {
        return new SignInView(activity);
    }

    private SignInView(SignInActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        mScrollView = (SmartScrollView) mActivity.get().findViewById(R.id.smart_scroll_view);
//        etUser = (EditText) mActivity.get().findViewById(R.id.et_user);
        etPassword = (EditText) mActivity.get().findViewById(R.id.et_password);
        mActivity.get().findViewById(R.id.tv_sign_in).setOnClickListener(this);

        etId = (EditText) mActivity.get().findViewById(R.id.et_product_id);
        etKey = (EditText) mActivity.get().findViewById(R.id.et_product_key);
        etSN = (EditText) mActivity.get().findViewById(R.id.et_product_sn);
        etImiSn = (EditText) mActivity.get().findViewById(R.id.et_imi_sn);

        etPassword.setFocusable(true);
        etPassword.setFocusableInTouchMode(true);
        etPassword.requestFocus();

//        etUser.setOnFocusChangeListener(this);
//        etUser.setOnClickListener(this);
        etPassword.setOnFocusChangeListener(this);
        etPassword.setOnClickListener(this);


        final int mKeyHeight = Utils.getHeight() / 3; // sScreenHeight为屏幕高度
        mScrollView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (oldBottom != 0 && bottom != 0 && (oldBottom - bottom) > mKeyHeight) {
                    isOpenSoft = true;
                } else if (oldBottom != 0 && bottom != 0 && (bottom - oldBottom) > mKeyHeight) {
                    isOpenSoft = false;
                }
            }
        });
    }

    @Override
    public void setPresenter(SignInContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_password:
                scrollBottom();
                break;
            case R.id.tv_sign_in:
                EditUtils.hideInput(mActivity.get(), etPassword);
                String pwd = etPassword.getText().toString();
                if (StringUtils.isEmpty(pwd)) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.input_empty));
                    return;
                }

                if (!pwd.equals(AppConstantInfo.getInstance().getMenuPwd())) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.password_error));
                    return;
                }

                String imiSn = etImiSn.getText().toString().trim();
                if (StringUtils.isEmpty(imiSn)) {
                    ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.input_empty1));
                    return;
                }

                MqttUtils.REGIST_NAME = etId.getText().toString().trim();
                MqttUtils.REGIST_PASSWORD = etKey.getText().toString().trim();
                MqttUtils.CLIENT_ID = etSN.getText().toString().trim();
                AppConstantInfo.getInstance().setDevSN(imiSn);
                Intent intent = new Intent(mActivity.get(), MainActivity.class);
                mActivity.get().startActivity(intent);
                mActivity.get().finish();
                break;
        }
    }


    @Override
    public void onFocusChange(View view, boolean b) {
        if (b) {
            switch (view.getId()) {
                case R.id.et_user:
                case R.id.et_password:
                    scrollBottom();
                    break;
            }
        }
    }

    private void scrollBottom() {
        int timeDelay = isOpenSoft ? 100 : 500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(ScrollView.FOCUS_DOWN, true);
            }
        }, timeDelay);
    }
}