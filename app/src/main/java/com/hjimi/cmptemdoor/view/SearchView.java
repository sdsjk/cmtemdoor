package com.hjimi.cmptemdoor.view;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.activity.SearchActivity;
import com.hjimi.cmptemdoor.adapters.RecordAdapter;
import com.hjimi.cmptemdoor.adapters.UserListAdapter;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.contract.SearchContract;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.hjimi.cmptemdoor.interfaces.JudgeNetCallBack;
import com.hjimi.cmptemdoor.utils.EditUtils;
import com.hjimi.cmptemdoor.utils.JudgeNetUtils;
import com.hjimi.cmptemdoor.utils.Utils;
import com.hjimi.cmptemdoor.utils.toast.ToastUtils;
import com.hjimi.cmptemdoor.wight.RecyclerLayout;

import java.lang.ref.SoftReference;
import java.util.List;


public class SearchView implements SearchContract.View, View.OnClickListener, TextView.OnEditorActionListener {
    private SoftReference<SearchActivity> mActivity;
    private SearchContract.Presenter mPresenter;
    private RecyclerLayout recyclerLayout;
    private UserListAdapter userListAdapter;
    private RecordAdapter recordAdapter;
    private EditText editText;
    private int type;

    public static SearchView newInstance(SearchActivity activity) {
        return new SearchView(activity);
    }

    private SearchView(SearchActivity activity) {
        mActivity = new SoftReference<>(activity);
        initView();

    }

    private void initView() {
        type = mActivity.get().getIntent().getIntExtra(AppConstants.PARMS_TYPE, 0);
        mActivity.get().findViewById(R.id.iv_back).setOnClickListener(this);
        editText = (EditText) mActivity.get().findViewById(R.id.et_search);
        editText.setOnEditorActionListener(this);

        recyclerLayout = (RecyclerLayout) mActivity.get().findViewById(R.id.recycler_layout);
        recyclerLayout.setVisibility(View.INVISIBLE);
        recyclerLayout.setContainView(R.id.recycler);
        if (type == 0) {
            recyclerLayout.setLayoutManager(new GridLayoutManager(mActivity.get(), 2));
            userListAdapter = new UserListAdapter(mActivity.get());
            recyclerLayout.setAdapter(userListAdapter);
        } else {
            recyclerLayout.setLayoutManager(new LinearLayoutManager(mActivity.get()));
            recordAdapter = new RecordAdapter(mActivity.get());
            recyclerLayout.setAdapter(recordAdapter);
        }
    }

    @Override
    public void setPresenter(SearchContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                EditUtils.hideInput(mActivity.get(), editText);
                mActivity.get().finish();
                break;
        }
    }


    private void search(String search) {
        recyclerLayout.setVisibility(View.VISIBLE);
        recyclerLayout.changeState(RecyclerLayout.RecyclerhState.In_loading, Utils.getStringByValues(R.string.loading));
        search.replace("%", "/%");
        search.replace("_", "/_");
        if (type == 0) {
            mPresenter.getUsers(search);
        } else {
            mPresenter.getRecords(search);
        }
    }

    public void refresh() {
        JudgeNetUtils.getInstance().judgeNetConnect(new JudgeNetCallBack() {
            @Override
            public void judgeNet(boolean isNetConnect) {
                if (isNetConnect) {
                    recyclerLayout.changeState(RecyclerLayout.RecyclerhState.NoDate, Utils.getStringByValues(R.string.empty), R.mipmap.ic_fail);
                } else {
                    recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Failure_notRetry, Utils.getStringByValues(R.string.net_error));
                }
            }
        });

    }


    @Override
    public void setUserData(List<UserInfo> users) {
        if (users == null || users.size() == 0) {
            refresh();
            return;
        }
        recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Normal_list, null);
        userListAdapter.setList(users);
    }

    @Override
    public void setRecordData(List<RecordInfo> recordInfos) {
        if (recordInfos == null || recordInfos.size() == 0) {
            refresh();
            return;
        }
        recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Normal_list, null);
        recordAdapter.setList(recordInfos);
    }

    @Override
    public void setRetryVisible(String uiMsg, String toastMsg, boolean isCanClick) {
        if (recyclerLayout.getCurrentState() == RecyclerLayout.RecyclerhState.Normal_list) {
            ToastUtils.showToast(mActivity.get(), toastMsg);
        } else {
            recyclerLayout.changeState(RecyclerLayout.RecyclerhState.Failure_notRetry, uiMsg);
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String search = editText.getText().toString().trim();
            //搜索操作
            if (TextUtils.isEmpty(search)) {
                ToastUtils.showToast(mActivity.get(), Utils.getStringByValues(R.string.input_empty));
                return false;
            }
            search(search);
            EditUtils.hideInput(mActivity.get(), editText);
            return true;
        }
        return false;
    }
}