package com.hjimi.cmptemdoor.view;


import android.view.View;
import android.widget.TextView;

import com.hjimi.cmptemdoor.R;
import com.hjimi.cmptemdoor.base.constants.AppConstants;
import com.hjimi.cmptemdoor.contract.EmptyContract;
import com.hjimi.cmptemdoor.dialog.InitErrorDialog;
import com.hjimi.cmptemdoor.interfaces.SuccessCallBack;
import com.hjimi.cmptemdoor.utils.FileUtils;

import java.lang.ref.SoftReference;

public class InitErrorDialogView implements EmptyContract.View, View.OnClickListener {

    private final SoftReference<InitErrorDialog> mFragment;
    private SuccessCallBack callBack;


    public InitErrorDialogView(InitErrorDialog fragment) {
        mFragment = new SoftReference<>(fragment);
        initView();
    }

    public static InitErrorDialogView newInstance(InitErrorDialog personalFragment) {
        return new InitErrorDialogView(personalFragment);
    }

    private void initView() {
        String title = mFragment.get().getArguments().getString(AppConstants.DIALOG_TITLE);
        TextView tvTitle = (TextView) mFragment.get().getView().findViewById(R.id.tv_title);
        tvTitle.setText(title);
        mFragment.get().getView().findViewById(R.id.tv_exit).setOnClickListener(this);
    }


    @Override
    public void setPresenter(EmptyContract.Presenter presenter) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_exit:
                if (callBack != null) {
                    callBack.setSuccessCallBack();
                }
                FileUtils.dissmissDialog(mFragment.get());
                break;
        }
    }

    public void setCallBack(SuccessCallBack callBack) {
        this.callBack = callBack;
    }
}
