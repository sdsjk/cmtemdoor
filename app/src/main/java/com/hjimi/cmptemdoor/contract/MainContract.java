package com.hjimi.cmptemdoor.contract;

import com.hjimi.cmptemdoor.base.BasePresenter;
import com.hjimi.cmptemdoor.base.BaseView;
import com.hjimi.cmptemdoor.bean.Face;
import com.hjimi.cmptemdoor.databean.FaceRecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;
import com.imi.sdk.face.Session;

public interface MainContract {
    interface View extends BaseView<Presenter> {

        void setMatchFeaceData(UserInfo data, String base64);

        void setTemData(float temData);

        void makeDataFinish();

        Session getFaceSession();

        void setResult(String code, String speechMsg, String showMsg);
    }

    interface Presenter extends BasePresenter {
        void makeFeature();

        void getMatchInfo(Face face);

        float getSimilarity();

//        void diningMerc();
//
//        void diningPay(String orderAmount, String phone, String payMode);
//
//        void diningQuery(String orderId, String orderDate);
//
//        void diningDate(String orderId, String sucDate, String sucTime);

        void deletePic(String url);

        void upFaceRecord(FaceRecordInfo info);

        void openDoor(float tem);

        void checkTem();
    }
}
