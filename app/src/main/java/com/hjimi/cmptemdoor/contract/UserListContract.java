package com.hjimi.cmptemdoor.contract;

import com.hjimi.cmptemdoor.base.BasePresenter;
import com.hjimi.cmptemdoor.base.BaseView;
import com.hjimi.cmptemdoor.databean.UserInfo;

import java.util.List;

public interface UserListContract {
    interface View extends BaseView<Presenter> {
        void setData(List<UserInfo> users, int page);

        void setRetryVisible(String uiMsg, String toastMsg, boolean isCanClick);

    }

    interface Presenter extends BasePresenter {
        void getUsers(int page);

    }
}
