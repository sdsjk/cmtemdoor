package com.hjimi.cmptemdoor.contract;

import com.hjimi.cmptemdoor.base.BasePresenter;
import com.hjimi.cmptemdoor.base.BaseView;

public interface TemSettingContract {
    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {

    }
}
