package com.hjimi.cmptemdoor.contract;

import com.hjimi.cmptemdoor.base.BasePresenter;
import com.hjimi.cmptemdoor.base.BaseView;
import com.hjimi.cmptemdoor.databean.RecordInfo;
import com.hjimi.cmptemdoor.databean.UserInfo;

import java.util.List;

public interface SearchContract {
    interface View extends BaseView<Presenter> {
        void setUserData(List<UserInfo> users);

        void setRecordData(List<RecordInfo> recordInfos);

        void setRetryVisible(String uiMsg, String toastMsg, boolean isCanClick);

    }

    interface Presenter extends BasePresenter {
        void getUsers(String search);

        void getRecords(String search);
    }
}
